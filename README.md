# Kholinarium
Kholinarium is a simple 2D Tower Defense game created as a software project for subject Java on Faculty of Mathematics and Physics of Charles University in Prague

## Screenshots
![Main menu](screen1.png?raw=true "Main Menu")
![Game](screen2.png?raw=true "Game")
## How to run:
```
mvn package
cd target
java -jar Kholinarium-1.0-SNAPSHOT.jar
```

## Requirements
Java 1.8
