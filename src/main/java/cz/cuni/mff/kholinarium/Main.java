package cz.cuni.mff.kholinarium;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import cz.cuni.mff.kholinarium.scenes.MainScene;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.prefs.Preferences;

/**
 * Main class that is the entry point of the program. Initializes frame,canvas. Loads and stores preferences.
 * Determines whether display is retina and can change frame size.<br>
 *<br>
 * Created by Adam Hornáček on 13/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague.
 */
public class Main {

    private static GLCanvas canvas; //canvas where everything is drawn
    private static JFrame frame;
    private static Preferences prefs; //program preferences = persistent user data

    public static boolean isRetina = hasRetinaDisplay();

    public static void main(String[] args) {
        prefs = Preferences.userRoot().node(Main.class.getName()); //preferences set up
        try {
            SwingUtilities.invokeAndWait(Main::createLayout);
        } catch (InterruptedException | InvocationTargetException e) {
            e.printStackTrace();
            System.exit(-1); //TODO show that something went wrong
        }
        FPSAnimator animator = new FPSAnimator(canvas, 60);
        animator.start();
    }

    /**
     * method to create necessary components to run the game
     */
    private static void createLayout() {
        frame = new JFrame("Kholinarium");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel mainPanel = new JPanel(new GridLayout()); //not working with implicit layout

        GLCapabilities capabilities = new GLCapabilities(GLProfile.get(GLProfile.GL2));
        capabilities.setDoubleBuffered(true); //easier than setting up FBOs
        capabilities.setHardwareAccelerated(true); //game = should run smoothly
        canvas = new GLCanvas(capabilities);
        canvas.setAutoSwapBufferMode(true);
        mainPanel.add(canvas);

        MainScene scene = new MainScene();

        //adding listeners
        canvas.addGLEventListener(scene);
        canvas.addMouseMotionListener(scene);
        canvas.addMouseListener(scene);
        canvas.addKeyListener(scene);

        frame.getContentPane().add(mainPanel);
        frame.pack();

        //need to change size after "pack"
        int w = 512;
        int h = 400;
        if (getBooleanPreference("hiResEnabled") && !isRetina) {
            w *= 2;
            h *= 2;
        } else if (!getBooleanPreference("hiResEnabled") && isRetina) {
            w /= 2;
            h /= 2;
        }
        changeFrameSize(new Dimension(w, h));

        frame.setLocationRelativeTo(null); //sets window location to the middle of the screen
        frame.setVisible(true);
        frame.setResizable(false);

        canvas.requestFocus(); //request focus so the intro will be skipped when pressing any button =
        // = no need to click on canvas
    }

    /**
     * method to change frame size to dimension + insets
     * @param d desired dimension
     */
    public static void changeFrameSize(Dimension d) {
        //add frame insets to the window size
        Insets insets = frame.getInsets();
        frame.setSize(new Dimension((int) (d.getWidth() + insets.left + insets.right),
                (int) (d.getHeight() + insets.top + insets.bottom)));
        canvas.requestFocus(); //requests focus after every change of dimension
        frame.setLocationRelativeTo(null); //sets window location to the middle of the screen
    }

    /**
     * @return determines whether display is retina
     */
    private static boolean hasRetinaDisplay() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        final GraphicsDevice device = env.getDefaultScreenDevice();
        try {
            Field field = device.getClass().getDeclaredField("scale");
            if (field != null) {
                field.setAccessible(true);
                Object scale = field.get(device);

                if (scale instanceof Integer && ((Integer) scale) == 2) {
                    return true;
                }
            }
        } catch (Exception ignore) {
        }
        return false;
    }

    /**
     * method to get integer value from user data
     * @param key identification key
     * @return appropriate integer for given key – if key was not found then returns 1 =
     *      = only one level is unlocked on start
     */
    public static int getIntPreference(String key) {
        return prefs.getInt(key, 1);
    }

    /**
     * method to put integer value to user data
     * @param key identification key
     * @param value value we want to store
     */
    public static void putIntPreference(String key, int value) {
        prefs.putInt(key, value);
    }

    /**
     * method to get boolean value from user data
     * @param key identification key
     * @return appropriate boolean for given key – if key was not found then returns true =
     *      = music and hi res implicitly enabled
     */
    public static boolean getBooleanPreference(String key) {
        return prefs.getBoolean(key, true);
    }

    /**
     * method to put boolean value to user data
     * @param key identification key
     * @param value value we want to store
     */
    public static void putBooleanPreference(String key, boolean value) {
        prefs.putBoolean(key, value);
    }
}