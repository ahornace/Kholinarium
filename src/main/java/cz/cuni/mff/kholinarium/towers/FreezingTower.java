package cz.cuni.mff.kholinarium.towers;

import cz.cuni.mff.kholinarium.level.Tile;
import cz.cuni.mff.kholinarium.monsters.Monster;

/**
 * Freezing tower class atop Tower class implements shoot, upgrade, get prices methods and implicit configuration for
 * every freezing tower.<br>
 * <br>
 * Created by Adam Hornáček on 16/03/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public final class FreezingTower extends Tower {

    private static final int START_DAMAGE_LEVEL = 1;
    private static final int START_RADIUS_LEVEL = 2;
    private static final int START_FREQUENCY_LEVEL = 5;

    public static final float START_RADIUS = 1.75f;
    private static final float START_DAMAGE = 0.8f;
    public static final int BUILD_PRICE = 325;

    private static final int FIRST_UPGRADE_PRICE = 650;
    private static final int SECOND_UPGRADE_PRICE = 975;

    private static final int FIRST_SELL_PRICE = 175;
    private static final int SECOND_SELL_PRICE = 325;
    private static final int THIRD_SELL_PRICE = 650;

    public FreezingTower(int x, int y, int mapWidth, int mapHeight, int xs, int ys, int texture, int delimiter,
                         Tile[][] map) {
        super(x, y, mapWidth, mapHeight, START_RADIUS, xs, ys, texture, delimiter, map);
        currentDamageLevel = START_DAMAGE_LEVEL;
        currentRadiusLevel = START_RADIUS_LEVEL;
        currentFrequencyLevel = START_FREQUENCY_LEVEL;

        damage = START_DAMAGE;

        radiusChange = 0.3f;
        damageChange = 0.2f;
    }

    /**
     * multiplies velocity of the monsters in the radius by damage value
     */
    public void shoot() {
        for (Tile tile : tilesInRadius) {
            for (Monster m : tile.getMonsters()) {
                if (isInRadius(m.getX(), m.getY())) {
                    m.slowDownBy(damage);
                }
            }
        }
    }

    /**
     * @return returns upgrade price according to the current tower level
     */
    @Override
    public int getUpgradePrice() {
        switch (level) {
            case 0:
                return FIRST_UPGRADE_PRICE;
            case 1:
                return SECOND_UPGRADE_PRICE;
            default:
                return 0; //cannot upgrade
        }
    }

    /**
     * @return returns sell price according to the current level
     */
    @Override
    public int getSellPrice() {
        switch (level) {
            case 0:
                return FIRST_SELL_PRICE;
            case 1:
                return SECOND_SELL_PRICE;
            default:
                return THIRD_SELL_PRICE;
        }
    }

    /**
     * upgrades this tower
     * @param tileX – x coordinate of the tile the tower is in
     * @param tileY – y coordinate of the tile the tower is in
     * @param mapWidth – width of the map
     * @param mapHeight – height of the map
     * @param secondTexture – tower texture of level 2
     * @param thirdTexture – tower texture of level 3
     * @param delimiter – tile dimension
     * @param map – map of tiles
     */
    @Override
    public void upgradeTower(int tileX, int tileY, int mapWidth, int mapHeight, int secondTexture,
                             int thirdTexture, int delimiter, Tile[][] map) {
        damage -= damageChange;
        radius += radiusChange;
        currentDamageLevel++;
        currentRadiusLevel++;
        if (level == 0) {
            texture = secondTexture;
        } else {
            texture = thirdTexture;
        }
        updateCellRadius(tileX, tileY, mapWidth, mapHeight, delimiter, map);
        level++;
    }
}
