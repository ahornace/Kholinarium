package cz.cuni.mff.kholinarium.towers;

import cz.cuni.mff.kholinarium.level.Tile;

/**
 * Laser tower class atop Tower class implements shoot, upgrade, get prices methods and implicit configuration for every
 * laser tower.<br>
 * <br>
 * Created by Adam Hornáček on 22/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public final class LaserTower extends Tower {

    public static final int KILLED_MONSTER = 0;

    public static final float START_RADIUS = 1.75f;
    private static final float START_DAMAGE = 13;
    public static final int BUILD_PRICE = 275;

    private static final int START_DAMAGE_LEVEL = 2;
    private static final int START_RADIUS_LEVEL = 2;
    private static final int START_FREQUENCY_LEVEL = 5;

    private static final int FIRST_UPGRADE_PRICE = 550;
    private static final int SECOND_UPGRADE_PRICE = 825;

    private static final int FIRST_SELL_PRICE = 125;
    private static final int SECOND_SELL_PRICE = 250;
    private static final int THIRD_SELL_PRICE = 375;

    public LaserTower(int x, int y, int mapWidth, int mapHeight, int tileX, int tileY, int texture, int delimiter,
                      Tile[][] map) {
        super(x, y, mapWidth, mapHeight, START_RADIUS, tileX, tileY, texture, delimiter, map);
        currentDamageLevel = START_DAMAGE_LEVEL;
        currentRadiusLevel = START_RADIUS_LEVEL;
        currentFrequencyLevel = START_FREQUENCY_LEVEL;

        damage = START_DAMAGE;

        damageChange = 7.5f;
        radiusChange = 0.5f;
    }

    /**
     * shoots at its target
     * @param delta change in time so we can compute damage to apply
     * @return if we killed the monster then we will return KILLED_MONSTER so gameScene can get rid of it
     */
    public int shoot(long delta) {
        if (target == null || !target.isAlive() || !isInRadius(target.getX(), target.getY())) {
            if (!findNewTarget()) {
                return -1;
            }
        }
        target.inflictDamage(damage * delta / 1000000000L);
        if (!target.isAlive()) {
            return KILLED_MONSTER;
        }
        return -1;
    }

    /**
     * @return returns upgrade price according to the current tower level
     */
    @Override
    public int getUpgradePrice() {
        switch (level) {
            case 0:
                return FIRST_UPGRADE_PRICE;
            case 1:
                return SECOND_UPGRADE_PRICE;
            default:
                return 0; //cannot upgrade
        }
    }

    /**
     * @return returns sell price according to the current level
     */
    @Override
    public int getSellPrice() {
        switch (level) {
            case 0:
                return FIRST_SELL_PRICE;
            case 1:
                return SECOND_SELL_PRICE;
            default:
                return THIRD_SELL_PRICE;
        }
    }

    /**
     * upgrades this tower
     * @param tileX x coordinate of the tile the tower is in
     * @param tileY y coordinate of the tile the tower is in
     * @param mapWidth width of the map
     * @param mapHeight height of the map
     * @param secondTexture tower texture of level 2
     * @param thirdTexture tower texture of level 3
     * @param delimiter tile dimension
     * @param map map of tiles
     */
    @Override
    public void upgradeTower(int tileX, int tileY, int mapWidth, int mapHeight, int secondTexture,
                             int thirdTexture, int delimiter, Tile[][] map) {
        damage += damageChange;
        radius += radiusChange;
        currentDamageLevel++;
        currentRadiusLevel++;
        if (level == 0) {
            texture = secondTexture;
        } else {
            texture = thirdTexture;
        }
        updateCellRadius(tileX, tileY, mapWidth, mapHeight, delimiter, map);
        level++;
    }
}
