package cz.cuni.mff.kholinarium.towers;

import cz.cuni.mff.kholinarium.level.Tile;
import cz.cuni.mff.kholinarium.level.TileType;
import cz.cuni.mff.kholinarium.monsters.Monster;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Tower class is a wrap class for all towers. Includes all the basic data that all the towers share e.g. radius or
 * current level of tower. <br>
 * <br>
 * Created by Adam Hornáček on 27/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */

public abstract class Tower {

    public static final int BASIC_TOWER = 0;
    public static final int ENHANCED_TOWER = 1;
    public static final int MASTER_TOWER = 2;

    int x;
    int y;
    float radius;
    float damage;
    long frequency;
    Monster target;
    LinkedList<Tile> tilesInRadius;
    private float rSqr;
    int level = BASIC_TOWER;
    int texture;

    int currentDamageLevel;
    int currentRadiusLevel;
    int currentFrequencyLevel;

    float damageChange;
    float radiusChange;
    long frequencyChange;

    Tower(int x, int y, int mapWidth, int mapHeight, float radius, int tileX, int tileY, int texture,
          int delimiter, Tile[][] map) {
        this.x = x;
        this.y = y;
        this.texture = texture;
        this.radius = radius;
        updateCellRadius(tileX, tileY, mapWidth, mapHeight, delimiter, map);
    }

    public abstract int getUpgradePrice();
    public abstract int getSellPrice();
    public abstract void upgradeTower(int cellClickedX, int cellClickedY, int mapWidth, int mapHeight,
                                      int secondTexture, int thirdTexture, int delimiter, Tile[][] map);

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getRadius() {
        return radius;
    }

    public Monster getTarget() {
        return target;
    }

    public int getLevel() {
        return level;
    }

    public int getTexture() {
        return texture;
    }

    public int getCurrentDamageLevel() {
        return currentDamageLevel;
    }

    public int getCurrentRadiusLevel() {
        return currentRadiusLevel;
    }

    public int getCurrentFrequencyLevel() {
        return currentFrequencyLevel;
    }

    public float getRadiusChange() {
        return radiusChange;
    }

    /**
     * sets the target to null
     */
    public void resetTarget() {
        target = null;
    }

    /**
     * method that finds all tiles in radius and sorts them according to the distance from the END =
     *      = monsters in the tile closest to the END are to be shot at first
     * @param tileX x map coordination of tower
     * @param tileY y map coordination of tower
     * @param mapWidth map width
     * @param mapHeight map height
     * @param delimiter tile dimension
     * @param map map of tiles
     */
    void updateCellRadius(int tileX, int tileY, int mapWidth, int mapHeight, int delimiter, Tile[][] map) {
        tilesInRadius = new LinkedList<>();

        float r = (delimiter * radius);
        rSqr = r * r;

        int cRadius = (int) Math.ceil(radius - 0.5f); //minus 0.5f because tower is in the middle of the tile
        for (int i = tileX - cRadius; i <= tileX + cRadius; i++) {
            for (int j = tileY - cRadius; j <= tileY + cRadius; j++) {
                if (i >= 0 && i < mapWidth && j >= 0 && j < mapHeight) {
                    //adds only appropriate tiles = monster cannot walk through built sites or non built sites
                    if (map[i][j].getType() == TileType.PATH || map[i][j].getType() == TileType.END) {
                        tilesInRadius.add(map[i][j]);
                    }
                }
            }
        }

        tilesInRadius.sort(Comparator.comparingInt(Tile::getLengthToTheClosestExit));
    }

    /**
     * checks if given x and y coordinates are in radius of this tower
     * @param x target's x coordinate
     * @param y target's y coordinate
     * @return true if given x and y coordinates are in radius, otherwise returns false
     */
    boolean isInRadius(double x, double y) {
        double dx = x - this.x;
        double dy = y - this.y;
        return dx * dx + dy * dy < rSqr;
    }

    /**
     * finds new target
     * @return true if found monster otherwise returns false
     */
    boolean findNewTarget() {
        for (Tile tile : tilesInRadius) {
            for (Iterator<Monster> it = tile.getMonsters().iterator(); it.hasNext(); ) {
                Monster m = it.next();
                if (!m.isAlive()) {
                    it.remove();
                } else if (isInRadius(m.getX(), m.getY())) {
                    target = m;
                    return true;
                }
            }
        }
        target = null;
        return false;
    }

}
