package cz.cuni.mff.kholinarium.towers;

import cz.cuni.mff.kholinarium.level.Tile;

/**
 * Arrow tower class atop Tower class implements shoot, upgrade, get prices methods and implicit configuration for every
 * arrow tower. Also implements functions for rotating and aiming at target.<br>
 *
 * Created by Adam Hornáček on 27/02/15.
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public final class ArrowTower extends Tower {

    private static final int START_DAMAGE_LEVEL = 3;
    private static final int START_RADIUS_LEVEL = 1;
    private static final int START_FREQUENCY_LEVEL = 2;

    public static final float START_RADIUS = 1.5f;
    private static final float START_DAMAGE = 14;
    private static final long START_FREQUENCY = 1000000000L;
    public static final int BUILD_PRICE = 200;

    private static final int FIRST_UPGRADE_PRICE = 400;
    private static final int SECOND_UPGRADE_PRICE = 600;

    private static final int FIRST_SELL_PRICE = 100;
    private static final int SECOND_SELL_PRICE = 200;
    private static final int THIRD_SELL_PRICE = 400;

    private static final float ROTATION_SPEED_IN_DEGREES = 180;
    private static final float PROJECTILE_VELOCITY = 3f;

    public float angle;
    private boolean isDirectedAtTarget;
    private long sinceLastShot;
    private double targetX;
    private double targetY;

    private int delimiter;

    public ArrowTower(int x, int y, int mapWidth, int mapHeight, int xs, int ys, int texture, int delimiter,
                      Tile[][] map) {
        super(x, y, mapWidth, mapHeight, START_RADIUS, xs, ys, texture, delimiter, map);
        currentDamageLevel = START_DAMAGE_LEVEL;
        currentRadiusLevel = START_RADIUS_LEVEL;
        currentFrequencyLevel = START_FREQUENCY_LEVEL;

        damage = START_DAMAGE;
        frequency = START_FREQUENCY;

        damageChange = 8;
        radiusChange = 0.25f;
        frequencyChange = 150000000; // 0.15 seconds

        this.delimiter = delimiter;
    }

    /**
     * @return returns upgrade price according to the current tower level
     */
    @Override
    public int getUpgradePrice() {
        switch (level) {
            case 0:
                return FIRST_UPGRADE_PRICE;
            case 1:
                return SECOND_UPGRADE_PRICE;
            default:
                return 0; //cannot upgrade
        }
    }

    /**
     * @return returns sell price according to the current level
     */
    @Override
    public int getSellPrice() {
        switch (level) {
            case 0:
                return FIRST_SELL_PRICE;
            case 1:
                return SECOND_SELL_PRICE;
            default:
                return THIRD_SELL_PRICE;
        }
    }

    /**
     * upgrades this tower
     * @param tileX x coordinate of the tile the tower is in
     * @param tileY y coordinate of the tile the tower is in
     * @param mapWidth width of the map
     * @param mapHeight height of the map
     * @param secondTexture tower texture of level 2
     * @param thirdTexture tower texture of level 3
     * @param delimiter tile dimension
     * @param map map of tiles
     */
    @Override
    public void upgradeTower(int tileX, int tileY, int mapWidth, int mapHeight, int secondTexture,
                             int thirdTexture, int delimiter, Tile[][] map) {
        damage += damageChange;
        radius += radiusChange;
        frequency -= frequencyChange;
        currentDamageLevel++;
        currentRadiusLevel++;
        currentFrequencyLevel++;
        if (level == 0) {
            texture = secondTexture;
        } else {
            texture = thirdTexture;
        }
        updateCellRadius(tileX, tileY, mapWidth, mapHeight, delimiter, map);
        level++;
    }

    /**
     * updates angle of the tower according to the change of time and target position
     * @param delta change of time since the last update
     */
    public void updateAngle(long delta) {
        if (target != null) {
            targetX = target.getX();
            targetY = target.getY();
            float time = computeTimeOfIntersection(delimiter);
            // – what is the time the arrow and monster are going to collide
            if (!Float.isNaN(time)) { //the intersection can be computed –
                // – changes target position to the collision position
                switch (target.getDirection()) {
                    case LEFT:
                        targetX = target.getX() - time * target.getVelocity() * delimiter;
                        targetY = target.getY();
                        break;
                    case RIGHT:
                        targetX = target.getX() + time * target.getVelocity() * delimiter;
                        targetY = target.getY();
                        break;
                    case UP:
                        targetX = target.getX();
                        targetY = target.getY() + time * target.getVelocity() * delimiter;
                        break;
                    case DOWN:
                        targetX = target.getX();
                        targetY = target.getY() - time * target.getVelocity() * delimiter;
                        break;
                }
            }
            float targetAngle = (float) Math.toDegrees(Math.atan2(targetY - y, targetX - x));
            //shoot the arrow the proper way

            if (targetAngle < 0) { //repairs angle to the interval [0, 360]
                targetAngle += 360;
            }
            float change = ROTATION_SPEED_IN_DEGREES * delta / 1000000000L;
            if (Math.abs(targetAngle - angle) <= change) { //we are looking at the target position
                angle = targetAngle;
                isDirectedAtTarget = true;
            } else { //we need to compute which way to rotate the tower to look at target
                if (angle > 360) {
                    angle %= 360;
                } else if (angle < 0) {
                    angle += 360;
                }
                isDirectedAtTarget = false;
                if (targetAngle > angle) {
                    if (targetAngle - angle < 360 - targetAngle + angle) {
                        angle += change;
                    } else {
                        angle -= change;
                    }
                } else {
                    if (angle - targetAngle < 360 - angle + targetAngle) {
                        angle -= change;
                    } else {
                        angle += change;
                    }
                }

            }
        }
        sinceLastShot += delta;
    }

    /**
     * computes angle between targets direction and the tower for use in the cosine theorem
     * @param angle angle between the tower and the monster position
     * @return angle between targets direction and the tower for use in the cosine theorem
     */
    private float angleBetweenTargetsDirectionAndTower(float angle) {
        switch (target.getDirection()) {
            case LEFT:
                if (angle > 180) {
                    angle = 360 - angle;
                }
                break;
            case RIGHT:
                if (angle < 180) {
                    angle = 180 - angle;
                } else {
                    angle -= 180;
                }
                break;
            case UP:
                if (angle < 90) {
                    angle += 90;
                } else if (angle < 180) {
                    angle = 180 - angle;
                } else if (angle < 270) {
                    angle = 270 - angle;
                } else {
                    angle -= 270;
                }
                break;
            case DOWN:
                if (angle < 90) {
                    angle = 90 - angle;
                } else if (angle < 180) {
                    angle -= 90;
                } else if (angle < 270) {
                    angle -= 90;
                } else {
                    angle = 450 - angle;
                }
        }
        return angle;
    }

    /**
     * computes time of intersection by the cosine theorem – we know arrow and monster speed, the way monster is heading
     * and the angle the tower is looking at
     * @param delimiter tile dimension so we know how to compute proper distances
     * @return time of the intersection so we can compute the intersection location
     */
    private float computeTimeOfIntersection(int delimiter) {
        float alpha = (float) Math.toDegrees(Math.atan2(target.getY() - y, target.getX() - x)); //angle between monster
        // and tower from monster perspective
        if (alpha < 0) {
            alpha += 360;
        }
        alpha = angleBetweenTargetsDirectionAndTower(alpha);
        float dx = (float) (target.getX() - x) / delimiter;
        float dy = (float) (target.getY() - y) / delimiter;

        //the cosine theorem c^2 = a^2 + b^2 - 2*a*c*cos(alpha)
        float distance = (float) Math.sqrt(dx * dx + dy * dy);
        float a = target.getVelocity() * target.getVelocity() - PROJECTILE_VELOCITY * PROJECTILE_VELOCITY;
        float b = (float) (-2 * distance * target.getVelocity() * Math.cos(Math.toRadians(alpha)));
        float c = distance * distance;
        float discriminant = b * b - 4 * a * c;
        if (discriminant < 0) {
            return Float.NaN;
        }
        discriminant = (float) Math.sqrt(discriminant);
        float divisor = 2 * a;
        float x1 = ((-b + discriminant) / divisor);
        float x2 = ((-b - discriminant) / divisor);
        return Math.max(x1, x2);
    }

    /**
     * shoots at given target if possible
     * @return projectile or null if did not shoot
     */
    public Projectile shoot() {
        if (target == null || !target.isAlive() || !isInRadius(target.getX(), target.getY())) {
            if (!findNewTarget()) {
                return null;
            }
            isDirectedAtTarget = false; //repairs bug when tower was shooting even if it shouldn't
        }
        if (!isDirectedAtTarget) {
            return null;
        }
        if (sinceLastShot > frequency) { //checks if we have arrow loaded and are ready to fire
            sinceLastShot = 0;
            Projectile p = new Projectile(x, y, PROJECTILE_VELOCITY, damage); //shoots
            p.setTarget(targetX, targetY, delimiter);
            return p;
        }
        return null;
    }
}
