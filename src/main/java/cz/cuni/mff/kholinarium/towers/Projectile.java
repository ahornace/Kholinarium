package cz.cuni.mff.kholinarium.towers;

import cz.cuni.mff.kholinarium.monsters.Monster;

/**
 * Projectile class represents projectile in the scene. Its position, direction, target position, speed and damage.<br>
 *<br>
 * Created by Adam Hornáček on 27/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public final class Projectile {

    public static final int DELETE_PROJECTILE = 0;

    private double x;
    private double y;
    private float angle;
    private double targetX;
    private double targetY;
    private double dx;
    private double dy;
    private float velocity;
    private double maxSpeedX;
    private double maxSpeedY;
    private float damage;

    private int tileX;
    private int tileY;

    public Projectile(double x, double y, float velocity, float damage) {
        this.x = x;
        this.y = y;
        this.velocity = velocity;
        this.damage = damage;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public float getAngle() {
        return angle;
    }

    public float getDamage() {
        return damage;
    }

    public int getTileX() {
        return tileX;
    }

    public int getTileY() {
        return tileY;
    }

    /**
     * sets the target location where arrow will be heading
     * @param targetX – target position x value
     * @param targetY – target position y value
     * @param delimiter – tileWidth/tileHeight
     */
    public void setTarget(double targetX, double targetY, int delimiter) {
        this.targetX = targetX;
        this.targetY = targetY;
        double angle = Math.atan2(targetY - y, targetX - x);
        dy = Math.sin(angle) * velocity * delimiter; //speed in y axis
        dx = Math.cos(angle) * velocity * delimiter; //speed in x axis
        maxSpeedX = 0.02 * Math.abs(dx);
        maxSpeedY = 0.02 * Math.abs(dy);
        this.angle = (float) Math.toDegrees(angle);
    }

    /**
     * moves projectile to its target location
     * @param delta – change in time so we know how much move the projectile
     * @param topBarHeight – height of the top bar = useful for determining in which tile the projectile is in
     * @param mapWidth – width of the map in tiles
     * @param mapHeight – height of the map in tiles
     * @param width – width of the canvas in pixels
     * @param height – height of the canvas in pixels
     * @param delimiter – tile width or height = both are the same
     * @return returns DELETE_PROJECTILE when reached its final destination or is outside map otherwise returns
     *      implicit value -1 which indicates that nothing happened
     */
    public int move(long delta, int topBarHeight, int mapWidth, int mapHeight,
                     int width, int height, int delimiter) {

        if (Math.abs(targetX - x) <= maxSpeedX && Math.abs(targetY - y) <= maxSpeedY) { //is in position
            return DELETE_PROJECTILE;
        }

        //change position
        x += dx * delta / 1000000000L;
        y += dy * delta / 1000000000L;

        //TODO check if it wouldn't be better just let the projectile reach its destination instead of dividing
        tileX = (((int) x + width / 2) / delimiter);
        tileY = ((height / 2 - (int) y - topBarHeight) / delimiter);

        if (tileX < 0 || tileX >= mapWidth || tileY < 0 || tileY >= mapHeight) { //projectile is not in the map
            return DELETE_PROJECTILE;
        }
        return -1; //nothing happens
    }

    /**
     * determines whether projectile hit some monster or not
     * @param m Monster with which we are supposed to determine the collision
     * @return true if they collide otherwise returns false
     */
    public boolean collidesWithMonster(Monster m) {
        return x >= m.getX() - m.getBoundingBoxX() && x <= m.getX() + m.getBoundingBoxX() &&
                y >= m.getY() - m.getBoundingBoxY() && y <= m.getY() + m.getBoundingBoxY();
    }

}
