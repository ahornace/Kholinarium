package cz.cuni.mff.kholinarium.level;

/**
 * State class is a model class that is used in pathFinder to find appropriate path using bfs.<br>
 * <br>
 * Created by Adam Hornáček on 14/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class State implements Comparable<State> {

    private boolean visited = false;
    private State from = null;
    private int distance = Integer.MAX_VALUE;
    private Point point;

    public State(int pointX, int pointY) {
        point = new Point();
        point.setX(pointX);
        point.setY(pointY);
    }

    public boolean isVisited() {
        return visited;
    }

    public State getFrom() {
        return from;
    }

    public Point getPoint() {
        return point;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public void setFrom(State from) {
        this.from = from;
    }

    @Override
    public int compareTo(State o) {
        return Integer.compare(this.distance, o.distance);
    }
}
