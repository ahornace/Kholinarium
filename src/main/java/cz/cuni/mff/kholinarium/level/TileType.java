package cz.cuni.mff.kholinarium.level;

/**
 * TileType is an enum class that represents all the types that tile can have.<br>
 * <br>
 * Created by Adam Hornáček on 13/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public enum TileType {
    PATH, TOWER, BUILT_SITE, NON_BUILT_SITE, END
}
