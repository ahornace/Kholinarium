package cz.cuni.mff.kholinarium.level;

import cz.cuni.mff.kholinarium.monsters.Monster;
import cz.cuni.mff.kholinarium.towers.Tower;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Tile is a wrapper class that implements all the necessary stuff for a tile in the map e.g. tile type, monsters in
 * tile, tower that is built on this tile and its distance to the end.<br>
 * <br>
 * Created by Adam Hornáček on 13/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class Tile {

    private int x;
    private int y;
    private TileType type = TileType.NON_BUILT_SITE;
    private String texture = "nonBuiltSite";
    private int lengthToTheClosestExit = Integer.MAX_VALUE;
    private LinkedList<Monster> monsters = new LinkedList<>();
    private Tower tower;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public TileType getType() {
        return type;
    }

    public String getTexture() {
        return texture;
    }

    public int getLengthToTheClosestExit() {
        return lengthToTheClosestExit;
    }

    public LinkedList<Monster> getMonsters() {
        return monsters;
    }

    public Tower getTower() {
        return tower;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setType(TileType type) {
        this.type = type;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public void setLengthToTheClosestExit(int length) {
        lengthToTheClosestExit = length;
    }

    public void setTower(Tower t) {
        tower = t;
    }

    /**
     * adds monster to the tile – monsters are sorted according to their velocity so the tower picks the one with
     * highest velocity
     * @param m Monster to be add to this tile
     */
    public void addMonster(Monster m) {
        monsters.add(m);
        monsters.sort((o1, o2) -> Float.compare(o1.getVelocity(), o2.getVelocity()));
    }

}
