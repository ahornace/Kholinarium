package cz.cuni.mff.kholinarium.level;

/**
 * Point is a model class that represents one point = x and y coordinates.<br>
 * <br>
 * Created by Adam Hornáček on 14/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class Point {

    private int x, y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
