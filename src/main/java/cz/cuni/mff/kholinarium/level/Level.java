package cz.cuni.mff.kholinarium.level;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Level is a wrapper class for the necessary information that game scene needs to run this level.<br>
 * <br>
 * Created by Adam Hornáček on 22/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class Level {

    private int startingMoney;
    private Tile[][] map;
    private int mapWidth;
    private int mapHeight;
    private LinkedList<Wave> waves = new LinkedList<>();
    private ArrayList<LinkedList<Point>> paths;

    public int getStartingMoney() {
        return startingMoney;
    }

    public Tile[][] getMap() {
        return map;
    }

    public int getMapWidth() {
        return mapWidth;
    }

    public int getMapHeight() {
        return mapHeight;
    }

    public LinkedList<Wave> getWaves() {
        return waves;
    }

    public void setPaths(ArrayList<LinkedList<Point>> paths) {
        this.paths = paths;
    }

    public LinkedList<Point> getPath(int id) {
        return paths.get(id);
    }

    Level(int startingMoney, int mapWidth, int mapHeight) {
        this.startingMoney = startingMoney;
        this.mapWidth = mapWidth;
        this.mapHeight = mapHeight;
        map = new Tile[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++)
            for (int j = 0; j < mapHeight; j++)
                map[i][j] = new Tile();
    }

    /**
     * sets type and texture type to the tile
     * @param x x position of the tile
     * @param y y position of the tile
     * @param type type of the tile
     * @param texture texture of the tile
     */
    public void changeMapTile(int x, int y, TileType type, String texture) {
        map[x][y].setType(type);
        map[x][y].setTexture(texture);
    }

    /**
     * for every tile finds its distance to the closest end tile
     */
    public void findDistancesToClosestEnd() {
        PathFinder pathFinder = new PathFinder();
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                if (map[i][j].getType() == TileType.PATH) {
                    int len = pathFinder.findPathLength(map, mapWidth, mapHeight, i, j);
                    map[i][j].setLengthToTheClosestExit(len);
                }
            }
        }
    }
}
