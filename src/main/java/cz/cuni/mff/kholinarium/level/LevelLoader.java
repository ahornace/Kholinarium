package cz.cuni.mff.kholinarium.level;

import cz.cuni.mff.kholinarium.monsters.EnemyNest;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * LevelLoader is a class that parses level xml and loads all the needed data.<br>
 * <br>
 * Created by Adam Hornáček on 22/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class LevelLoader {

    private Document dom; 

    /**
     * creates document according to the parameter
     * @param pathToLevelFile level name that we are supposed to parse
     */
    private void parseXmlFile(String pathToLevelFile) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputStream is = this.getClass().getResourceAsStream("/levels/" + pathToLevelFile);
            InputStream bufferedIn = new BufferedInputStream(is);
            dom = db.parse(bufferedIn);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(-1); //TODO show message
        }
    }

    /**
     * @param pathToLevelFile path to the level file
     * @return returns level that it parses from file path given in parameter
     */
    public Level parseLevel(String pathToLevelFile) {
        parseXmlFile(pathToLevelFile);
        Element docEle = dom.getDocumentElement();
        int startingMoney = getIntValue(docEle, "StartingMoney");
        int mapWidth = getIntValue(docEle, "MapWidth");
        int mapHeight = getIntValue(docEle, "MapHeight");
        Level lvl = new Level(startingMoney, mapWidth, mapHeight);

        loadTiles(docEle, lvl);

        loadPaths(docEle, lvl);

        loadWaves(docEle, lvl, mapWidth, mapHeight);

        return lvl;
    }

    /**
     * loads tiles for the level map
     * @param docEle root element of level xml
     * @param lvl instance of Level
     */
    private void loadTiles(Element docEle, Level lvl) {
        NodeList tiles = docEle.getElementsByTagName("Tile");
        if (tiles != null && tiles.getLength() > 0) {
            for (int i = 0; i < tiles.getLength(); i ++) {
                Element el = (Element)tiles.item(i);
                String type = getTextValue(el, "Type");
                int x = getIntValue(el, "x");
                int y = getIntValue(el, "y");
                String texture = getTextValue(el, "Texture");
                lvl.changeMapTile(x, y, TileType.valueOf(type), texture);
            }
        }
        lvl.findDistancesToClosestEnd();
    }

    /**
     * loads paths = paths with reference points
     * @param docEle root element of level xml
     * @param lvl instance of Level
     */
    private void loadPaths(Element docEle, Level lvl) {
        NodeList paths = docEle.getElementsByTagName("Path");
        if (paths != null && paths.getLength() > 0) {
            ArrayList<LinkedList<Point>> levelPaths = new ArrayList<>();
            for (int i = 0; i < paths.getLength(); i++) {
                Element p = (Element) paths.item(i);
                LinkedList<Point> path = new LinkedList<>();
                NodeList points = p.getElementsByTagName("Point");
                if (points != null && points.getLength() > 0) {
                    for (int j = 0; j < points.getLength(); j++) {
                        Element point = (Element) points.item(j);
                        int x = getIntValue(point, "x");
                        int y = getIntValue(point, "y");
                        Point point1 = new Point();
                        point1.setX(x);
                        point1.setY(y);
                        path.add(point1);
                    }
                }
                levelPaths.add(path);
            }
            lvl.setPaths(levelPaths);
        }
    }

    /**
     * loads enemy waves
     * @param docEle root element of level xml
     * @param lvl instance of Level
     * @param mapWidth width of map
     * @param mapHeight height of map
     */
    private void loadWaves(Element docEle, Level lvl, int mapWidth, int mapHeight) {
        PathFinder pathFinder = new PathFinder();
        NodeList waves = docEle.getElementsByTagName("Wave");
        if (waves != null && waves.getLength() > 0) {
            for (int i = 0; i < waves.getLength(); i++) {
                Wave wave = new Wave();
                Element w = (Element) waves.item(i);
                NodeList nests = w.getElementsByTagName("Nest");
                if (nests != null && nests.getLength() > 0) {
                    for (int j = 0; j < nests.getLength(); j++) {
                        Element nest = (Element) nests.item(j);
                        int pathId = Integer.parseInt(nest.getAttribute("pathId"));
                        float startTime = getFloatValue(nest, "StartTime");
                        int x = getIntValue(nest, "x");
                        int y = getIntValue(nest, "y");
                        int monsterType = getIntValue(nest, "MonsterType");
                        float monsterVelocity = getFloatValue(nest, "MonsterVelocity");
                        float monsterHealth = getFloatValue(nest, "MonsterHealth");
                        int monsterValue = getIntValue(nest, "MonsterValue");
                        int count = getIntValue(nest, "Count");
                        double frequency = getDoubleValue(nest, "Frequency");
                        wave.getNests().add(new EnemyNest(startTime, x, y, monsterType, count, frequency,
                                pathFinder.findPath(lvl.getMap(), mapWidth, mapHeight, x, y, lvl.getPath(pathId)),
                                monsterVelocity, monsterHealth, monsterValue));
                    }
                }
                lvl.getWaves().addLast(wave);
            }
        }
    }

    /**
     * @param element element we want to get the string value from
     * @param tagName name of the tag that the value is stored in
     * @return returns string value with tagName in element
     */
    private String getTextValue(Element element, String tagName) {
        String textVal = null;
        NodeList nl = element.getElementsByTagName(tagName);
        if(nl != null && nl.getLength() > 0) {
            Element el = (Element)nl.item(0);
            textVal = el.getFirstChild().getNodeValue();
        }
        return textVal;
    }

    /**
     * @param element element we want to get the int value from
     * @param tagName name of the tag that the value is stored in
     * @return returns int value with tagName in element
     */
    private int getIntValue(Element element, String tagName) {
        String textVal = getTextValue(element, tagName);
        return Integer.parseInt(textVal);
    }

    /**
     * @param element element we want to get the double value from
     * @param tagName name of the tag that the value is stored in
     * @return returns double value with tagName in element
     */
    private double getDoubleValue(Element element, String tagName) {
        String textVal = getTextValue(element, tagName);
        return Double.parseDouble(textVal);
    }

    /**
     * @param element element we want to get the float value from
     * @param tagName name of the tag that the value is stored in
     * @return returns float value with tagName in element
     */
    private float getFloatValue(Element element, String tagName) {
        String textVal = getTextValue(element, tagName);
        return Float.parseFloat(textVal);
    }

}
