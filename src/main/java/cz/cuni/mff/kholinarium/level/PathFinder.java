package cz.cuni.mff.kholinarium.level;

import java.util.LinkedList;

/**
 * PathFinder is a class that represents methods for finding paths from different nests through milestones up to the
 * end.<br>
 * <br>
 * Created by Adam Hornáček on 13/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
class PathFinder {

    /**
     * @param map level map
     * @param mapWidth width of map
     * @param mapHeight height of map
     * @param startX x position of starting tile
     * @param startY y position of starting tile
     * @param pointsOfReference points that we need to pass
     * @return returns fastest path from starting tile to any ending tile that passes all the points from
     * pointsOfReference2
     */
    public LinkedList<Point> findPath(Tile[][] map, int mapWidth, int mapHeight, int startX, int startY,
                         LinkedList<Point> pointsOfReference) {

        LinkedList<Point> pointsOfReference2 = new LinkedList<>(pointsOfReference);
        LinkedList<Point> finalPath = new LinkedList<>();
        Point last;
        LinkedList<Point> partOfPath;
        if (pointsOfReference2.size() > 0) {
            Point p = pointsOfReference2.pollFirst();
            partOfPath = findPath(map, mapWidth, mapHeight, startX, startY, p.getX(), p.getY());
            for (Point point : partOfPath)
                finalPath.addLast(point);
        }
        for (Point p1 : pointsOfReference2) {
            last = finalPath.getLast();
            partOfPath = findPath(map, mapWidth, mapHeight, last.getX(), last.getY(), p1.getX(), p1.getY());
            for (Point point : partOfPath)
                finalPath.addLast(point);
        }
        if (pointsOfReference.size() == 0) { //no points of reference – find path to end
            partOfPath = findPath(map, mapWidth, mapHeight, startX, startY);
        } else {
            last = finalPath.getLast();
            partOfPath = findPath(map, mapWidth, mapHeight, last.getX(), last.getY());
        }
        for (Point point : partOfPath)
            finalPath.addLast(point);
        return finalPath;
    }

    /**
     * finds fastest path from start position to the end position
     * @param map level map
     * @param mapWidth width of map
     * @param mapHeight height of map
     * @param startX x position of starting tile
     * @param startY y position of starting tile
     * @param endX x position of ending tile
     * @param endY y position of ending tile
     * @return fastest path from starting tile to the specified ending tile
     */
    private LinkedList<Point> findPath(Tile[][] map, int mapWidth, int mapHeight, int startX, int startY, int endX,
                                       int endY) {
        if (startX >= 0 && startX < mapWidth && startY >= 0 && startY < mapHeight) {
            LinkedList<State> queue = new LinkedList<>();
            State[][] states = createImplicitStates(map, mapWidth, mapHeight);
            states[startX][startY].setVisited(true);
            queue.add(states[startX][startY]);

            while (!queue.isEmpty()) {
                State state = queue.pollFirst();
                if (state.getPoint().getX() == endX && state.getPoint().getY() == endY) {
                    return getPath(state);
                }
                bfsStep(state, states, queue, mapWidth, mapHeight);
            }
        }
        return new LinkedList<>();
    }

    /**
     * @param map – level map
     * @param mapWidth – width of map
     * @param mapHeight – height of map
     * @param startX – x position of starting tile
     * @param startY – y position of starting tile
     * @return – fastest path from the starting tile to any ending tile
     */
    private LinkedList<Point> findPath(Tile[][] map, int mapWidth, int mapHeight, int startX, int startY) {

        if (startX >= 0 && startX < mapWidth && startY >= 0 && startY < mapHeight) {
            LinkedList<State> queue = new LinkedList<>();
            State[][] states = createImplicitStates(map, mapWidth, mapHeight);
            states[startX][startY].setVisited(true);
            queue.add(states[startX][startY]);

            while (!queue.isEmpty()) {
                State state = queue.pollFirst();
                if (map[state.getPoint().getX()][state.getPoint().getY()].getType() == TileType.END) {
                    return getPath(state);
                }
                bfsStep(state, states, queue, mapWidth, mapHeight);
            }
        }
        return null;
    }

    /**
     *
     * @param endState state that is on the end
     * @return list of points that represent the path which we used to get to the endState
     */
    private LinkedList<Point> getPath(State endState) {
        LinkedList<Point> path = new LinkedList<>();
        path.addFirst(endState.getPoint());
        State s;
        while ((s = endState.getFrom()) != null) {
            path.addFirst(s.getPoint());
            endState = s;
        }
        path.removeFirst(); //removes first point
        return path;
    }

    /**
     *
     * @param map level map
     * @param mapWidth map width
     * @param mapHeight map height
     * @return implicit states according to the map
     */
    private State[][] createImplicitStates(Tile[][] map, int mapWidth, int mapHeight) {
        State[][] states = new State[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                states[i][j] = new State(i, j);
                if (map[i][j].getType() != TileType.PATH && map[i][j].getType() != TileType.END)
                    states[i][j].setVisited(true);
            }
        }
        return states;
    }

    /**
     * looks around adds possible route to the queue
     * @param state current state that we are in
     * @param states map that is represented by states
     * @param queue queue of states
     * @param mapWidth width of map
     * @param mapHeight height of map
     */
    private void bfsStep(State state, State[][] states, LinkedList<State> queue, int mapWidth, int mapHeight) {
        //left tile
        if (state.getPoint().getX() - 1 >= 0 &&
                !states[state.getPoint().getX() - 1][state.getPoint().getY()].isVisited()) {
            states[state.getPoint().getX() - 1][state.getPoint().getY()].setFrom(state);
            states[state.getPoint().getX() - 1][state.getPoint().getY()].setVisited(true);
            queue.addLast(states[state.getPoint().getX() - 1][state.getPoint().getY()]);
        }
        //right tile
        if (state.getPoint().getX() + 1 < mapWidth &&
                !states[state.getPoint().getX() + 1][state.getPoint().getY()].isVisited()) {
            states[state.getPoint().getX() + 1][state.getPoint().getY()].setFrom(state);
            states[state.getPoint().getX() + 1][state.getPoint().getY()].setVisited(true);
            queue.addLast(states[state.getPoint().getX() + 1][state.getPoint().getY()]);
        }
        //top tile
        if (state.getPoint().getY() - 1 >= 0
                && !states[state.getPoint().getX()][state.getPoint().getY() - 1].isVisited()) {
            states[state.getPoint().getX()][state.getPoint().getY() - 1].setFrom(state);
            states[state.getPoint().getX()][state.getPoint().getY() - 1].setVisited(true);
            queue.addLast(states[state.getPoint().getX()][state.getPoint().getY() - 1]);
        }
        //bottom tile
        if (state.getPoint().getY() + 1 < mapHeight
                && !states[state.getPoint().getX()][state.getPoint().getY() + 1].isVisited()) {
            states[state.getPoint().getX()][state.getPoint().getY() + 1].setFrom(state);
            states[state.getPoint().getX()][state.getPoint().getY() + 1].setVisited(true);
            queue.addLast(states[state.getPoint().getX()][state.getPoint().getY() + 1]);
        }
    }

    /**
     * @param map level map
     * @param mapWidth width of map
     * @param mapHeight height of map
     * @param startX x position of starting tile
     * @param startY y position of starting tile
     * @return returns length of fastest path to the end tile
     */
    public int findPathLength(Tile[][] map, int mapWidth, int mapHeight, int startX, int startY) {
        LinkedList<Point> list = findPath(map, mapWidth, mapHeight, startX, startY);
        return  list != null ? list.size() - 1 : 0;
    }

}
