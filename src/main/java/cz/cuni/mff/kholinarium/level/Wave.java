package cz.cuni.mff.kholinarium.level;

import cz.cuni.mff.kholinarium.monsters.EnemyNest;

import java.util.LinkedList;

/**
 * Wave is a model class that represents one wave with multiple enemy nests.<br>
 * <br>
 * Created by Adam Hornáček on 22/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class Wave {
    private LinkedList<EnemyNest> nests = new LinkedList<EnemyNest>();

    public LinkedList<EnemyNest> getNests() {
        return nests;
    }
}
