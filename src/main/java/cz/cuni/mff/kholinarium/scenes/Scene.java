package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;

import java.awt.event.MouseEvent;

/**
 * Scene interface implements all the must have methods that every scene needs to implement.<br>
 * <br>
 * Created by Adam Hornáček on 09/05/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
interface Scene {

    void update(long delta);

    void render(GL2 gl, Drawer drawer, int width, int height);

    void reshape(int width, int height);

    void mouseMoved(MouseEvent e, int width, int height);

    int mouseClicked(MouseEvent e, int width, int height);

}
