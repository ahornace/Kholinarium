package cz.cuni.mff.kholinarium.scenes;

/**
 * Bounding box is a model class for bounding box.<br>
 * <br>
 * Created by Adam Hornáček on 07/03/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class BoundingBox {

    float startX;
    float endX;
    float startY;
    float endY;
    int id;

    public BoundingBox(float startX, float endX, float startY, float endY,int id) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
        this.id = id;
    }
}