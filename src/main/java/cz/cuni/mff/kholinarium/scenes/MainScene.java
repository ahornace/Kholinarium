package cz.cuni.mff.kholinarium.scenes;


import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import cz.cuni.mff.kholinarium.Main;
import cz.cuni.mff.kholinarium.level.Level;
import cz.cuni.mff.kholinarium.level.LevelLoader;

import java.awt.*;

import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Main scene is a wrapper class for every scene. Main scene loads appropriate scenes, updates and renders them
 * appropriately.<br>
 * <br>
 * Created by Adam Hornáček on 22/04/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class MainScene implements GLEventListener, MouseListener, MouseMotionListener, KeyListener {

    public static final int START_NEXT_LEVEL = 1;
    public static final int TRY_AGAIN = 2;
    public static final int UNLOCK_LVL_AND_GO_TO_MENU = 3;
    public static final int GO_TO_MENU = 4;

    private MenuScene menuScene;
    private IntroScene introScene;
    private Scene currentScene;

    private boolean isInGame = false;
    private boolean isIntroShowing = true;

    private long lastTime;
    private int width;
    private int height;
    private Drawer drawer;

    private int currentLevel;
    private LevelLoader levelLoader;

    /**
     * initializes main scene = loads all the important data before rendering e.g. textures
     * @param glAutoDrawable GLAutoDrawable instance for GL2 instantiation
     */
    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        GL2 gl = glAutoDrawable.getGL().getGL2();
        gl.glEnable(GL2.GL_TEXTURE_2D);
        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glColor4f(1f, 1f, 1f, 1f);
        R.loadTextures(gl);
        drawer = new Drawer();
        introScene = new IntroScene();
        currentScene = introScene;
        levelLoader = new LevelLoader();
        lastTime = System.nanoTime();
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {

    }

    /**
     * method that fpsAnimator call 60 times per second so we firstly update the scene and then render it
     * @param glAutoDrawable GLAutoDrawable instance for GL2 instantiation
     */
    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl = glAutoDrawable.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glColor4f(1, 1, 1, 1);
        long currentTime = System.nanoTime();
        currentScene.update(currentTime - lastTime);
        lastTime = currentTime;
        currentScene.render(gl, drawer, width, height);
        gl.glFlush();
    }

    /**
     * method that is called every time the frame is reshaped so we can update width and height of the frame and update
     * dimensions
     * @param glAutoDrawable used to create GL2 instance
     * @param x viewport x-coord in pixel units
     * @param y viewport y-coord in pixel units
     * @param width new frame width
     * @param height new frame height
     */
    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl = glAutoDrawable.getGL().getGL2();
        this.width = width;
        this.height = height;
        R.loadDimensions(width, height);

        drawer.reshape();
        currentScene.reshape(width, height);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();

        int widthHalf = width / 2;
        int heightHalf = height / 2;

        gl.glOrthof(-widthHalf, widthHalf, -heightHalf, heightHalf, 0f, 1f);
        gl.glViewport(0, 0, width, height);

    }

    /**
     * runs game with selected level name
     * @param levelNumber number of the level to be run
     */
    private void runGameWithLevel(int levelNumber) {
        Level level = levelLoader.parseLevel("level" + levelNumber + ".xml");
        int tileDimension = R.getDimension("tileDimension");
        int topBarHeight = R.getDimension("topBarFull");
        int w = level.getMapWidth() * tileDimension;
        int h = level.getMapHeight() * tileDimension + topBarHeight;
        if (Main.isRetina) {
            Main.changeFrameSize(new Dimension(w / 2, h / 2));
        } else {
            Main.changeFrameSize(new Dimension(w, h));
        }
        GameScene gameScene;
        if (levelNumber % 3 == 0) {
            gameScene = new BossScene(level, w, h);
        } else {
            gameScene = new GameScene(level, w, h);
        }
        currentScene = gameScene;
        lastTime = System.nanoTime();
        isInGame = true;
    }

    /**
     * mouse clicked called by mouse listener which runs mouseClicked method of currentScene and acts according to the
     * results returns back to menu or starts new level
     * @param e MouseEvent instance
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int result = currentScene.mouseClicked(e, width, height);
        if (isInGame) {
            switch (result) {
                case START_NEXT_LEVEL:
                    if (currentLevel < 9) {
                        currentLevel++;
                        int maxUnlockedLevel = Main.getIntPreference("maxUnlockedLevel");
                        if (maxUnlockedLevel < currentLevel) {
                            Main.putIntPreference("maxUnlockedLevel", currentLevel);
                        }
                        runGameWithLevel(currentLevel);
                    } else { //starts main menu
                        isInGame = false;
                        menuScene.drawBaseMainMenu();
                        currentScene = menuScene;
                        int w = 512;
                        int h = 400;
                        if (Main.getBooleanPreference("hiResEnabled")) {
                            w *= 2;
                            h *= 2;
                        }
                        if (Main.isRetina) {
                            Main.changeFrameSize(new Dimension(w / 2, h / 2));
                        } else {
                            Main.changeFrameSize(new Dimension(w, h));
                        }
                    }
                    break;
                case TRY_AGAIN:
                    runGameWithLevel(currentLevel);
                    break;
                case UNLOCK_LVL_AND_GO_TO_MENU: //won level but is going to main menu
                    isInGame = false;
                    if (currentLevel < 9) {
                        currentLevel++;
                        int maxUnlockedLevel = Main.getIntPreference("maxUnlockedLevel");
                        if (maxUnlockedLevel < currentLevel) {
                            Main.putIntPreference("maxUnlockedLevel", currentLevel);
                        }
                    }
                    menuScene.drawBaseMainMenu();
                    currentScene = menuScene;
                    int w = 512;
                    int h = 400;
                    if (Main.getBooleanPreference("hiResEnabled")) {
                        w *= 2;
                        h *= 2;
                    }
                    if (Main.isRetina) {
                        Main.changeFrameSize(new Dimension(w / 2, h / 2));
                    } else {
                        Main.changeFrameSize(new Dimension(w, h));
                    }
                    break;
                case GO_TO_MENU: //defeated and is going to main menu
                    isInGame = false;
                    menuScene.drawBaseMainMenu();
                    currentScene = menuScene;
                    w = 512;
                    h = 400;
                    if (Main.getBooleanPreference("hiResEnabled")) {
                        w *= 2;
                        h *= 2;
                    }
                    if (Main.isRetina) {
                        Main.changeFrameSize(new Dimension(w / 2, h / 2));
                    } else {
                        Main.changeFrameSize(new Dimension(w, h));
                    }
                    break;
            }
        } else {
            if (result > 0) { //someone picked level to start
                runGameWithLevel(result);
                currentLevel = result;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        currentScene.mouseMoved(e, width, height);
    }

    /**
     * key typed called by key listener – anything typed skips the intro scene and runs the menu scenu
     * @param e – instance of KeyEvent
     */
    @Override
    public void keyTyped(java.awt.event.KeyEvent e) {
        if (isIntroShowing) {
            menuScene = new MenuScene(width, height);
            introScene.stopMusic();
            currentScene = menuScene;
            isIntroShowing = false;
        }
    }

    @Override
    public void keyPressed(java.awt.event.KeyEvent e) {

    }

    @Override
    public void keyReleased(java.awt.event.KeyEvent e) {

    }
}
