package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.awt.TextRenderer;
import cz.cuni.mff.kholinarium.Main;
import cz.cuni.mff.kholinarium.monsters.Monster;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Drawer class is a helpful class that implements most of the necessary drawing methods for every scene.<br>
 * <br>
 * Created by Adam Hornáček on 06/03/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */

final class Drawer {

    private int moneyX;
    private int moneyY;
    private int waveInfoX;
    private int waveInfoY;
    private TextRenderer renderer;
    private TextRenderer smallRenderer;
    private TextRenderer priceRenderer;
    private TextRenderer levelPickRenderer;
    private TextRenderer levelPickShadowRenderer;

    private Color greenPriceColor = new Color(74, 156, 59);
    private Color redPriceColor = new Color(205, 103, 75);

    public Drawer() {
        reshape();
    }

    /**
     * called when frame is resized – changes text renderers dimensions
     */
    public void reshape() {
        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, this.getClass().
                    getResourceAsStream("/font/UbuntuTitling-Bold.ttf"));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1); //TODO show error
        } catch (FontFormatException e) {
            e.printStackTrace();
            System.exit(-1); //TODO show some message
        }
        //initializes text renderers
        if (Main.getBooleanPreference("hiResEnabled")) {
            renderer = new TextRenderer(font.deriveFont(48F), true, true);
            smallRenderer = new TextRenderer(font.deriveFont(36F), true, true);
            priceRenderer = new TextRenderer(font.deriveFont(26F), true, true);
            levelPickRenderer = new TextRenderer(font.deriveFont(40F), true, true);
            levelPickShadowRenderer = new TextRenderer(font.deriveFont(40F), true, true);
        } else {
            renderer = new TextRenderer(font.deriveFont(24F), true, true);
            smallRenderer = new TextRenderer(font.deriveFont(18F), true, true);
            priceRenderer = new TextRenderer(font.deriveFont(13F), true, true);
            levelPickRenderer = new TextRenderer(font.deriveFont(20F), true, true);
            levelPickShadowRenderer = new TextRenderer(font.deriveFont(20F), true, true);
        }
        renderer.setColor(0.37254f, 0.37254f, 0.37254f, 1);
        smallRenderer.setColor(0.37254f, 0.37254f, 0.37254f, 1);
        levelPickRenderer.setColor(0.24313f, 0.25882f, 0.16078f, 1);
        levelPickShadowRenderer.setColor(0.87450f, 0.90196f, 0.76078f, 1);
    }

    /**
     * draws money bar – where the money text is put into
     * @param gl instance of GL2
     */
    public void drawMoneyBar(GL2 gl) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, R.getTexture("moneyBar"));
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2i(R.getDimension("moneyBarEndX"), R.getDimension("topBarCenter") +
                R.getDimension("moneyBarHeightHalf"));
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2i(R.getDimension("moneyBarEndX"), R.getDimension("topBarCenter") -
                R.getDimension("moneyBarHeightHalf"));
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2i(R.getDimension("moneyBarEndX") - R.getDimension("moneyBarWidth"),
                R.getDimension("topBarCenter") - R.getDimension("moneyBarHeightHalf"));
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2i(R.getDimension("moneyBarEndX") - R.getDimension("moneyBarWidth"),
                R.getDimension("topBarCenter") + R.getDimension("moneyBarHeightHalf"));
        gl.glEnd();
    }

    /**
     * draws level bar – where the wave number and wave progress is displayed
     * @param gl instance of GL2
     */
    public void drawLevelBar(GL2 gl) {
        drawRectangleWithCenterAt(gl, 0, R.getDimension("topBarCenter"), R.getDimension("levelBarWidthHalf"),
                R.getDimension("levelBarHeightHalf"), R.getTexture("levelBar"));
    }

    /**
     * draws wave progress
     * @param gl instance of GL2
     * @param percentage how much percent should be drawn from the bar
     */
    public void drawLevelBarStatus(GL2 gl, float percentage) {
        drawRectangleWithTopLeftAtFloat(gl, R.getDimension("levelBarStatusStart"),
                R.getDimension("topBarCenter") + R.getDimension("levelBarStatusHeightHalf"),
                R.getDimension("levelBarStatusLength") * percentage, R.getDimension("levelBarStatusHeight"),
                R.getTexture("levelBarStatus"));
    }

    /**
     * draws hearts – number of lives left
     * @param gl instance of GL2
     * @param lives number of lives left
     */
    public void drawHearts(GL2 gl, int lives) {
        switch (lives) {
            default:
                drawHeartRow(gl, R.getTexture("heartNone"), R.getTexture("heartNone"), R.getTexture("heartNone"));
                break;
            case 1:
                drawHeartRow(gl, R.getTexture("heartBroken"), R.getTexture("heartNone"), R.getTexture("heartNone"));
                break;
            case 2:
                drawHeartRow(gl, R.getTexture("heart"), R.getTexture("heartNone"), R.getTexture("heartNone"));
                break;
            case 3:
                drawHeartRow(gl, R.getTexture("heart"), R.getTexture("heartBroken"), R.getTexture("heartNone"));
                break;
            case 4:
                drawHeartRow(gl, R.getTexture("heart"), R.getTexture("heart"), R.getTexture("heartNone"));
                break;
            case 5:
                drawHeartRow(gl, R.getTexture("heart"), R.getTexture("heart"), R.getTexture("heartBroken"));
                break;
            case 6:
                drawHeartRow(gl, R.getTexture("heart"), R.getTexture("heart"), R.getTexture("heart"));
        }
    }

    /**
     * draws heart row – three hearts each with possibly different texture
     * @param gl instance of GL2
     * @param t1 first heart texture
     * @param t2 second heart texture
     * @param t3 third heart texture
     */
    private void drawHeartRow(GL2 gl, int t1, int t2, int t3) {
        drawRectangleWithCenterAt(gl, R.getDimension("firstHeartPosition"), R.getDimension("topBarCenter"),
                R.getDimension("heartWidthHalf"), R.getDimension("heartHeightHalf"), t1);
        drawRectangleWithCenterAt(gl, R.getDimension("secondHeartPosition"), R.getDimension("topBarCenter"),
                R.getDimension("heartWidthHalf"), R.getDimension("heartHeightHalf"), t2);
        drawRectangleWithCenterAt(gl, R.getDimension("thirdHeartPosition"), R.getDimension("topBarCenter"),
                R.getDimension("heartWidthHalf"), R.getDimension("heartHeightHalf"), t3);
    }

    /**
     * draws top bar – wooden background
     * @param gl instance of GL2
     * @param widthHalf half of the frame width
     * @param heightHalf half of the frame height
     */
    public void drawTopBar(GL2 gl, int widthHalf, int heightHalf) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, R.getTexture("topBar"));
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2i(widthHalf, heightHalf);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2i(widthHalf, R.getDimension("topBar"));
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2i(-widthHalf, R.getDimension("topBar"));
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2i(-widthHalf, heightHalf);
        gl.glEnd();
    }

    /**
     * draws laser that laser tower shoots
     * @param gl instance of GL2
     * @param x1 x axis position of start of laser
     * @param y1 y axis position of start of laser
     * @param x2 x axis position of end of laser
     * @param y2 y axis position of end of laser
     */
    public void drawLine(GL2 gl, int x1, int y1, int x2, int y2) {
        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glLineWidth(R.getDimension("laserWidth"));
        gl.glBegin(GL.GL_LINES);
        gl.glVertex2i(x1, y1);
        gl.glVertex2i(x2, y2);
        gl.glEnd();
        gl.glDisable(GL2.GL_LINE_SMOOTH);
        gl.glEnable(GL.GL_TEXTURE_2D);
    }

    /**
     * draws money info into money bar
     * @param currentMoney current money to draw
     * @param width frame width
     * @param height frame height
     * @param widthHalf half of the frame width
     * @param moneyChanged indicates if money changed
     */
    public void drawMoneyInfo(int currentMoney, int width, int height, int widthHalf, boolean moneyChanged) {
        String s = "" + currentMoney;
        TextRenderer renderer = smallRenderer;
        if (currentMoney > 9999) { //text should be smaller because it won't fit the money bar
            renderer = priceRenderer;
            renderer.setColor(0.37254f, 0.37254f, 0.37254f, 1);
        }
        if (moneyChanged) { //if money changed then compute the position of text to draw
            Rectangle2D rect = renderer.getBounds(s);
            moneyX = widthHalf + R.getDimension("moneyBarEndX") - R.getDimension("moneyAlign")
                    - (int) rect.getWidth();
            moneyY = height - R.getDimension("topBarHalf") - (int) (rect.getHeight() / 2);
        }
        drawFont(s, moneyX, moneyY, width, height, renderer);
    }

    /**
     * draws wave info into level bar
     * @param waves number of waves
     * @param width frame width
     * @param height frame height
     * @param widthHalf half of the frame width
     * @param wavesChanged indicates if wave number changed
     */
    public void drawWaveInfo(int waves, int width, int height, int widthHalf, boolean wavesChanged) {
        String s = "" + waves;
        if (wavesChanged) { //if wave number changed then compute the position of the text to draw
            Rectangle2D rect = renderer.getBounds(s);
            waveInfoX = widthHalf + R.getDimension("waveTextPosition") - (int) (rect.getWidth() / 2);
            waveInfoY = height - R.getDimension("topBarHalf") - (int) (rect.getHeight() / 2);
        }
        drawFont(s, waveInfoX, waveInfoY, width, height, renderer);
    }

    /**
     * draws font at given position and renderer
     * @param s text to render
     * @param x x position of text
     * @param y y position of text
     * @param width frame width
     * @param height frame height
     * @param renderer renderer to render the text with
     */
    private void drawFont(String s, int x, int y, int width, int height, TextRenderer renderer) {
        renderer.beginRendering(width, height);
        renderer.draw(s, x, y);
        renderer.endRendering();
    }

    /**
     * draws upgrade bar price
     * @param s text to render
     * @param x x position of text
     * @param y y position of text
     * @param width frame width
     * @param height frame height
     * @param color color of the text
     */
    private void drawUpgradeBarPriceAlignedToRight(String s, int x, int y, int width, int height, Color color) {
        priceRenderer.setColor(color);
        priceRenderer.beginRendering(width, height);
        Rectangle2D rect = priceRenderer.getBounds(s);
        priceRenderer.draw(s, width / 2 + x - (int) rect.getWidth(), height / 2 + y - (int) (rect.getHeight() / 2));
        priceRenderer.endRendering();
    }

    /**
     * draws font aligned to right
     * @param s text to render
     * @param x x position of text
     * @param y y position of text
     * @param width frame width
     * @param height frame height
     * @param renderer renderer to render text with
     */
    public void drawFontAlignedToRight(String s, int x, int y, int width, int height, TextRenderer renderer) {
        renderer.beginRendering(width, height);
        Rectangle2D rect = renderer.getBounds(s);
        renderer.draw(s, width * x / 2 - (int) rect.getWidth(), height * y / 2 - (int) (rect.getHeight() / 2));
        renderer.endRendering();
    }

    /**
     * @param text linked list that contains words of text
     * @param width frame width
     * @param height frame height
     * @param maxTextWidth maximum width of the line we want to render
     * @param renderer renderer that we want to render text with
     * @return returns sentence from text that is not longer than maxTextWidth
     */
    public List<String> getLinesOfText(List<String> text, int width, int height, int maxTextWidth,
                                      TextRenderer renderer) {

        if (renderer == null) {
            renderer = this.smallRenderer;
        }
        String s = "";
        renderer.beginRendering(width, height);

        List<String> lines = new LinkedList<>();
        for (String word : text) {
            if (renderer.getBounds(s.isEmpty() ? s + word : s + " " + word).getWidth() < maxTextWidth) {
                if (s.isEmpty()) {
                    s += word;
                } else {
                    s += " " + word;
                }
            } else {
                lines.add(s);
                s = word;
            }
        }

        if (!s.isEmpty()) {
            lines.add(s);
        }

        renderer.endRendering();
        return lines;
    }

    /**
     * renders line of text on the screen
     * @param s text to render
     * @param x x centered position of text
     * @param y y centered position of text
     * @param width frame width
     * @param height frame height
     * @param renderer renderer to render text with
     * @param color one component of color – the color will always be gray
     * @param alpha alpha of the text color
     */
    public void drawLineOfText(String s, int x, int y, int width, int height, TextRenderer renderer, float color,
                               float alpha) {
        if (renderer == null)
            renderer = this.smallRenderer;

        renderer.setColor(color, color, color, alpha);
        drawFontCentered(s, x, y, width, height, renderer);
    }

    /**
     * renders text at center
     * @param s text to render
     * @param x x centered position of text
     * @param y y centered position of text
     * @param width frame width
     * @param height frame height
     * @param renderer renderer to render the text with
     */
    private void drawFontCentered(String s, int x, int y, int width, int height, TextRenderer renderer) {
        renderer.beginRendering(width, height);
        Rectangle2D rect = renderer.getBounds(s);
        renderer.draw(s, x + width / 2 - (int) (rect.getWidth() / 2), y + height / 2 - (int) (rect.getHeight() / 2));
        renderer.endRendering();
    }

    /**
     * draws projectile
     * @param gl instance of GL2
     * @param x x position of tip of the arrow
     * @param y y position of tip of the arrow
     * @param angle angle the arrow is rotated at
     */
    public void drawProjectile(GL2 gl, float x, float y, float angle) {
        gl.glPushMatrix();
        gl.glBindTexture(GL2.GL_TEXTURE_2D, R.getTexture("arrow"));
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(angle, 0, 0, 1);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(0, R.getDimension("arrowHeightHalf"));
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(0, -R.getDimension("arrowHeightHalf"));
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(-R.getDimension("arrowWidth"), -R.getDimension("arrowHeightHalf"));
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(-R.getDimension("arrowWidth"), R.getDimension("arrowHeightHalf"));
        gl.glEnd();
        gl.glPopMatrix();
    }

    /**
     * popUpBar – has cancel button in the top right
     * @param gl instance of GL2
     * @param x x position of the left side of bar
     * @param y y position top side of bar
     * @param width bar width
     * @param height bar height
     * @param texture background texture of bar
     * @param cancelHover indicates if mouse is over cancel button
     */
    private void drawPopUpBar(GL2 gl, int x, int y, int width, int height, int texture, boolean cancelHover) {
        drawRectangleWithTopLeftAt(gl, x, y, width, height, texture);
        drawButtonWithTopLeftAt(gl, x + width + R.getDimension("popUpCancelDecreaseX"),
                y + R.getDimension("popUpCancelExtensionY"), R.getDimension("cancelButtonWidth"),
                R.getDimension("cancelButtonHeight"), cancelHover, R.getTexture("cancelBar"),
                R.getTexture("cancelBarHover"));
    }

    /**
     * draws button with top left at position given in parameters
     * @param gl instance of GL2
     * @param x x position of the left side of button
     * @param y y position of top side of button
     * @param dx width of the button
     * @param dy height of the button
     * @param hover indicates whether mouse is over this button or not
     * @param texture ordinary texture of button
     * @param hoverTexture hover texture of button
     */
    private void drawButtonWithTopLeftAt(GL2 gl, int x, int y, int dx, int dy, boolean hover, int texture,
                                         int hoverTexture) {
        if (hover)
            drawRectangleWithTopLeftAt(gl, x, y, dx, dy, hoverTexture);
        else
            drawRectangleWithTopLeftAt(gl, x, y, dx, dy, texture);
    }

    /**
     * draws button with center at position given in parameters
     * @param gl instance of GL2
     * @param x x position of center of button
     * @param y y position of center of button
     * @param dx button width
     * @param dy button height
     * @param hover indicates whether mouse is over this button or not
     * @param texture ordinary texture of button
     * @param hoverTexture hover texture of button
     */
    private void drawButtonWithCenterAt(GL2 gl, int x, int y, int dx, int dy, boolean hover, int texture,
                                        int hoverTexture) {
        if (hover)
            drawRectangleWithCenterAt(gl, x, y, dx, dy, hoverTexture);
        else
            drawRectangleWithCenterAt(gl, x, y, dx, dy, texture);
    }

    /**
     * draws monster
     * @param gl instance of GL2
     * @param x x position of center of monster
     * @param y y position of center of monster
     * @param type monster type
     * @param frozen indicates if monster is frozen – apply different texture
     */
    public void drawMonster(GL2 gl, float x, float y, int type, boolean frozen) {
        if (frozen) {
            switch (type) {
                case Monster.FIRE_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("fireEnemyFrozenWidthHalf"),
                            R.getDimension("fireEnemyFrozenHeightHalf"), R.getTexture("fireEnemyFrozen"));
                    break;
                case Monster.ICE_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("iceEnemyFrozenWidthHalf"),
                            R.getDimension("iceEnemyFrozenHeightHalf"), R.getTexture("iceEnemyFrozen"));
                    break;
                case Monster.SLIME_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("slimeEnemyFrozenWidthHalf"),
                            R.getDimension("slimeEnemyFrozenHeightHalf"), R.getTexture("slimeEnemyFrozen"));
                    break;
                case Monster.FIRE_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("fireEnemyFrozenWidthHalf"),
                            R.getDimension("fireEnemyFrozenHeightHalf"), R.getTexture("fireEnemyFrozen"),
                            1.25f);
                    break;
                case Monster.SLIME_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("slimeEnemyFrozenWidthHalf"),
                            R.getDimension("slimeEnemyFrozenHeightHalf"), R.getTexture("slimeEnemyFrozen"),
                            1.25f);
                    break;
                case Monster.ICE_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("iceEnemyFrozenWidthHalf"),
                            R.getDimension("iceEnemyFrozenHeightHalf"), R.getTexture("iceEnemyFrozen"),
                            1.25f);
                    break;
            }
        } else {
            switch (type) {
                case Monster.FIRE_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("fireEnemyWidthHalf"),
                            R.getDimension("fireEnemyHeightHalf"), R.getTexture("fireEnemy"));
                    break;
                case Monster.ICE_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("iceEnemyWidthHalf"),
                            R.getDimension("iceEnemyHeightHalf"), R.getTexture("iceEnemy"));
                    break;
                case Monster.SLIME_MONSTER:
                    drawRectangleWithCenterAtFloat(gl, x, y, R.getDimension("slimeEnemyWidthHalf"),
                            R.getDimension("slimeEnemyHeightHalf"), R.getTexture("slimeEnemy"));
                    break;
                case Monster.FIRE_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("fireEnemyWidthHalf"),
                            R.getDimension("fireEnemyHeightHalf"), R.getTexture("fireEnemy"),
                            1.25f);
                    break;
                case Monster.SLIME_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("slimeEnemyWidthHalf"),
                            R.getDimension("slimeEnemyHeightHalf"), R.getTexture("slimeEnemy"),
                            1.25f);
                    break;
                case Monster.ICE_MONSTER_BOSS:
                    drawRectangleWithCenterAtFloatWScale(gl, x, y, R.getDimension("iceEnemyWidthHalf"),
                            R.getDimension("iceEnemyHeightHalf"), R.getTexture("iceEnemy"),
                            1.25f);
                    break;
            }
        }
    }

    /**
     * draws rectangle with center at given position with scale given as parameter
     * @param gl instance of GL2
     * @param x x position of center of rectangle
     * @param y y position of center of rectangle
     * @param dxHalf half of the width of the rectangle
     * @param dyHalf half of the height of the rectangle
     * @param texture texture to apply to the rectangle
     * @param scale scale by which to scale the rectangle
     */
    private void drawRectangleWithCenterAtFloatWScale(GL2 gl, float x, float y, int dxHalf, int dyHalf, int texture,
                                                      float scale) {
        gl.glPushMatrix();
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl.glTranslatef(x, y, 0);
        gl.glScalef(scale, scale, 1);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(dxHalf, dyHalf);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(dxHalf, -dyHalf);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(-dxHalf, -dyHalf);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(-dxHalf, dyHalf);
        gl.glEnd();
        gl.glPopMatrix();
    }

    /**
     * draws rectangle with top left at position with float parameters
     * @param gl instance of GL2
     * @param x x position of the left side of rectangle
     * @param y y position of the top side of rectangle
     * @param dx rectangle width
     * @param dy rectangle height
     * @param texture texture of the rectangle
     */
    private void drawRectangleWithTopLeftAtFloat(GL2 gl, float x, float y, float dx, float dy, int texture) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(x + dx, y);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(x + dx, y - dy);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(x, y - dy);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(x, y);
        gl.glEnd();
    }

    /**
     * draws rectangle with top left at position with int parameters
     * @param gl instance of GL2
     * @param x x position of the left side of rectangle
     * @param y y position of the top side of rectangle
     * @param dx rectangle width
     * @param dy rectangle height
     * @param texture texture of the rectangle
     */
    private void drawRectangleWithTopLeftAt(GL2 gl, int x, int y, int dx, int dy, int texture) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2i(x + dx, y);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2i(x + dx, y - dy);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2i(x, y - dy);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2i(x, y);
        gl.glEnd();
    }

    /**
     * draws rectangle with center at position with float parameters
     * @param gl instance of GL2
     * @param x x position of center of rectangle
     * @param y y position of center of rectangle
     * @param dxHalf half of the rectangle width
     * @param dyHalf half of the rectangle height
     * @param texture texture of the rectangle
     */
    private void drawRectangleWithCenterAtFloat(GL2 gl, float x, float y, int dxHalf, int dyHalf, int texture) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(x + dxHalf, y + dyHalf);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(x + dxHalf, y - dyHalf);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(x - dxHalf, y - dyHalf);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(x - dxHalf, y + dyHalf);
        gl.glEnd();
        gl.glPopMatrix();
    }

    /**
     * draws rectangle with center at position with float parameters
     * @param gl instance of GL2
     * @param x x position of center of rectangle
     * @param y y position of center of rectangle
     * @param dxHalf half of the rectangle width
     * @param dyHalf half of the rectangle height
     * @param texture texture of the rectangle
     */
    public void drawRectangleWithCenterAt(GL2 gl, int x, int y, int dxHalf, int dyHalf, int texture) {
        gl.glBindTexture(GL.GL_TEXTURE_2D, texture);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2i(x + dxHalf, y + dyHalf);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2i(x + dxHalf, y - dyHalf);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2i(x - dxHalf, y - dyHalf);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2i(x - dxHalf, y + dyHalf);
        gl.glEnd();
    }

    /**
     * draws laser tower at position
     * @param gl instance of GL2
     * @param x x position of center of tower
     * @param y y position of center of tower
     * @param texture texture of tower
     */
    public void drawLaserTower(GL2 gl, int x, int y, int texture) {
        drawRectangleWithCenterAt(gl, x, y,
                R.getDimension("laserTowerWidthHalf"), R.getDimension("laserTowerHeightHalf"), texture);
    }

    /**
     * draws freezing tower at position
     * @param gl instance of GL2
     * @param x x position of center of tower
     * @param y y position of center of tower
     * @param texture – texture of tower
     */
    public void drawFreezingTower(GL2 gl, int x, int y, int texture) {
        drawRectangleWithCenterAt(gl, x, y,
                R.getDimension("freezingTowerWidthHalf"), R.getDimension("freezingTowerHeightHalf"),
                texture);
    }

    /**
     * draws arrow tower at position
     * @param gl instance of GL2
     * @param x x position of center of tower
     * @param y y position of center of tower
     * @param angle angle the tower is rotated at
     * @param texture texture of tower
     */
    public void drawArrowTower(GL2 gl, float x, float y, float angle, int texture) {
        gl.glPushMatrix();
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl.glTranslatef(x, y, 0);
        gl.glRotatef(angle, 0, 0, 1);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(R.getDimension("arrowTowerDimensionXHalf"),
                R.getDimension("arrowTowerDimensionYHalf"));
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(R.getDimension("arrowTowerDimensionXHalf"),
                -R.getDimension("arrowTowerDimensionYHalf"));
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(-R.getDimension("arrowTowerDimensionXHalf"),
                -R.getDimension("arrowTowerDimensionYHalf"));
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(-R.getDimension("arrowTowerDimensionXHalf"),
                R.getDimension("arrowTowerDimensionYHalf"));
        gl.glEnd();
        gl.glPopMatrix();
    }

    /**
     * draws circle at position
     * @param gl instance of GL2
     * @param cx circle x position of center
     * @param cy circle y position of center
     * @param num_segments number of segments of the circle – the more the better accuracy for decreased performance
     * @param r radius of circle
     */
    public void drawCircle(GL2 gl, float cx, float cy, int num_segments, float r) {
        //TODO optimize! – no need to compute vertices every time
        float theta = (float) (2 * Math.PI / num_segments);
        float c = (float) Math.cos(theta); //precalculate the sine and cosine
        float s = (float) Math.sin(theta);
        float t;

        float x = r; //we start at angle = 0
        float y = 0;

        gl.glColor4f(0.2784f, 0.7137f, 1, 0.2f);
        gl.glDisable(GL.GL_TEXTURE_2D);
        gl.glEnable(GL2.GL_LINE_SMOOTH);
        gl.glBegin(GL2.GL_TRIANGLE_FAN);
        gl.glVertex2f(cx, cy); //middle of the circle
        for(int i = 0; i < num_segments; i++)
        {
            gl.glVertex2f(x + cx, y + cy); //output vertex
            //apply the rotation matrix
            t = x;
            x = c * x - s * y;
            y = s * t + c * y;
        }
        gl.glVertex2f(cx + r, cy); //once again the start of the circle to have enclosed circle
        gl.glEnd();
        gl.glDisable(GL2.GL_LINE_SMOOTH);
        gl.glEnable(GL.GL_TEXTURE_2D);
    }

    /**
     * draws level pick window
     * @param gl instance of GL2
     * @param width frame width
     * @param height frame height
     * @param maxUnlockedLevel integer that indicates max unlocked level
     * @param levelPickButtonHover array of boolean variables that indicate if mouse is over one of them
     * @param menuHover indicates if mouse is over menu button
     */
    void drawLevelPick(GL2 gl, int width, int height, int maxUnlockedLevel, boolean[] levelPickButtonHover,
                       boolean menuHover) {
        //draws background
        drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("menuLevelPickBGWidthHalf"),
                R.getDimension("menuLevelPickBGHeightHalf"),
                R.getTexture("menuLevelPickBG"));
        //draws level pick buttons
        for (int i = 0; i < 9; i++) {
            int row = i / 3;
            int column = i % 3;
            if (i < maxUnlockedLevel) {
                //draws button
                drawButtonWithTopLeftAt(gl,
                        R.getDimension("menuLevelPickButtonsStartX") + column *
                                R.getDimension("menuLevelPickButtonsColumnWidth"),
                        R.getDimension("menuLevelPickButtonsStartY") - row *
                                R.getDimension("menuLevelPickButtonsRowHeight"),
                        R.getDimension("menuLevelPickButtonWidth"),
                        R.getDimension("menuLevelPickButtonHeight"),
                        levelPickButtonHover[i],
                        R.getTexture("menuLevelPickButton"),
                        R.getTexture("menuLevelPickButtonHover"));
                //draws button number shadow
                drawFontCentered("" + (i + 1),
                        R.getDimension("menuLevelPickButtonsStartX") + column *
                                R.getDimension("menuLevelPickButtonsColumnWidth") +
                                R.getDimension("menuLevelPickButtonWidthHalf"),
                        R.getDimension("menuLevelPickButtonsStartY") - row *
                                R.getDimension("menuLevelPickButtonsRowHeight") -
                                R.getDimension("menuLevelPickButtonFontYFromTop") -
                                R.getDimension("onePixelHeight"),
                        width, height, levelPickShadowRenderer);
                //draws button number
                drawFontCentered("" + (i + 1),
                        R.getDimension("menuLevelPickButtonsStartX") + column *
                                R.getDimension("menuLevelPickButtonsColumnWidth") +
                                R.getDimension("menuLevelPickButtonWidthHalf"),
                        R.getDimension("menuLevelPickButtonsStartY") - row *
                                R.getDimension("menuLevelPickButtonsRowHeight") -
                                R.getDimension("menuLevelPickButtonFontYFromTop"),
                        width, height, levelPickRenderer);
                gl.glColor4f(1, 1, 1, 1);
            } else {
                drawRectangleWithTopLeftAt(gl,
                        R.getDimension("menuLevelPickButtonsStartX") + column *
                                R.getDimension("menuLevelPickButtonsColumnWidth"),
                        R.getDimension("menuLevelPickButtonsStartY") - row *
                                R.getDimension("menuLevelPickButtonsRowHeight"),
                        R.getDimension("menuLevelPickButtonWidth"),
                        R.getDimension("menuLevelPickButtonHeight"),
                        R.getTexture("menuLevelPickButtonLocked")
                );
            }
        }
        //draws main menu button
        drawButtonWithCenterAt(gl, 0, R.getDimension("menuLevelPickMenuButtonCenterY"),
                R.getDimension("menuLevelPickMenuButtonWidthHalf"), R.getDimension("menuLevelPickMenuButtonHeightHalf"),
                menuHover, R.getTexture("victoryMainMenuButton"),
                R.getTexture("victoryMainMenuButtonHover"));
    }

    /**
     * draws yes or no window when player wants to quit the game
     * @param gl instance of GL2
     * @param yesButtonHover indicates whether mouse is over yes button
     * @param noButtonHover indicates whether mouse is over no button
     */
    void drawYesOrNoWindow(GL2 gl, boolean yesButtonHover, boolean noButtonHover) {
        drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("yesOrNoWindowWidthHalf"),
                R.getDimension("yesOrNoWindowHeightHalf"), R.getTexture("yesOrNoWindow"));
        drawButtonWithTopLeftAt(gl, R.getDimension("yesButtonStartXFromCenter"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter"),
                R.getDimension("yesOrNoButtonWidth"),
                R.getDimension("yesOrNoButtonHeight"), yesButtonHover,
                R.getTexture("yesButton"), R.getTexture("yesButtonHover"));
        drawButtonWithTopLeftAt(gl, R.getDimension("noButtonStartXFromCenter"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter"),
                R.getDimension("yesOrNoButtonWidth"),
                R.getDimension("yesOrNoButtonHeight"), noButtonHover,
                R.getTexture("noButton"), R.getTexture("noButtonHover"));
    }

    /**
     * draws main menu
     * @param gl instance of GL2
     * @param startButtonHover indicates whether mouse is over start button
     * @param aboutButtonHover indicates whether mouse is over about button
     * @param quitButtonHover indicates whether mouse is over quit button
     * @param musicEnabled indicates whether music is enabled
     * @param musicSliderHover indicates whether mouse is over music slider
     * @param hiResEnabled indicates whether high resolution is enabled
     * @param hiResSliderHover indicates whether mouse is over high resolution slider
     */
    void drawMainMenu(GL2 gl, boolean startButtonHover, boolean aboutButtonHover, boolean quitButtonHover,
                      boolean musicEnabled, boolean musicSliderHover, boolean hiResEnabled,
                      boolean hiResSliderHover) {
        //draw background
        drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("menuWidthHalf"),
                R.getDimension("menuHeightHalf"), R.getTexture("mainMenu"));
        //draw main menu buttons
        drawButtonWithTopLeftAt(gl, R.getDimension("menuButtonStartX"),
                R.getDimension("menuStartButtonStartY"), R.getDimension("menuButtonWidth"),
                R.getDimension("menuButtonHeight"), startButtonHover,
                R.getTexture("menuStartButton"), R.getTexture("menuStartButtonHover"));
        drawButtonWithTopLeftAt(gl, R.getDimension("menuButtonStartX"),
                R.getDimension("menuAboutButtonStartY"), R.getDimension("menuButtonWidth"),
                R.getDimension("menuButtonHeight"), aboutButtonHover,
                R.getTexture("menuAboutButton"), R.getTexture("menuAboutButtonHover"));
        drawButtonWithTopLeftAt(gl, R.getDimension("menuButtonStartX"),
                R.getDimension("menuQuitButtonStartY"), R.getDimension("menuButtonWidth"),
                R.getDimension("menuButtonHeight"), quitButtonHover,
                R.getTexture("menuQuitButton"), R.getTexture("menuQuitButtonHover"));
        //draw music slider
        if (musicEnabled) {
            drawButtonWithTopLeftAt(gl, R.getDimension("menuMusicSliderStartX"),
                    R.getDimension("menuSliderStartY"), R.getDimension("menuSliderWidth"),
                    R.getDimension("menuSliderHeight"), musicSliderHover,
                    R.getTexture("menuSlider"), R.getTexture("menuSliderHover"));

        } else {
            drawButtonWithTopLeftAt(gl, R.getDimension("menuMusicSliderOffStartX"),
                    R.getDimension("menuSliderStartY"), R.getDimension("menuSliderWidth"),
                    R.getDimension("menuSliderHeight"), musicSliderHover,
                    R.getTexture("menuSliderOff"), R.getTexture("menuSliderOffHover"));
        }
        //draw high resolution slider
        if (hiResEnabled) {
            drawButtonWithTopLeftAt(gl, R.getDimension("menuHiResSliderStartX"),
                    R.getDimension("menuSliderStartY"), R.getDimension("menuSliderWidth"),
                    R.getDimension("menuSliderHeight"), hiResSliderHover,
                    R.getTexture("menuSlider"), R.getTexture("menuSliderHover"));
        } else {
            drawButtonWithTopLeftAt(gl, R.getDimension("menuHiResSliderOffStartX"),
                    R.getDimension("menuSliderStartY"), R.getDimension("menuSliderWidth"),
                    R.getDimension("menuSliderHeight"), hiResSliderHover,
                    R.getTexture("menuSliderOff"), R.getTexture("menuSliderOffHover"));
        }
    }

    /**
     * draws buy tower bar
     * @param gl instance of L2
     * @param x x position of left side of buy tower bar
     * @param y y position of top side of buy tower bar
     * @param cancelButtonHover indicates whether mouse is over cancel button
     * @param arrowTowerBuyButtonHover indicates whether mouse is over buy arrow tower button
     * @param laserTowerBuyButtonHover indicates whether mouse is over buy laser tower button
     * @param freezingTowerBuyButtonHover indicates whether mouse is over buy freezing tower button
     * @param canBuyArrowTower indicates if player has enough money to buy arrow tower
     * @param canBuyLaserTower indicates whether player has enough money to buy laser tower
     * @param canBuyFreezingTower indicates whether player has enough money to buy freezing tower
     */
    void drawBuyTowerBar(GL2 gl, int x, int y, boolean cancelButtonHover,
                         boolean arrowTowerBuyButtonHover, boolean laserTowerBuyButtonHover,
                         boolean freezingTowerBuyButtonHover, boolean canBuyArrowTower,
                         boolean canBuyLaserTower, boolean canBuyFreezingTower) {
        //TODO repair buy button shadows
        //draw background with cancel button
        drawPopUpBar(gl, x, y, R.getDimension("buyTowerWidth"),
                R.getDimension("buyTowerHeight"), R.getTexture("buyTowerBar"), cancelButtonHover);
        //draw buy buttons
        if (canBuyArrowTower)
            drawButtonWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                y + R.getDimension("buyButtonFirstStartY"),
                R.getDimension("buyButtonWidth"),
                R.getDimension("buyButtonHeight"), arrowTowerBuyButtonHover,
                R.getTexture("buyButton"), R.getTexture("buyButtonHover"));
        else
            drawRectangleWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                    y + R.getDimension("buyButtonFirstStartY"),
                    R.getDimension("buyButtonWidth"), R.getDimension("buyButtonHeight"),
                    R.getTexture("buyButtonInactive"));
        if (canBuyLaserTower)
            drawButtonWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                y + R.getDimension("buyButtonSecondStartY"),
                R.getDimension("buyButtonWidth"),
                R.getDimension("buyButtonHeight"), laserTowerBuyButtonHover,
                R.getTexture("buyButton"), R.getTexture("buyButtonHover"));
        else
            drawRectangleWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                    y + R.getDimension("buyButtonSecondStartY"),
                    R.getDimension("buyButtonWidth"), R.getDimension("buyButtonHeight"),
                    R.getTexture("buyButtonInactive"));
        if (canBuyFreezingTower)
            drawButtonWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                y + R.getDimension("buyButtonThirdStartY"),
                R.getDimension("buyButtonWidth"),
                R.getDimension("buyButtonHeight"), freezingTowerBuyButtonHover,
                R.getTexture("buyButton"), R.getTexture("buyButtonHover"));
        else
            drawRectangleWithTopLeftAt(gl, x + R.getDimension("buyButtonStartX"),
                    y + R.getDimension("buyButtonThirdStartY"),
                    R.getDimension("buyButtonWidth"), R.getDimension("buyButtonHeight"),
                    R.getTexture("buyButtonInactive"));
    }

    /**
     * draws pause button
     * @param gl instance of GL2
     */
    void drawPauseButton(GL2 gl) {
        drawRectangleWithCenterAt(gl, R.getDimension("moneyBarEndX")
                        - R.getDimension("pauseButtonWidthHalf")
                        - R.getDimension("pauseButtonShadowHalf"),
                R.getDimension("pauseButtonY"), R.getDimension("pauseButtonWidthHalf") +
                        R.getDimension("pauseButtonShadowHalf"),
                R.getDimension("pauseButtonHeightHalf"), R.getTexture("pauseButton"));
    }

    /**
     * draws paused, victory or defeat window
     * @param gl instance of GL2
     * @param bgTexture background texture
     * @param leftButtonTexture texture of left button
     * @param leftButtonHoverTexture texture of left button when mouse is over it
     * @param leftButtonHover indicates whether mouse is over left button
     * @param mainMenuButtonHover indicates whether mouse is over right button
     */
    void drawPopUp2(GL2 gl, int bgTexture, int leftButtonTexture, int leftButtonHoverTexture,
                    boolean leftButtonHover, boolean mainMenuButtonHover) {
        //draw background
        drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("victoryOrDefeatBgWidthHalf"),
                R.getDimension("victoryOrDefeatBgHeightHalf"), bgTexture);
        //draw left button
        drawButtonWithTopLeftAt(gl, R.getDimension("victoryOrDefeatLeftButtonStartX"),
                R.getDimension("victoryOrDefeatButtonStartY"),
                R.getDimension("victoryOrDefeatButtonWidth"),
                R.getDimension("victoryOrDefeatButtonHeight"), leftButtonHover,
                leftButtonTexture, leftButtonHoverTexture);
        //draw right button = main menu button
        drawButtonWithTopLeftAt(gl, R.getDimension("victoryOrDefeatMenuButtonStartX"),
                R.getDimension("victoryOrDefeatButtonStartY"),
                R.getDimension("victoryOrDefeatButtonWidth"),
                R.getDimension("victoryOrDefeatButtonHeight"), mainMenuButtonHover,
                R.getTexture("victoryMainMenuButton"),
                R.getTexture("victoryMainMenuButtonHover"));
    }

    /**
     * draws upgrade bar
     * @param gl instance of GL2
     * @param x x position of left side of bar
     * @param y y position of top side of bar
     * @param width frame width
     * @param height frame height
     * @param cancelButtonHover indicates whether mouse is over cancel button
     * @param isUpgradeable indicates whether the upgrade button is drawn as active or inactive
     * @param isUpgradedToMax indicates whether tower that player wants to upgrade is upgraded to max
     * @param upgradeButtonHover indicates whether mouse is over upgrade button
     * @param sellButtonHover indicates whether mouse is over sell button
     * @param upgradePrice how much the upgrade costs
     * @param sellPrice how much the player will get if he decides to sell this tower
     */
    void drawUpgradeBar(GL2 gl, int x, int y, int width, int height,
                        boolean cancelButtonHover, boolean isUpgradeable, boolean isUpgradedToMax,
                        boolean upgradeButtonHover, boolean sellButtonHover, int upgradePrice, int sellPrice) {
        //draw background and cancel button
        drawPopUpBar(gl, x, y, R.getDimension("upgradeBarWidth"),
                R.getDimension("upgradeBarHeight"), R.getTexture("upgradeBar"), cancelButtonHover);
        //draw upgrade button and upgrade price
        if (isUpgradeable) {
            drawButtonWithTopLeftAt(gl, x + R.getDimension("upgradeBarButtonStartX"),
                    y - R.getDimension("upgradeButtonStartY"),
                    R.getDimension("upgradeBarButtonWidth"), R.getDimension("upgradeBarButtonHeight"),
                    upgradeButtonHover, R.getTexture("upgradeButton"),
                    R.getTexture("upgradeButtonHover"));
            drawUpgradeBarPriceAlignedToRight("" + upgradePrice,
                    x + R.getDimension("upgradeBarPriceEndX"),
                    y - R.getDimension("upgradeBarUpgradePriceCenterY"), width, height, greenPriceColor);
        } else {
            drawRectangleWithTopLeftAt(gl, x + R.getDimension("upgradeBarButtonStartX"),
                    y - R.getDimension("upgradeButtonStartY"),
                    R.getDimension("upgradeBarButtonWidth"), R.getDimension("upgradeBarButtonHeight"),
                    R.getTexture("upgradeButtonInactive"));

            if (isUpgradedToMax) {//TODO draw infinity font
                //drawUpgradeBarPriceAlignedToRight("\u221E",
                //        x + R.getDimension("upgradeBarPriceEndX"),
                //        y - R.getDimension("upgradeBarUpgradePriceCenterY"), width, height, greenPriceColor);
            } else
                drawUpgradeBarPriceAlignedToRight("" + upgradePrice,
                        x + R.getDimension("upgradeBarPriceEndX"),
                        y - R.getDimension("upgradeBarUpgradePriceCenterY"), width, height, greenPriceColor);
        }
        gl.glColor4f(1, 1, 1, 1);
        //draw sell button and sell price
        drawButtonWithTopLeftAt(gl, x + R.getDimension("upgradeBarButtonStartX"),
                y - R.getDimension("sellButtonStartY"),
                R.getDimension("upgradeBarButtonWidth"), R.getDimension("upgradeBarButtonHeight"),
                sellButtonHover, R.getTexture("sellButton"),
                R.getTexture("sellButtonHover"));
        drawUpgradeBarPriceAlignedToRight("" + sellPrice,
                x + R.getDimension("upgradeBarPriceEndX"),
                y - R.getDimension("upgradeBarSellPriceCenterY"), width, height, redPriceColor);
    }

    /**
     * draws tower attributes = 5 dots in a row that indicates how powerful the tower is
     * @param gl instance of GL2
     * @param upgradeBarX x position of left side of upgrade bar
     * @param upgradeBarY y position of top side of upgrade bar
     * @param upgradeButtonHover indicates whether mouse is over upgrade button
     * @param currentAttLevel number from 0-4 that indicates current attribute level of the tower
     * @param row number from 0-2 that indicates row to which draw the tower attributes
     */
    void drawTowerAttributes(GL2 gl, int upgradeBarX, int upgradeBarY, boolean upgradeButtonHover,
                             int currentAttLevel, int row) {
        int i;
        int texture = R.getTexture("yesPoint");
        for (i = 0; i < 5; i++) {
            if (upgradeButtonHover && i == currentAttLevel)
                texture = R.getTexture("point");
            else if (i >= currentAttLevel)
                texture = R.getTexture("noPoint");
            drawRectangleWithTopLeftAt(gl, upgradeBarX + R.getDimension("upgradeBarParametersStartX") +
                            i * R.getDimension("upgradeBarParametersDelimiterX"),
                    upgradeBarY - R.getDimension("upgradeBarParametersStartY") -
                            row * R.getDimension("upgradeBarParametersDelimiterY"),
                            R.getDimension("upgradeBarDotWidth"), R.getDimension("upgradeBarDotHeight"),
                            texture);
        }
    }

    /**
     * draws cloud
     * @param gl instance of GL2
     * @param x x position of center of cloud
     * @param y y position of center of cloud
     * @param widthHalf half of the width of cloud
     * @param heightHalf half of the height of cloud
     * @param texture cloud texture
     */
    void drawCloud(GL2 gl, float x, float y, int widthHalf, int heightHalf, int texture) {
        gl.glPushMatrix();
        gl.glBindTexture(GL2.GL_TEXTURE_2D, texture);
        gl.glTranslatef(x, y, 0);
        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2f(1.0f, 1.0f);
        gl.glVertex2f(widthHalf, heightHalf);
        gl.glTexCoord2f(1.0f, 0.0f);
        gl.glVertex2f(widthHalf, -heightHalf);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(-widthHalf, -heightHalf);
        gl.glTexCoord2f(0.0f, 1.0f);
        gl.glVertex2f(-widthHalf, heightHalf);
        gl.glEnd();
        gl.glPopMatrix();
    }

    /**
     * draws about window
     * @param gl instance of GL2
     * @param menuButtonHover indicates whether mouse is over main menu button
     */
    void drawAboutWindow(GL2 gl, boolean menuButtonHover) {
        drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("menuAboutWidthHalf"),
                R.getDimension("menuAboutHeightHalf"), R.getTexture("menuAbout"));
        drawButtonWithCenterAt(gl, 0, R.getDimension("menuAboutMenuButtonCenterY"),
                R.getDimension("menuLevelPickMenuButtonWidthHalf"),
                R.getDimension("menuLevelPickMenuButtonHeightHalf"), menuButtonHover,
                R.getTexture("victoryMainMenuButton"), R.getTexture("victoryMainMenuButtonHover"));
    }
}