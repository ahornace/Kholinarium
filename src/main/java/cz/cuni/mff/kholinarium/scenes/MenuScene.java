package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;
import cz.cuni.mff.kholinarium.Main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.LinkedList;

/**
 * Menu scene class represents main menu scene. It takes care for rendering and user input like clicking.<br>
 * <br>
 * Created by Adam Hornáček on 20/04/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public final class MenuScene implements Scene {

    public static final int START_LEVEL_BUTTON = 0;
    public static final int ABOUT_BUTTON = 1;
    public static final int QUIT_BUTTON = 2;
    public static final int MUSIC_SLIDER = 3;
    public static final int HI_RES_SLIDER = 4;
    public static final int YES_BUTTON = 5;
    public static final int NO_BUTTON = 6;
    public static final int MENU_BUTTON_AT_PICK_LEVEL = 7;
    public static final int MENU_BUTTON_AT_ABOUT = 8;

    private HoverListener hoverListener = new HoverListener();
    private boolean musicEnabled = Main.getBooleanPreference("musicEnabled");
    private boolean hiResEnabled = Main.getBooleanPreference("hiResEnabled");
    private boolean menuButtonHover = false;
    private boolean startButtonHover = false;
    private boolean aboutButtonHover = false;
    private boolean quitButtonHover = false;
    private boolean musicSliderHover = false;
    private boolean hiResSliderHover = false;
    private boolean yesButtonHover = false;
    private boolean noButtonHover = false;
    private boolean drawYesOrNoWindow = false;
    private boolean drawAbout = false;
    private boolean drawLevelPick = false;
    private boolean[] levelPickButtonHover = new boolean[9];

    private LinkedList<Cloud> clouds = new LinkedList<Cloud>();

    private int widthHalf;
    private int heightHalf;

    private Clip music;

    private int maxUnlockedLevel;

    public MenuScene(int width, int height) {
        widthHalf = width / 2;
        heightHalf = height / 2;
        startMusic();
        drawBaseMainMenu();
    }

    /**
     * creates clouds that are flowing on the background – sets their initial position and velocities
     */
    private void createClouds() {
        clouds.clear();
        clouds.add(new Cloud(-widthHalf + R.getDimension("cloud1StartX") + R.getDimension("cloud1WidthHalf"),
                heightHalf + R.getDimension("cloud1StartY") - R.getDimension("cloud1HeightHalf"),
                R.getDimension("cloud1WidthHalf"), R.getDimension("cloud1HeightHalf"), 10,
                R.getTexture("cloud1")));
        clouds.add(new Cloud(-widthHalf + R.getDimension("cloud2StartX") + R.getDimension("cloud2WidthHalf"),
                heightHalf + R.getDimension("cloud2StartY") - R.getDimension("cloud2HeightHalf"),
                R.getDimension("cloud2WidthHalf"), R.getDimension("cloud2HeightHalf"), -6,
                R.getTexture("cloud2")));
        clouds.add(new Cloud(-widthHalf + R.getDimension("cloud3StartX") + R.getDimension("cloud3WidthHalf"),
                heightHalf + R.getDimension("cloud3StartY") - R.getDimension("cloud3HeightHalf"),
                R.getDimension("cloud3WidthHalf"), R.getDimension("cloud3HeightHalf"), -8,
                R.getTexture("cloud3")));
        clouds.add(new Cloud(-widthHalf + R.getDimension("cloud4StartX") + R.getDimension("cloud4WidthHalf"),
                heightHalf + R.getDimension("cloud4StartY") - R.getDimension("cloud4HeightHalf"),
                R.getDimension("cloud4WidthHalf"), R.getDimension("cloud4HeightHalf"), 4,
                R.getTexture("cloud4")));
    }

    /**
     * frame size was changed so loads the thing that could go wrong again – creates clouds and restarts hover listeners
     * because if resolution was changed then the scaling was changed too
     * @param width new width of the frame
     * @param height new height of the frame
     */
    @Override
    public void reshape(int width, int height) {
        widthHalf = width / 2;
        heightHalf = height / 2;
        createClouds();
        hoverListener.resetListener();
        hoverListener.addMenuDefaultHoverListeners(); //because dimensions in resources must be instantiated
    }

    /**
     * draws base menu = shows the menu like in the beginning
     */
    void drawBaseMainMenu() {
        drawLevelPick = false;
        drawAbout = false;
        drawYesOrNoWindow = false;
        createClouds();
        hoverListener.resetListener();
        hoverListener.addMenuDefaultHoverListeners();
    }

    /**
     * updates scene according to the change of time from previous update
     * @param delta change of time in nanoseconds
     */
    public void update(long delta) {
        if (delta > 20000000) { //prevents a big lag
            delta = 20000000;
        }
        float change = (float) delta / 1000000000; //seconds that passed from previous update
        for (Cloud c : clouds) {
            c.moveByX(change);
            if (c.getX() < -widthHalf || c.getX() > widthHalf) { //cloud is outside of scope = change its direction
                c.flipDirection();
            }
        }
    }

    /**
     * renders scene so the changes of update can be seen
     * @param gl openGL instance
     * @param drawer helpful class for rendering
     * @param width frame width
     * @param height frame height
     */
    public void render(GL2 gl, Drawer drawer, int width, int height) {
        //background
        drawer.drawRectangleWithCenterAt(gl, 0, 0, width / 2, height / 2, R.getTexture("menuBackground"));
        for (Cloud c : clouds) {
            drawer.drawCloud(gl, c.getX(), c.getY(), c.getWidthHalf(), c.getHeightHalf(), c.getTexture());
        }
        //logo
        drawer.drawRectangleWithCenterAt(gl, 0, height / 2 - R.getDimension("menuLogoStartY")
                        - R.getDimension("logoHeightHalf"),
                R.getDimension("logoWidthHalf"), R.getDimension("logoHeightHalf"), R.getTexture("logo"));
        if (drawLevelPick) {
            drawer.drawLevelPick(gl, width, height, maxUnlockedLevel, levelPickButtonHover, menuButtonHover);
        } else if (drawYesOrNoWindow) {
            drawer.drawYesOrNoWindow(gl, yesButtonHover, noButtonHover);
        } else if (drawAbout) {
            drawer.drawAboutWindow(gl, menuButtonHover);
        } else {
            drawer.drawMainMenu(gl, startButtonHover, aboutButtonHover, quitButtonHover, musicEnabled, musicSliderHover,
                    hiResEnabled, hiResSliderHover);
        }
    }

    /**
     * resets hover booleans used in base main menu
     */
    private void resetMainMenuButtonHovers() {
        startButtonHover = false;
        aboutButtonHover = false;
        quitButtonHover = false;
        musicSliderHover = false;
        hiResSliderHover = false;
    }

    /**
     * starts endless game loop to be played in menu and game
     */
    private void startMusic() {
        try {
            InputStream is = this.getClass().getResourceAsStream("/sounds/music.wav");
            InputStream bufferedIn = new BufferedInputStream(is);
            AudioInputStream as = AudioSystem.getAudioInputStream(bufferedIn);
            music = AudioSystem.getClip();
            music.open(as);
            if (musicEnabled) {
                music.loop(Clip.LOOP_CONTINUOUSLY);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * checks where we clicked and acts according to it
     * @param e MouseEvent instance
     * @param width frame width
     * @param height frame height
     * @return special constant for main scene to do according to the constant
     */
    @Override
    public int mouseClicked(MouseEvent e, int width, int height) {
        int x = e.getX();
        int y = e.getY();
        int clickedX, clickedY;
        if (Main.isRetina) {
            clickedX = x * 2 - width / 2;
            clickedY = height / 2 - y * 2;
        } else {
            clickedX = x - width / 2;
            clickedY = height / 2 - y;
        }
        BoundingBox box;
        if ((box = hoverListener.getElementAt(clickedX, clickedY)) != null) {
            switch (box.id) {
                case START_LEVEL_BUTTON:
                    drawLevelPick = true;
                    resetMainMenuButtonHovers();
                    hoverListener.resetListener();
                    maxUnlockedLevel = Main.getIntPreference("maxUnlockedLevel");
                    hoverListener.addLevelPickHoverListeners(maxUnlockedLevel);
                    break;
                case ABOUT_BUTTON:
                    drawAbout = true;
                    resetMainMenuButtonHovers();
                    hoverListener.resetListener();
                    hoverListener.addAboutWindowHoverListener();
                    break;
                case QUIT_BUTTON:
                    drawYesOrNoWindow = true;
                    resetMainMenuButtonHovers();
                    hoverListener.resetListener();
                    hoverListener.addYesOrNoHoverListeners();
                    break;
                case MUSIC_SLIDER:
                    musicEnabled = !musicEnabled;
                    Main.putBooleanPreference("musicEnabled", musicEnabled);
                    if (musicEnabled)
                        music.loop(Clip.LOOP_CONTINUOUSLY);
                    else
                        music.stop();
                    break;
                case HI_RES_SLIDER:
                    hiResEnabled = !hiResEnabled;
                    Main.putBooleanPreference("hiResEnabled", hiResEnabled);
                    if (Main.isRetina) {
                        if (hiResEnabled) {
                            Main.changeFrameSize(new Dimension(width, height));
                        } else {
                            Main.changeFrameSize(new Dimension(width / 4, height / 4));
                        }
                    } else {
                        if (hiResEnabled) {
                            Main.changeFrameSize(new Dimension(width * 2, height * 2));
                        } else {
                            Main.changeFrameSize(new Dimension(width / 2, height / 2));
                        }
                    }
                    break;
                case YES_BUTTON:
                    System.exit(0);
                    break;
                case NO_BUTTON:
                    drawYesOrNoWindow = false;
                    hoverListener.resetListener();
                    hoverListener.addMenuDefaultHoverListeners();
                    break;
                case MENU_BUTTON_AT_PICK_LEVEL:
                    drawLevelPick = false;
                    menuButtonHover = false;
                    hoverListener.resetListener();
                    hoverListener.addMenuDefaultHoverListeners();
                    break;
                case MENU_BUTTON_AT_ABOUT:
                    drawAbout = false;
                    menuButtonHover = false;
                    hoverListener.resetListener();
                    hoverListener.addMenuDefaultHoverListeners();
                    break;
                default: //level pick
                    for (int i = 0; i < maxUnlockedLevel; i++) {
                        levelPickButtonHover[i] = false;
                    }
                    return box.id % 10 + 1; //run level with given id
            }
        }
        return -1;
    }

    /**
     * checks the position of the mouse for optional hover of the button
     * @param e MouseEvent instance
     * @param width frame width
     * @param height frame height
     */
    @Override
    public void mouseMoved(MouseEvent e, int width, int height) {
        int x = e.getX();
        int y = e.getY();

        int clickedX, clickedY;
        if (Main.isRetina) {
            clickedX = x * 2 - width / 2;
            clickedY = height / 2 - y * 2;
        } else {
            clickedX = x - width / 2;
            clickedY = height / 2 - y;
        }
        BoundingBox box;
        if ((box = hoverListener.getElementAt(clickedX, clickedY)) != null) {
            switch (box.id) {
                case START_LEVEL_BUTTON:
                    startButtonHover = true;
                    aboutButtonHover = false;
                    quitButtonHover = false;
                    musicSliderHover = false;
                    hiResSliderHover = false;
                    break;
                case ABOUT_BUTTON:
                    aboutButtonHover = true;
                    startButtonHover = false;
                    quitButtonHover = false;
                    musicSliderHover = false;
                    hiResSliderHover = false;
                    break;
                case QUIT_BUTTON:
                    quitButtonHover = true;
                    startButtonHover = false;
                    aboutButtonHover = false;
                    musicSliderHover = false;
                    hiResSliderHover = false;
                    break;
                case MUSIC_SLIDER:
                    musicSliderHover = true;
                    startButtonHover = false;
                    aboutButtonHover = false;
                    quitButtonHover = false;
                    hiResSliderHover = false;
                    break;
                case HI_RES_SLIDER:
                    hiResSliderHover = true;
                    startButtonHover = false;
                    aboutButtonHover = false;
                    quitButtonHover = false;
                    musicSliderHover = false;
                    break;
                case YES_BUTTON:
                    yesButtonHover = true;
                    noButtonHover = false;
                    break;
                case NO_BUTTON:
                    noButtonHover = true;
                    yesButtonHover = false;
                    break;
                case MENU_BUTTON_AT_PICK_LEVEL:
                    for (int i = 0; i < maxUnlockedLevel; i++)
                            levelPickButtonHover[i] = false;
                    menuButtonHover = true;
                    break;
                case MENU_BUTTON_AT_ABOUT:
                    menuButtonHover = true;
                    break;
                default: //level pick
                    for (int i = 0; i < 9; i++)
                            levelPickButtonHover[i] = false;
                    levelPickButtonHover[box.id % 10] = true;
            }
        } else {
            if (drawYesOrNoWindow) {
                yesButtonHover = false;
                noButtonHover = false;
            } else if (drawLevelPick) {
                for (int i = 0; i < maxUnlockedLevel; i++) {
                    levelPickButtonHover[i] = false;
                }
                menuButtonHover = false;
            } else if (drawAbout) {
                menuButtonHover = false;
            } else {
                resetMainMenuButtonHovers();
            }
        }
    }
}
