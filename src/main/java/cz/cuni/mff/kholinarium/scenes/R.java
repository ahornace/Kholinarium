package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import cz.cuni.mff.kholinarium.Main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;

/**
 * R class represents all the resources that game will need.<br>
 * <br>
 * Created by Adam Hornáček on 08/03/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
final class R {

    private static final String IMAGES_SRC = "/images";

    private static Map<String, Integer> dimensions = new HashMap<>();
    private static Map<String, Integer> textures = new HashMap<>();

    private static GL2 gl;

    /**
     * loads textures and adds them to hashmap so we can use them quickly
     * @param gl instance of GL2 so we can load textures
     */
    static void loadTextures(GL2 gl) {
        R.gl = gl;

        loadTowerTextures();
        loadBlockTileTextures();
        loadRoadTileTextures();
        loadTopBarTextures();
        loadEnemiesTextures();
        loadPopUpAcessoriesTextures();
        loadVictoryTextures();
        loadDefeatTextures();
        loadPausedTextures();
        loadMainMenuTextures();
        loadMainMenuAccessoriesTextures();
        loadCloudTextures();
        loadIntroSceneTextures();

        R.gl = null;
    }

    private static void loadTowerTextures() {
        loadTextureIntoMap("Arrow.png", "arrow");
        loadTextureIntoMap("Gem_gun.png", "laserTower");
        loadTextureIntoMap("Gem_gun2.png", "laserTower2");
        loadTextureIntoMap("Gem_gun3.png", "laserTower3");
        loadTextureIntoMap("arrow_turret1.png", "arrowTower");
        loadTextureIntoMap("arrow_turret2.png", "arrowTower2");
        loadTextureIntoMap("arrow_turret3.png", "arrowTower3");
        loadTextureIntoMap("Freezing_turret.png", "freezingTower");
        loadTextureIntoMap("Freezing_turret2.png", "freezingTower2");
        loadTextureIntoMap("Freezing_turret3.png", "freezingTower3");
    }

    private static void loadBlockTileTextures() {
        loadTextureIntoMap("Green_Tile.png", "builtSite");
        loadTextureIntoMap("Green_tile_without_corners.png", "builtSiteWithoutCorners");
        loadTextureIntoMap("Gray_tile.png", "nonBuiltSite");
    }

    private static void loadRoadTileTextures() {
        loadTextureIntoMap("Road_horizontal.png", "roadHorizontal");
        loadTextureIntoMap("Road_vert.png", "roadVertical");
        loadTextureIntoMap("Road_cross.png", "roadCross");
        loadTextureIntoMap("Road_top_right.png", "roadTopRight");
        loadTextureIntoMap("Road_top_left.png", "roadTopLeft");
        loadTextureIntoMap("Road_bottom_right.png", "roadBottomRight");
        loadTextureIntoMap("Road_bottom_left.png", "roadBottomLeft");
        loadTextureIntoMap("Road_T_bottom.png", "roadTBottom");
        loadTextureIntoMap("Road_T_left.png", "roadTLeft");
        loadTextureIntoMap("Road_T_right.png", "roadTRight");
        loadTextureIntoMap("Road_T_top.png", "roadTTop");
    }

    private static void loadTopBarTextures() {
        loadTextureIntoMap("Top_bar.png", "topBar");
        loadTextureIntoMap("Heart.png", "heart");
        loadTextureIntoMap("Heart_broken.png", "heartBroken");
        loadTextureIntoMap("Heart_none.png", "heartNone");
        loadTextureIntoMap("Level_bar.png", "levelBar");
        loadTextureIntoMap("Bar_filling.png", "levelBarStatus");
        loadTextureIntoMap("Money.png", "moneyBar");
        loadTextureIntoMap("Pause_button.png", "pauseButton");
    }

    private static void loadEnemiesTextures() {
        loadTextureIntoMap("Ice_enemy.png", "iceEnemy");
        loadTextureIntoMap("Ice_enemy_frozen.png", "iceEnemyFrozen");
        loadTextureIntoMap("Fire_enemy.png", "fireEnemy");
        loadTextureIntoMap("Fire_enemy_frozen.png", "fireEnemyFrozen");
        loadTextureIntoMap("Slime_enemy.png", "slimeEnemy");
        loadTextureIntoMap("Slime_enemy_frozen.png", "slimeEnemyFrozen");
    }

    private static void loadPopUpAcessoriesTextures() {
        loadTextureIntoMap("Upgrade_bg.png", "upgradeBar");
        loadTextureIntoMap("Close.png", "cancelBar");
        loadTextureIntoMap("Close_hover.png", "cancelBarHover");
        loadTextureIntoMap("Upgrade.png", "upgradeButton");
        loadTextureIntoMap("Upgrade_hover.png", "upgradeButtonHover");
        loadTextureIntoMap("Upgrade_inactive.png", "upgradeButtonInactive");
        loadTextureIntoMap("Sell.png", "sellButton");
        loadTextureIntoMap("Sell_hover.png", "sellButtonHover");
        loadTextureIntoMap("Buy_bg_full.png", "buyTowerBar");
        loadTextureIntoMap("Buy.png", "buyButton");
        loadTextureIntoMap("Buy_hover.png", "buyButtonHover");
        loadTextureIntoMap("Buy_button_inactive.png", "buyButtonInactive");
        loadTextureIntoMap("Quit.png", "yesOrNoWindow");
        loadTextureIntoMap("Point.png", "point");
        loadTextureIntoMap("No_point.png", "noPoint");
        loadTextureIntoMap("Yes_point.png", "yesPoint");
    }

    private static void loadVictoryTextures() {
        loadTextureIntoMap("Victory_bg.png", "victoryBg");
        loadTextureIntoMap("Next_level_butt.png", "victoryNextLevelButton");
        loadTextureIntoMap("Next_level_butt_hover.png", "victoryNextLevelButtonHover");
        loadTextureIntoMap("Main_menu_butt.png", "victoryMainMenuButton");
        loadTextureIntoMap("Main_menu_butt_hover.png", "victoryMainMenuButtonHover");
    }

    private static void loadDefeatTextures() {
        loadTextureIntoMap("Defeat_bg.png", "defeatBg");
        loadTextureIntoMap("Try_again_butt.png", "tryAgainButton");
        loadTextureIntoMap("Try_again_butt_hover.png", "tryAgainButtonHover");
    }

    private static void loadPausedTextures() {
        loadTextureIntoMap("Paused_bg.png", "pausedBg");
        loadTextureIntoMap("Resume_button.png", "resumeButton");
        loadTextureIntoMap("Resume_button_hover.png", "resumeButtonHover");
    }

    private static void loadMainMenuTextures() {
        loadTextureIntoMap("main_menu.png", "mainMenu");
        loadTextureIntoMap("Start_game_button.png", "menuStartButton");
        loadTextureIntoMap("Start_game_button_hover.png", "menuStartButtonHover");
        loadTextureIntoMap("About_button.png", "menuAboutButton");
        loadTextureIntoMap("About_button_hover.png", "menuAboutButtonHover");
        loadTextureIntoMap("Quit_button.png", "menuQuitButton");
        loadTextureIntoMap("Quit_button_hover.png", "menuQuitButtonHover");
        loadTextureIntoMap("Main_menu_BG.png", "menuBackground");
    }

    private static void loadMainMenuAccessoriesTextures() {
        loadTextureIntoMap("Slider.png", "menuSlider");
        loadTextureIntoMap("Slider_hover.png", "menuSliderHover");
        loadTextureIntoMap("Slider_off.png", "menuSliderOff");
        loadTextureIntoMap("Slider_off_hover.png", "menuSliderOffHover");
        loadTextureIntoMap("Yes_button.png", "yesButton");
        loadTextureIntoMap("Yes_button_hover.png", "yesButtonHover");
        loadTextureIntoMap("No_button.png", "noButton");
        loadTextureIntoMap("No_button_hover.png", "noButtonHover");
        loadTextureIntoMap("About.png", "menuAbout");
        loadTextureIntoMap("Level_pick_BG.png", "menuLevelPickBG");
        loadTextureIntoMap("Level_button.png", "menuLevelPickButton");
        loadTextureIntoMap("Level_button_hover.png", "menuLevelPickButtonHover");
        loadTextureIntoMap("Level_button_locked.png", "menuLevelPickButtonLocked");
    }

    private static void loadCloudTextures() {
        loadTextureIntoMap("Cloud1.png", "cloud1");
        loadTextureIntoMap("Cloud2.png", "cloud2");
        loadTextureIntoMap("Cloud3.png", "cloud3");
        loadTextureIntoMap("Cloud4.png", "cloud4");
    }

    private static void loadIntroSceneTextures() {
        loadTextureIntoMap("Story_text_bg.png", "textBg");
        loadTextureIntoMap("Logo.png", "logo");
    }

    private static void loadTextureIntoMap(String image, String mapKey) {
        Texture t = null;
        try {
            t = TextureIO.newTexture(R.class.getResource(IMAGES_SRC + "/" + image), false, "png");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        textures.put(mapKey, t.getTextureObject(gl));
    }

    /**
     * loads dimensions according to the frame width and height
     * @param width frame width
     * @param height frame height
     */
    static void loadDimensions(int width, int height) {
        int scaling = 1;
        if (Main.getBooleanPreference("hiResEnabled")) {
            scaling = 2;
        }

        dimensions.put("topBar", height / 2 - 64 * scaling);
        dimensions.put("topBarHalf", 32 * scaling);
        dimensions.put("topBarFull", 64 * scaling);
        dimensions.put("topBarCenter", height / 2 - 32 * scaling);

        dimensions.put("tileDimension", 54 * scaling);

        dimensions.put("heartWidth", 26 * scaling);
        dimensions.put("heartWidthHalf", 13 * scaling);
        dimensions.put("heartHeightHalf", 12 * scaling);
        dimensions.put("heartDelimeter", dimensions.get("heartWidth") + (int) (0.025 * width / 2));

        dimensions.put("moneyBarEndX", (int) (0.85 * width / 2));
        dimensions.put("moneyBarHeightHalf", 18 * scaling);
        dimensions.put("moneyBarWidth", 78 * scaling);
        dimensions.put("moneyAlign", 12 * scaling);

        dimensions.put("fireEnemyWidthHalf", 11 * scaling);
        dimensions.put("fireEnemyHeightHalf", 15 * scaling);
        dimensions.put("fireEnemyFrozenWidthHalf", 21 * scaling);
        dimensions.put("fireEnemyFrozenHeightHalf", 25 * scaling);
        dimensions.put("iceEnemyWidthHalf", 15 * scaling);
        dimensions.put("iceEnemyHeightHalf", 15 * scaling);
        dimensions.put("iceEnemyFrozenWidthHalf", 28 * scaling);
        dimensions.put("iceEnemyFrozenHeightHalf", 25 * scaling);
        dimensions.put("slimeEnemyWidthHalf", 13 * scaling);
        dimensions.put("slimeEnemyHeightHalf", 14 * scaling);
        dimensions.put("slimeEnemyFrozenWidthHalf", 22 * scaling);
        dimensions.put("slimeEnemyFrozenHeightHalf", 24 * scaling);

        dimensions.put("waveTextPosition", -93 * scaling);

        dimensions.put("levelBarHeightHalf", 26 * scaling);
        dimensions.put("levelBarWidthHalf", 119 * scaling);
        dimensions.put("levelBarStatusStart", -67 * scaling);
        dimensions.put("levelBarStatusLength", 179 * scaling);
        dimensions.put("levelBarStatusHeight", 16 * scaling);
        dimensions.put("levelBarStatusHeightHalf", 8 * scaling);

        dimensions.put("laserTowerWidthHalf", 18 * scaling);
        dimensions.put("laserTowerHeightHalf", 18 * scaling);
        dimensions.put("freezingTowerWidthHalf", 17 * scaling);
        dimensions.put("freezingTowerHeightHalf", 19 * scaling);

        dimensions.put("arrowTowerDimensionXHalf", 19 * scaling);
        dimensions.put("arrowTowerDimensionYHalf", 19 * scaling);

        dimensions.put("laserWidth", 2 * scaling);

        dimensions.put("firstHeartPosition", -(int) (0.85 * width / 2));
        dimensions.put("secondHeartPosition", dimensions.get("firstHeartPosition") + dimensions.get("heartDelimeter"));
        dimensions.put("thirdHeartPosition", dimensions.get("firstHeartPosition") + 2 * dimensions.get("heartDelimeter"));

        dimensions.put("pauseButtonWidthHalf", 18 * scaling);
        dimensions.put("pauseButtonHeightHalf", 28 * scaling);
        dimensions.put("pauseButtonShadowHalf", 2 * scaling);
        dimensions.put("pauseButtonY", R.getDimension("topBarCenter") -
                24 * scaling - R.getDimension("pauseButtonHeightHalf"));
        dimensions.put("pauseButtonWithoutChainY", dimensions.get("topBar") - 26 * scaling);

        dimensions.put("upgradeBarWidth", 184 * scaling);
        dimensions.put("upgradeBarHeight", 178 * scaling);
        dimensions.put("upgradeBarPriceEndX", 158 * scaling);
        dimensions.put("upgradeBarUpgradePriceCenterY", 28 * scaling);
        dimensions.put("upgradeBarSellPriceCenterY", 145 * scaling);
        dimensions.put("upgradeBarButtonWidth", 93 * scaling);
        dimensions.put("upgradeBarButtonHeight", 34 * scaling);
        dimensions.put("upgradeBarButtonStartX", 17 * scaling);
        dimensions.put("upgradeButtonStartY", 14 * scaling);
        dimensions.put("upgradeBarParametersStartX", 109 * scaling);
        dimensions.put("upgradeBarParametersDelimiterX", 9 * scaling);
        dimensions.put("upgradeBarParametersStartY", 64 * scaling);
        dimensions.put("upgradeBarParametersDelimiterY", 18 * scaling);
        dimensions.put("upgradeBarDotWidth", 6 * scaling);
        dimensions.put("upgradeBarDotHeight", 6 * scaling);
        dimensions.put("sellButtonStartY", 130 * scaling);
        dimensions.put("cancelButtonWidth", 28 * scaling);
        dimensions.put("cancelButtonHeight", 28 * scaling);

        dimensions.put("popUpCancelExtensionX", 11 * scaling);
        dimensions.put("popUpCancelExtensionY", 11 * scaling);
        dimensions.put("popUpCancelDecreaseX", -17 * scaling);
        dimensions.put("popUpCancelDecreaseY", -17 * scaling);
        dimensions.put("buyTowerWidth", 214 * scaling);
        dimensions.put("buyTowerCancelStartX", 197 * scaling);
        dimensions.put("buyTowerHeight", 388 * scaling);
        dimensions.put("buyButtonWidth", 74 * scaling);
        dimensions.put("buyButtonHeight", 34 * scaling);
        dimensions.put("buyButtonStartX", 68 * scaling);
        dimensions.put("buyButtonFirstStartY", -26 * scaling);
        dimensions.put("buyButtonSecondStartY", -144 * scaling);
        dimensions.put("buyButtonThirdStartY", -262 * scaling);

        dimensions.put("arrowWidth", 32 * scaling);
        dimensions.put("arrowHeightHalf", 5 * scaling);

        dimensions.put("victoryOrDefeatBgWidthHalf", 111 * scaling);
        dimensions.put("victoryOrDefeatBgHeightHalf", 53 * scaling);
        dimensions.put("victoryOrDefeatButtonStartY", -6 * scaling);
        dimensions.put("victoryOrDefeatLeftButtonStartX", -94 * scaling);
        dimensions.put("victoryOrDefeatMenuButtonStartX", 6 * scaling);
        dimensions.put("victoryOrDefeatButtonWidth", 88 * scaling);
        dimensions.put("victoryOrDefeatButtonHeight", 34 * scaling);

        //main menu resources
        dimensions.put("menuWidthHalf", 95 * scaling);
        dimensions.put("menuHeightHalf", 127 * scaling);

        dimensions.put("menuButtonStartX", -75 * scaling);
        dimensions.put("menuButtonWidth", 150 * scaling);
        dimensions.put("menuButtonHeight", 40 * scaling);
        dimensions.put("menuStartButtonStartY", 63 * scaling);
        dimensions.put("menuAboutButtonStartY", 19 * scaling);
        dimensions.put("menuQuitButtonStartY", -25 * scaling);
        dimensions.put("menuButtonsShadowHeight", 6 * scaling);

        dimensions.put("menuSliderStartY", -86 * scaling);
        dimensions.put("menuMusicSliderStartX", -43 * scaling);
        dimensions.put("menuMusicSliderOffStartX", -73 * scaling);
        dimensions.put("menuHiResSliderOffStartX", 5 * scaling);
        dimensions.put("menuHiResSliderStartX", 35 * scaling);
        dimensions.put("menuSliderWidth", 38 * scaling);
        dimensions.put("menuSliderHeight", 26 * scaling);
        dimensions.put("menuSliderPlaceHolderWidth", 68 * scaling);
        dimensions.put("menuSliderPlaceHolderHeight", 26 * scaling);
        dimensions.put("menuSliderPlaceHolderStartY", -83 * scaling);

        dimensions.put("menuAboutWidthHalf", 105 * scaling);
        dimensions.put("menuAboutHeightHalf", 123 * scaling);
        dimensions.put("menuLogoStartY", 15 * scaling);
        dimensions.put("menuAboutMenuButtonCenterY", -94 * scaling);

        dimensions.put("menuLevelPickBGWidthHalf", 95 * scaling);
        dimensions.put("menuLevelPickBGHeightHalf", 133 * scaling);
        dimensions.put("menuLevelPickButtonsRowHeight", 52 * scaling);
        dimensions.put("menuLevelPickButtonsColumnWidth", 52 * scaling);
        dimensions.put("menuLevelPickButtonsStartX", -76 * scaling);
        dimensions.put("menuLevelPickButtonsStartY", 77 * scaling);
        dimensions.put("menuLevelPickButtonWidth", 48 * scaling);
        dimensions.put("menuLevelPickButtonWidthHalf", 24 * scaling);
        dimensions.put("menuLevelPickButtonFontYFromTop", 21 * scaling);
        dimensions.put("menuLevelPickButtonHeight", 48 * scaling);
        dimensions.put("menuLevelPickButtonsStartXWithoutShadow", -73 * scaling);
        dimensions.put("menuLevelPickButtonWidthWithoutShadow", 42 * scaling);
        dimensions.put("menuLevelPickButtonHeightWithoutShadow", 42 * scaling);
        dimensions.put("menuLevelPickMenuButtonCenterY", -104 * scaling);
        dimensions.put("menuLevelPickMenuButtonWidthHalf", 47 * scaling);
        dimensions.put("menuLevelPickMenuButtonHeightHalf", 17 * scaling);

        dimensions.put("onePixelHeight", scaling);

        dimensions.put("yesOrNoWindowWidthHalf", 92 * scaling);
        dimensions.put("yesOrNoWindowHeightHalf", 46 * scaling);
        dimensions.put("yesButtonStartXFromCenter", -82 * scaling);
        dimensions.put("noButtonStartXFromCenter", 2 * scaling);
        dimensions.put("yesOrNoButtonWidth", 80 * scaling);
        dimensions.put("yesOrNoButtonHeight", 34 * scaling);

        dimensions.put("yesOrNoWindowButtonStartYFromCenter", -5 * scaling);
        dimensions.put("yesOrNoWindowButtonEndYFromCenter", -33 * scaling);

        dimensions.put("cloud1WidthHalf", 37 * scaling);
        dimensions.put("cloud1HeightHalf", 19 * scaling);
        dimensions.put("cloud2WidthHalf", 52 * scaling);
        dimensions.put("cloud2HeightHalf", 26 * scaling);
        dimensions.put("cloud3WidthHalf", 34 * scaling);
        dimensions.put("cloud3HeightHalf", 20 * scaling);
        dimensions.put("cloud4WidthHalf", 54 * scaling);
        dimensions.put("cloud4HeightHalf", 31 * scaling);

        dimensions.put("cloud1StartX", 66 * scaling);
        dimensions.put("cloud1StartY", -238 * scaling);
        dimensions.put("cloud2StartX", 327 * scaling);
        dimensions.put("cloud2StartY", -81 * scaling);
        dimensions.put("cloud3StartX", 379 * scaling);
        dimensions.put("cloud3StartY", -323 * scaling);
        dimensions.put("cloud4StartX", 54 * scaling);
        dimensions.put("cloud4StartY", -30 * scaling);

        //story scene
        dimensions.put("textBgWidthHalf", 191 * scaling);
        dimensions.put("textBgHeightHalf", 23 * scaling);
        dimensions.put("logoWidthHalf", 135 * scaling);
        dimensions.put("logoHeightHalf", 30 * scaling);
        dimensions.put("textBgBottomMargin", 20 * scaling);
        dimensions.put("textMargin", 10 * scaling);
    }

    /**
     * @param key identification of the texture we want to get
     * @return texture id according to the key from the textures hashmap
     */
    public static int getTexture(String key) {
        try {
            return textures.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * @param key identification of the dimension we want to get
     * @return dimension according to the key from the dimensions hashmap
     */
    public static int getDimension(String key) {
        return dimensions.get(key);
    }

}
