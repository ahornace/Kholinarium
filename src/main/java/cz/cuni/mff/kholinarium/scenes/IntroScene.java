package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;
import cz.cuni.mff.kholinarium.Main;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Intro scene is a scene that is shown when game starts. Runs music and shows lyrics for story.<br>
 * <br>
 * Created by Adam Hornáček on 08/05/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class IntroScene implements Scene {

    // text lines divided into sentences that has to be together
    private static final String[] TEXT_LINES = {
            "Humans lived in peace for many centuries.",
            "Eventually, they became reckless in terms of defence.",
            "That is what enemy had been waiting for.",
            "When elementals came, no city was prepared to face the new threat.",
            "Soon enough, elementals conquered every human city. Except one.",
            "Kholinarium was repelling elemental attacks for a while now.",
            "However, elementals are gathering forces for the final attack.",
            "There is only one person who can preserve the humankind,",
            "the best Kholinarium’s strategist and tinker - you…"
    };

    private static final Long[] TEXT_LINE_TIME_SHOWN = {
            4500000000L,
            2700000000L,
            5000000000L,
            6400000000L,
            4700000000L,
            4800000000L,
            4000000000L,
            4500000000L,
    };

    private static final float ALPHA_STEP = 1f / 45;
    private static final float FONT_COLOR = 95f / 255;


    private List<String> linesToRender = new LinkedList<>();

    private LinkedList<String> lines;
    private LinkedList<Long> milestones;
    private long timePassed = 0;
    private long nextText;
    private boolean newText = true;
    private boolean musicVolumeIncreased = false;

    private float alpha = 0;

    private boolean musicEnabled = Main.getBooleanPreference("musicEnabled");

    private Clip music;
    private Clip voice;

    public IntroScene() {

        lines = new LinkedList<>(Arrays.asList(TEXT_LINES));

        nextText = 3700000000L;
        milestones = new LinkedList<>(Arrays.asList(TEXT_LINE_TIME_SHOWN));
        startMusic();
    }

    /**
     * starts music and voice in the intro scene
     */
    private void startMusic() {
        try {
            InputStream is1 = this.getClass().getResourceAsStream("/sounds/intro.wav");
            InputStream bufferedIn1 = new BufferedInputStream(is1);
            AudioInputStream as1 = AudioSystem.getAudioInputStream(bufferedIn1);
            music = AudioSystem.getClip();
            music.open(as1);

            InputStream is2 = this.getClass().getResourceAsStream("/sounds/voice.wav");
            InputStream bufferedIn2 = new BufferedInputStream(is2);
            AudioInputStream as2 = AudioSystem.getAudioInputStream(bufferedIn2);
            voice = AudioSystem.getClip();
            voice.open(as2);

            //we lessen the music volume while voice is being played
            FloatControl floatControl = (FloatControl) music.getControl(FloatControl.Type.MASTER_GAIN);
            floatControl.setValue(-6); //changes volume by the value -6

            if (musicEnabled) {
                music.loop(Clip.LOOP_CONTINUOUSLY);
                voice.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * stops the intro music – called when menu scene is created
     */
    void stopMusic() {
        music.stop();
        voice.stop();
    }

    /**
     * updates scene
     * @param delta change of time in nanoseconds since the last update
     */
    @Override
    public void update(long delta) {
        if (delta > 20000000) {
            delta = 20000000; //prevents too long lags
        }
        if (nextText - timePassed < 750000000L) { //fading of texts lasts 0.75 seconds
            alpha -= ALPHA_STEP;
        } else {
            if (alpha < 1) {
                alpha += ALPHA_STEP;
            }
        }

        if (timePassed > nextText) {
            newText = true;
            alpha = 0; //starts showing text with alpha 0
            if (milestones.size() > 0) { //there is text to be showed
                nextText = milestones.pollFirst();
            } else {
                nextText = 3000000000L; //fades every 3 seconds
                //voice is no longer playing – we change the music's volume to normal
                if (!voice.isRunning() && !musicVolumeIncreased) {
                    FloatControl floatControl = (FloatControl) music.getControl(FloatControl.Type.MASTER_GAIN);
                    floatControl.setValue(0);
                    musicVolumeIncreased = true;
                }
            }
            timePassed = 0;
        }
        timePassed += delta;
    }

    /**
     * renders scene
     * @param gl instance of GL2
     * @param drawer helpful class for rendering
     * @param width frame width
     * @param height frame height
     */
    public void render(GL2 gl, Drawer drawer, int width, int height) {
        //draw background
        drawer.drawRectangleWithCenterAt(gl, 0, 0, width / 2, height / 2, R.getTexture("menuBackground"));
        //draw logo
        drawer.drawRectangleWithCenterAt(gl, 0, 0, R.getDimension("logoWidthHalf"),
                R.getDimension("logoHeightHalf"), R.getTexture("logo"));
        //draw white background for text
        drawer.drawRectangleWithCenterAt(gl, 0, -height / 2 + R.getDimension("textBgHeightHalf") +
                        R.getDimension("textBgBottomMargin"),
                R.getDimension("textBgWidthHalf"),
                R.getDimension("textBgHeightHalf"), R.getTexture("textBg"));

        if (newText) { //we are supposed to update the text
            if (lines.size() == 0) { //no more text to be shown – repeatedly show "Press any key to continue..."
                linesToRender = Collections.singletonList("Press any key to continue...");
            } else { //load new text
                String[] w = lines.pollFirst().split(" ");
                linesToRender = drawer.getLinesOfText(Arrays.asList(w), width, height,
                        R.getDimension("textBgWidthHalf") * 2 - R.getDimension("textMargin"), null);
            }
            newText = false;
        }
        //draw waves of text
        if (linesToRender.size() == 1) {
            drawer.drawLineOfText(linesToRender.get(0), 0, -height / 2 + R.getDimension("textBgHeightHalf") +
                            R.getDimension("textBgHeightHalf"),
                    width, height, null, FONT_COLOR, alpha);
        } else if (linesToRender.size() == 2) {
            drawer.drawLineOfText(linesToRender.get(0), 0, -height / 2 + 2 * R.getDimension("textBgHeightHalf") +
                            R.getDimension("textMargin"),
                    width, height, null, FONT_COLOR, alpha);
            drawer.drawLineOfText(linesToRender.get(1), 0, -height / 2 + 2 * R.getDimension("textBgHeightHalf") -
                            R.getDimension("textMargin"),
                    width, height, null, FONT_COLOR, alpha);
        }

    }

    @Override
    public void reshape(int width, int height) {

    }

    @Override
    public void mouseMoved(MouseEvent e, int width, int height) {

    }

    @Override
    public int mouseClicked(MouseEvent e, int width, int height) {
        return -1;
    }

}
