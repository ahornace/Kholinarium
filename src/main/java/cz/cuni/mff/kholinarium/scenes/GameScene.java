package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;
import cz.cuni.mff.kholinarium.Main;
import cz.cuni.mff.kholinarium.level.Level;
import cz.cuni.mff.kholinarium.level.Tile;
import cz.cuni.mff.kholinarium.level.TileType;
import cz.cuni.mff.kholinarium.monsters.EnemyNest;
import cz.cuni.mff.kholinarium.monsters.Monster;
import cz.cuni.mff.kholinarium.towers.*;

import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Game scene is a scene where all the major stuff is happening – the game. It takes care of rendering and updating all
 * the appropriate components of the game.<br>
 * <br>
 * Created by Adam Hornáček on 13/02/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class GameScene implements Scene {

    public static final int CANCEL_BUY_WINDOW = 0;
    public static final int BUY_ARROW_TOWER = 1;
    public static final int BUY_LASER_TOWER = 2;
    public static final int BUY_FREEZING_TOWER = 3;
    public static final int CANCEL_UPGRADE_WINDOW = 4;
    public static final int UPGRADE_TOWER = 5;
    public static final int SELL_TOWER = 6;
    private static final int NEXT_LEVEL_BUTTON = 7;
    private static final int TRY_AGAIN_BUTTON = 8;
    private static final int VICTORY_MENU_BUTTON = 9;
    private static final int DEFEAT_MENU_BUTTON = 10;
    public static final int PAUSE_BUTTON = 11;
    private static final int RESUME_BUTTON = 12;

    LinkedList<Monster> monsters = new LinkedList<Monster>();
    LinkedList<EnemyNest> nests = new LinkedList<EnemyNest>();
    private LinkedList<Tower> towers = new LinkedList<Tower>();
    private LinkedList<Projectile> projectiles = new LinkedList<Projectile>();

    private Level level;
    Tile[][] map;

    int currentMoney;
    int lives;
    long timePassed = 0; //time passed since beginning of line

    boolean paused = false;
    private boolean drawUpgradeBar = false;
    boolean drawGameOver = false;
    boolean drawGameWon = false;
    private boolean drawBuyTowerBar = false;
    private boolean drawPauseMenu = false;

    private boolean firstBuyButtonHover = false;
    private boolean secondBuyButtonHover = false;
    private boolean thirdBuyButtonHover = false;
    private boolean cancelButtonHover = false;
    private boolean upgradeButtonHover = false;
    private boolean sellButtonHover = false;
    private boolean popUpLeftButtonHover = false;
    private boolean mainMenuButtonHover = false;

    private boolean canBuyArrowTower;
    private boolean canBuyLaserTower;
    private boolean canBuyFreezingTower;

    private int waves = 0; //actual line of monsters

    //time that specifies length of line – time during which monsters from specific line will be spawned
    private long waveLength;

    boolean moneyChanged = true;
    boolean wavesChanged;

    int delimiter;

    int delimiterHalf;

    //position of bars – upgrade and buy tower
    private int barX;
    private int barY;

    private int tileClickedIndexX;
    private int tileClickedIndexY;
    private int tileClickedX;
    private int tileClickedY;

    int width;
    int height;
    int widthHalf;
    int heightHalf;

    private boolean canUpgradeTower;
    private boolean isUpgradedToMax;

    private HoverListener hoverListener = new HoverListener();

    GameScene(Level level, int width, int height) {
        this.level = level;
        map = level.getMap();
        currentMoney = level.getStartingMoney();
        delimiter = R.getDimension("tileDimension");
        delimiterHalf = delimiter / 2;
        lives = 6;
        this.width = width;
        this.height = height;
        widthHalf = width / 2;
        heightHalf = height / 2;
        updateTilesPosition(width, height);
        hoverListener.addPauseButtonListener();
        if (level.getWaves().size() > 0)
            startNewWave();
    }

    /**
     * preprocessing for tiles position so we don't have to compute it every frame
     * @param width frame width
     * @param height frame height
     */
    private void updateTilesPosition(int width, int height) {
        for (int i = 0; i < level.getMapWidth(); i++)
            for (int j = 0; j < level.getMapHeight(); j++) {
                //we add or substract delimiterHalf to get the center of tile
                map[i][j].setX(-width / 2 + i * delimiter + delimiterHalf);
                map[i][j].setY(height / 2 - j * delimiter - delimiterHalf - R.getDimension("topBarFull"));
            }
    }

    /**
     * starts new wave of enemies
     */
    private void startNewWave() {
        if (level.getWaves().size() == 0 && monsters.size() == 0) {
            //there are no enemies and none are supposed to come so we have won
            paused = true;
            drawGameWon = true;
            showVictoryOrDefeatBar();
            return;
        } else if (level.getWaves().size() == 0) { //there are still monsters but none will be spawned
            return;
        }
        //start new wave
        waves++; //increments wave number
        wavesChanged = true; //indicates that wave number has changed so the drawer will have to compute the
        //width and height of new text that is showing
        nests = level.getWaves().pollFirst().getNests(); //loads nests for this wave
        waveLength = 0;
        timePassed = 0;
        for (EnemyNest n : nests) { //finds maximum of time that nests will spawn monsters
            if (waveLength < n.getEndTime()) {
                waveLength = n.getEndTime();
            }
        }
    }

    /**
     * frame size was changed – we need to redraw and update some position
     * @param width frame width
     * @param height frame height
     */
    @Override
    public void reshape(int width, int height) {
        this.width = width;
        this.height = height;
        widthHalf = width / 2;
        heightHalf = height / 2;
        wavesChanged = true; //need to redraw wave number
        moneyChanged = true; //need to redraw money number
        for (Monster m : monsters) { //need to repair monster positions
            m.repairPosition(delimiter, delimiterHalf, R.getDimension("topBarFull"), widthHalf, heightHalf);
        }

        updateTilesPosition(width, height); //need to recompute tile position

        //updates pause button listener
        hoverListener.resetListener();
        if (drawGameWon || drawGameOver) {
            showVictoryOrDefeatBar();
        } else if (drawUpgradeBar) {
            showUpgradeBar(tileClickedIndexX, tileClickedIndexY);
        } else if (drawBuyTowerBar) {
            showBuyTowerBar(tileClickedIndexX, tileClickedIndexY);
        } else if (drawPauseMenu) {
            hoverListener.addPopUp2ButtonListeners(RESUME_BUTTON, DEFEAT_MENU_BUTTON);
        } else {
            hoverListener.addPauseButtonListener();
        }
    }

    /**
     * makes changes according to the time that passed since last update
     * @param delta time in nanoseconds since last update
     */
    @Override
    public void update(long delta) {
        if (paused) { //game is paused – cannot update anything
            return;
        }
        if (delta > 20000000) { //if the lag is too long – suppress it
            delta = 20000000;
        }
        timePassed += delta;
        if (timePassed > waveLength) { //it's time to start new wave
            startNewWave();
        }
        spawnMonsters(delta);
        moveMonsters(delta);
        if (lives <= 0) { //player has lost – show defeat bar and don't continue with the update
            paused = true;
            drawGameOver = true;
            showVictoryOrDefeatBar();
            return;
        }
        for (Tower tower : towers) {
            if (tower instanceof LaserTower) {
                int res = ((LaserTower) tower).shoot(delta);
                Monster m = tower.getTarget();
                if (res == LaserTower.KILLED_MONSTER) { //laser tower killed monster – we need to get rid of it
                    tower.resetTarget();
                    monsters.remove(m);
                    int x = ((int) m.getX() + width / 2) / delimiter;
                    int y = (height / 2 - (int) m.getY() - R.getDimension("topBarFull")) / delimiter;
                    map[x][y].getMonsters().remove(m); //removes monster from tile
                    currentMoney += m.getValue(); //adds monster value to current money
                    moneyChanged = true;
                }
            } else if (tower instanceof ArrowTower) {
                ((ArrowTower) tower).updateAngle(delta);
                Projectile p = ((ArrowTower) tower).shoot();
                if (p != null) //arrow shot a new arrow
                    projectiles.addLast(p);
            } else {
                ((FreezingTower) tower).shoot();
            }
        }
        moveProjectiles(delta);
    }

    /**
     * spawns monster from nests
     * @param delta change of time in nanoseconds since last update – need for nest to determine the time since the
     *              last enemy was spawned
     */
    void spawnMonsters(long delta) {
        for (EnemyNest nest : nests) {
            Monster m = nest.spawnMonster(delta, timePassed, R.getDimension("topBarFull"),
                    delimiter, delimiterHalf, width, height);
            if (m == null) { //nothing to be spawned
                continue;
            }
            switch (m.getType()) { //according to the type adds the monster dimensions to determine when projectile
            // hit them
                case Monster.FIRE_MONSTER:
                    m.setBoundingBox(R.getDimension("fireEnemyWidthHalf"), R.getDimension("fireEnemyHeightHalf"));
                    break;
                case Monster.ICE_MONSTER:
                    m.setBoundingBox(R.getDimension("iceEnemyWidthHalf"), R.getDimension("iceEnemyHeightHalf"));
                    break;
                case Monster.SLIME_MONSTER:
                    m.setBoundingBox(R.getDimension("slimeEnemyWidthHalf"), R.getDimension("slimeEnemyHeightHalf"));
                    break;
            }
            //adds monster to game scene and appropriate tile
            monsters.addFirst(m);
            map[m.getLastPointX()][m.getLastPointY()].addMonster(m);
        }
    }

    /**
     * moves monsters according to the change of time since last update
     * @param delta change of time in nanoseconds since last update
     */
    void moveMonsters(long delta) {
        for (Iterator<Monster> it = monsters.iterator(); it.hasNext(); ) {
            Monster m = it.next();
            int res = m.move(delta, R.getDimension("topBarFull"), delimiter, delimiterHalf, width, height);
            if (res == Monster.AT_THE_END) { //is at the end
                map[m.getLastPointX()][m.getLastPointY()].getMonsters().remove(m);
                it.remove();
                m.inflictDamage(Float.MAX_VALUE);
                lives--; //decrement lives
            } else if (res == Monster.CHANGED_TILE) { //change tile
                map[m.getLastPointX()][m.getLastPointY()].getMonsters().remove(m);
                map[m.getNextPointX()][m.getNextPointY()].getMonsters().add(m);
            }
        }
    }

    /**
     * moves projectiles according to the change of time since last update
     * @param delta change of time in nanoseconds since last update
     */
    private void moveProjectiles(long delta) {
        for (Iterator<Projectile> it = projectiles.iterator(); it.hasNext(); ) {
            Projectile p = it.next();
            int res = p.move(delta, R.getDimension("topBarFull"), level.getMapWidth(), level.getMapHeight(),
                    width, height, delimiter);
            if (res == Projectile.DELETE_PROJECTILE) { //projectile came to its destination and didn't hit any monsters
                it.remove();
            } else {
                //check for every monster that is in the same tile if it collides with projectile
                for (Iterator<Monster> mIt = map[p.getTileX()][p.getTileY()].getMonsters().iterator(); mIt.hasNext(); ) {
                    Monster m = mIt.next();
                    if (p.collidesWithMonster(m)) {
                        //projectile hit the monster
                        m.inflictDamage(p.getDamage());
                        if (!m.isAlive()) { //projectile killed the monster
                            monsters.remove(m);
                            mIt.remove();
                            currentMoney += m.getValue();
                            moneyChanged = true;
                        }
                        it.remove();
                        break;
                    }
                }
            }
        }
    }

    /**
     * renders the game scene
     * @param gl instance of GL2
     * @param drawer helpful class for rendering
     * @param width frame width
     * @param height frame height
     */
    @Override
    public void render(GL2 gl, Drawer drawer, int width, int height) {
        if (paused) {
            gl.glColor4f(0.5f, 0.5f, 0.5f, 1f); //dims the background if game is paused
        } else {
            gl.glColor4f(1, 1, 1, 1);
        }
        renderTiles(gl, drawer);
        for (Monster monster : monsters) {
            drawer.drawMonster(gl, (float) monster.getX(), (float) monster.getY(), monster.getType(),
                    monster.isFrozen());
        }
        for (Tower tower : towers) {
            if (tower instanceof ArrowTower) {
                drawer.drawArrowTower(gl, tower.getX(), tower.getY(), ((ArrowTower) tower).angle, tower.getTexture());
            } else if (tower instanceof LaserTower) {
                drawer.drawLaserTower(gl, tower.getX(), tower.getY(), tower.getTexture());
                if (tower.getTarget() != null) {
                    //determine color of the laser
                    switch (tower.getLevel()) {
                        case Tower.BASIC_TOWER:
                            gl.glColor4f(0.21569f, 0.62745f, 0.90196f, 1);
                            break;
                        case Tower.ENHANCED_TOWER:
                            gl.glColor4f(1, 0.30980f, 0.39216f, 1);
                            break;
                        case Tower.MASTER_TOWER:
                            gl.glColor4f(0.74118f, 0.06275f, 0.87843f, 1);
                            break;
                    }
                    //draw laser
                    drawer.drawLine(gl, tower.getX(), tower.getY(), (int) tower.getTarget().getX(),
                            (int) tower.getTarget().getY());
                    if (paused)
                        gl.glColor4f(0.5f, 0.5f, 0.5f, 1);
                    else
                        gl.glColor4f(1, 1, 1, 1);
                }
            } else {
                drawer.drawFreezingTower(gl, tower.getX(), tower.getY(), tower.getTexture());
            }
        }
        for (Projectile projectile : projectiles) {
            drawer.drawProjectile(gl, (float) projectile.getX(), (float) projectile.getY(), projectile.getAngle());
        }
        if (paused) {
            gl.glColor4f(1, 1, 1, 1);
            if (drawBuyTowerBar) {
                renderBuyTowerRadii(gl, drawer);
            } else if (drawUpgradeBar) {
                Tower t = map[tileClickedIndexX][tileClickedIndexY].getTower();
                float r = t.getRadius();
                if (upgradeButtonHover) { //button hover – draw circle of tower radius
                    r += t.getRadiusChange();
                }
                drawer.drawCircle(gl, tileClickedX, tileClickedY, 100, delimiter * r);
            }
            gl.glColor4f(0.5f, 0.5f, 0.5f, 1);
        }
        renderTopBar(gl, drawer);
        if (paused) {
            gl.glColor4f(1f, 1f, 1f, 1f);
            if (drawUpgradeBar) {
                highlightClicked(gl, drawer);
                renderUpgradeBar(gl, drawer, width, height);
            } else if (drawBuyTowerBar) {
                highlightClicked(gl, drawer);
                renderBuyTowerBar(gl, drawer);
            } else if (drawGameOver) {
                drawer.drawPopUp2(gl, R.getTexture("defeatBg"),
                        R.getTexture("tryAgainButton"),
                        R.getTexture("tryAgainButtonHover"), popUpLeftButtonHover,
                        mainMenuButtonHover);
            } else if (drawGameWon) {
                drawer.drawPopUp2(gl, R.getTexture("victoryBg"),
                        R.getTexture("victoryNextLevelButton"),
                        R.getTexture("victoryNextLevelButtonHover"), popUpLeftButtonHover,
                        mainMenuButtonHover);
            } else { //paused
                drawer.drawPopUp2(gl, R.getTexture("pausedBg"),
                        R.getTexture("resumeButton"),
                        R.getTexture("resumeButtonHover"), popUpLeftButtonHover,
                        mainMenuButtonHover);
            }
            gl.glColor4f(0.5f, 0.5f, 0.5f, 1);
        }
    }

    /**
     * renders upgrade bar
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     * @param width frame width
     * @param height frame height
     */
    private void renderUpgradeBar(GL2 gl, Drawer drawer, int width, int height) {
        Tower t = map[tileClickedIndexX][tileClickedIndexY].getTower();

        drawer.drawUpgradeBar(gl, barX, barY, width, height, cancelButtonHover, canUpgradeTower, isUpgradedToMax,
                upgradeButtonHover, sellButtonHover, t.getUpgradePrice(),
                t.getSellPrice());
        gl.glColor4f(1, 1, 1, 1);

        //draws dots of tower capabilities
        drawer.drawTowerAttributes(gl, barX, barY, upgradeButtonHover, t.getCurrentDamageLevel(), 0);
        drawer.drawTowerAttributes(gl, barX, barY, upgradeButtonHover, t.getCurrentRadiusLevel(), 1);
        drawer.drawTowerAttributes(gl, barX, barY, upgradeButtonHover, t.getCurrentFrequencyLevel(), 2);
    }

    /**
     * renders top bar
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     */
    void renderTopBar(GL2 gl, Drawer drawer) {
        drawer.drawTopBar(gl, widthHalf, heightHalf);
        drawer.drawHearts(gl, lives);
        drawer.drawLevelBar(gl);
        float percentage = (float) timePassed / waveLength;
        if (percentage > 1) {
            percentage = 1;
        }
        drawer.drawLevelBarStatus(gl, percentage);
        drawer.drawMoneyBar(gl);
        drawer.drawWaveInfo(waves, width, height, widthHalf, wavesChanged);
        if (wavesChanged) {
            wavesChanged = false;
        }
        drawer.drawMoneyInfo(currentMoney, width, height, widthHalf, moneyChanged);
        if (moneyChanged) {
            moneyChanged = false;
        }
        if (paused) {
            gl.glColor4f(0.5f, 0.5f, 0.5f, 1);
        } else {
            gl.glColor4f(1, 1, 1, 1);
        }
        drawer.drawPauseButton(gl);
    }

    /**
     * renders buy tower bar
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     */
    private void renderBuyTowerBar(GL2 gl, Drawer drawer) {
        drawer.drawBuyTowerBar(gl, barX, barY, cancelButtonHover, firstBuyButtonHover,
                secondBuyButtonHover, thirdBuyButtonHover, canBuyArrowTower, canBuyLaserTower, canBuyFreezingTower);
    }

    /**
     * renders radii when mouse is over buy tower button
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     */
    private void renderBuyTowerRadii(GL2 gl, Drawer drawer) {
        float r;
        //if mouse is over a buy button for some tower then draw circle that shows the radius of tower
        if (firstBuyButtonHover) {
            r = ArrowTower.START_RADIUS;
            drawer.drawCircle(gl, tileClickedX, tileClickedY, 100, delimiter * r);
        } else if (secondBuyButtonHover) {
            r = LaserTower.START_RADIUS;
            drawer.drawCircle(gl, tileClickedX, tileClickedY, 100, delimiter * r);
        } else if (thirdBuyButtonHover) {
            r = FreezingTower.START_RADIUS;
            drawer.drawCircle(gl, tileClickedX, tileClickedY, 100, delimiter * r);
        }
        gl.glColor4f(1, 1, 1, 1);
    }

    /**
     * renders tiles
     * @param gl instance of GL2
     * @param drawer instance of Drawer - helpful class for rendering components
     */
    private void renderTiles(GL2 gl, Drawer drawer) {
        for (int i = 0; i < level.getMapWidth(); i++) {
            for (int j = 0; j < level.getMapHeight(); j++) {
                drawer.drawRectangleWithCenterAt(gl, map[i][j].getX(), map[i][j].getY(), delimiterHalf, delimiterHalf,
                        R.getTexture(map[i][j].getTexture()));
            }
        }
    }

    /**
     * highlights clicked tile when buy tower bar or upgrade bar are showing
     * @param gl instance of GL2
     * @param drawer instance of Drawer – helpful class for rendering components
     */
    private void highlightClicked(GL2 gl, Drawer drawer) {
        if (drawBuyTowerBar || drawUpgradeBar) {
            gl.glColor4f(1, 1, 1, 1);
            drawer.drawRectangleWithCenterAt(gl, map[tileClickedIndexX][tileClickedIndexY].getX(),
                    map[tileClickedIndexX][tileClickedIndexY].getY(), delimiterHalf, delimiterHalf,
                    R.getTexture("builtSiteWithoutCorners")); //always have to be built site where we clicked
            if (map[tileClickedIndexX][tileClickedIndexY].getType() == TileType.TOWER) {
                Tower t = map[tileClickedIndexX][tileClickedIndexY].getTower();
                if (t instanceof ArrowTower) {
                    drawer.drawArrowTower(gl, t.getX(), t.getY(), ((ArrowTower) t).angle, t.getTexture());
                } else if (t instanceof LaserTower) {
                    drawer.drawLaserTower(gl, t.getX(), t.getY(), t.getTexture());
                } else {
                    drawer.drawFreezingTower(gl, t.getX(), t.getY(), t.getTexture());
                }
            }
            gl.glColor4f(1, 1, 1, 1);
        }
    }

    /**
     * @param i x axis tile index in map from left
     * @param barWidth width of the bar we want to draw
     * @param width frame width
     * @return returns start x axis position for given bar to be drawn at
     */
    private int getStartPositionX(int i, int barWidth, int width) {
        if ((i + 1) * delimiter + barWidth <= width) { //can be drawn on the right side of clicked tile
            return (i + 1) * delimiter - width / 2;
        } else { //if cannot be drawn on the right side then has to be drawn on the left side
            return i * delimiter - width / 2 - barWidth + R.getDimension("popUpCancelExtensionX");
        }
    }

    /**
     * @param j y axis tile index oin map from top
     * @param barHeight height of the bar we want to draw
     * @param height frame height
     * @return returns start y axis position for given bar to be drawn at
     */
    private int getStartPositionY(int j, int barHeight, int height) {
        for (int k = j; k >= 0; k--) { //starts from given tile and goes to top
            if (height - k * delimiter - barHeight - R.getDimension("topBarFull") >= 0) {
                return height / 2 - k * delimiter - R.getDimension("topBarFull");
            }
        }
        return 0;
    }

    /**
     * adds appropriate hover listeners for victory or defeat bar
     */
    void showVictoryOrDefeatBar() {
        int leftButtonId = 7;
        int mainMenuButtonId = 9;
        if (drawGameOver) {
            leftButtonId = 8;
            mainMenuButtonId = 10;
        }

        popUpLeftButtonHover = false;
        mainMenuButtonHover = false;

        hoverListener.addPopUp2ButtonListeners(leftButtonId, mainMenuButtonId);
    }

    /**
     * determines buy tower bar position and adds appropriate hover listeners
     * @param i x axis tile index in map from left
     * @param j y axis tile index in map from top
     */
    private void showBuyTowerBar(int i, int j) {
        paused = true;
        upgradeButtonHover = false; //repairs bug when reopening upgrade window showed white dots
        tileClickedX = map[i][j].getX();
        tileClickedY = map[i][j].getY();
        tileClickedIndexX = i;
        tileClickedIndexY = j;
        barX = getStartPositionX(i, R.getDimension("buyTowerWidth") +
                R.getDimension("popUpCancelExtensionX"), width);
        barY = getStartPositionY(j, R.getDimension("buyTowerHeight") +
                R.getDimension("popUpCancelExtensionY"), height);

        firstBuyButtonHover = false;
        secondBuyButtonHover = false;
        thirdBuyButtonHover = false;
        cancelButtonHover = false;

        canBuyArrowTower = currentMoney >= ArrowTower.BUILD_PRICE;
        canBuyLaserTower = currentMoney >= LaserTower.BUILD_PRICE;
        canBuyFreezingTower = currentMoney >= FreezingTower.BUILD_PRICE;

        hoverListener.addBuyTowerButtonListeners(barX, barY, canBuyArrowTower, canBuyLaserTower,
                canBuyFreezingTower);

        drawBuyTowerBar = true;
    }

    /**
     * determines upgrade bar position and adds appropriate hover listeners
     * @param i x axis tile index in map from left
     * @param j y axis tile index in map from top
     */
    private void showUpgradeBar(int i, int j) {
        paused = true;
        upgradeButtonHover = false; //repairs bug when reopening upgrade window showed white dots

        tileClickedX = map[i][j].getX();
        tileClickedY = map[i][j].getY();
        tileClickedIndexX = i;
        tileClickedIndexY = j;
        barX = getStartPositionX(i, R.getDimension("upgradeBarWidth") +
                R.getDimension("popUpCancelExtensionX"), width);
        barY = getStartPositionY(j, R.getDimension("upgradeBarHeight") +
                R.getDimension("popUpCancelExtensionY"), height);
        Tower t = map[i][j].getTower();
        canUpgradeTower = (currentMoney >= t.getUpgradePrice() && t.getLevel() < Tower.MASTER_TOWER);
        isUpgradedToMax = t.getLevel() == Tower.MASTER_TOWER;
        hoverListener.addUpgradeBarListeners(canUpgradeTower, barX, barY);

        drawUpgradeBar = true;
    }

    /**
     * buys arrow tower at clicked tile
     */
    private void buyArrowTower() {
        map[tileClickedIndexX][tileClickedIndexY].setType(TileType.TOWER);
        Tower t = new ArrowTower(map[tileClickedIndexX][tileClickedIndexY].getX(),
                map[tileClickedIndexX][tileClickedIndexY].getY(),
                level.getMapWidth(), level.getMapHeight(), tileClickedIndexX, tileClickedIndexY,
                R.getTexture("arrowTower"), delimiter, map);
        towers.add(t);
        currentMoney -= ArrowTower.BUILD_PRICE;
        moneyChanged = true;
        map[tileClickedIndexX][tileClickedIndexY].setTower(t);
    }

    /**
     * buys laser tower at clicked tile
     */
    private void buyLaserTower() {
        map[tileClickedIndexX][tileClickedIndexY].setType(TileType.TOWER);
        Tower t = new LaserTower(map[tileClickedIndexX][tileClickedIndexY].getX(),
                map[tileClickedIndexX][tileClickedIndexY].getY(),
                level.getMapWidth(), level.getMapHeight(), tileClickedIndexX, tileClickedIndexY,
                R.getTexture("laserTower"), delimiter, map);
        towers.add(t);
        currentMoney -= LaserTower.BUILD_PRICE;
        moneyChanged = true;
        map[tileClickedIndexX][tileClickedIndexY].setTower(t);
    }

    /**
     * buys freezing tower at clicked tile
     */
    private void buyFreezingTower() {
        map[tileClickedIndexX][tileClickedIndexY].setType(TileType.TOWER);
        Tower t = new FreezingTower(map[tileClickedIndexX][tileClickedIndexY].getX(),
                map[tileClickedIndexX][tileClickedIndexY].getY(),
                level.getMapWidth(), level.getMapHeight(), tileClickedIndexX, tileClickedIndexY,
                R.getTexture("freezingTower"), delimiter, map);
        towers.add(t);
        currentMoney -= FreezingTower.BUILD_PRICE;
        moneyChanged = true;
        map[tileClickedIndexX][tileClickedIndexY].setTower(t);
    }

    /**
     * upgrades tower at clicked tile
     */
    private void upgradeTower() {
        Tower t = map[tileClickedIndexX][tileClickedIndexY].getTower();
        currentMoney -= t.getUpgradePrice();
        if (t instanceof ArrowTower) {
            t.upgradeTower(tileClickedIndexX, tileClickedIndexY, level.getMapWidth(), level.getMapHeight(),
                    R.getTexture("arrowTower2"), R.getTexture("arrowTower3"), delimiter, map);
        } else if (t instanceof LaserTower) {
            t.upgradeTower(tileClickedIndexX, tileClickedIndexY, level.getMapWidth(), level.getMapHeight(),
                    R.getTexture("laserTower2"), R.getTexture("laserTower3"), delimiter, map);
        } else {
            t.upgradeTower(tileClickedIndexX, tileClickedIndexY, level.getMapWidth(), level.getMapHeight(),
                    R.getTexture("freezingTower2"), R.getTexture("freezingTower3"), delimiter, map);
        }
        moneyChanged = true;
    }

    /**
     * sells tower at clicked tile
     */
    private void sellTower() {
        Tower t = map[tileClickedIndexX][tileClickedIndexY].getTower();
        map[tileClickedIndexX][tileClickedIndexY].setTower(null);
        map[tileClickedIndexX][tileClickedIndexY].setType(TileType.BUILT_SITE);
        towers.remove(t);
        currentMoney += t.getSellPrice();
        moneyChanged = true;
    }

    /**
     * @param e instance of MouseEvent
     * @param width frame width
     * @param height frame height
     * @return returns special int constant that tells main scene how to react
     */
    @Override
    public int mouseClicked(MouseEvent e, int width, int height) {
        int x = e.getX();
        int y = e.getY();
        if (y - R.getDimension("topBarHalf") < 0) {
            return -1;
        }
        int i, j;
        int clickedX, clickedY;
        if (Main.isRetina) {
            i = x * 2 / delimiter;
            j = (y * 2 - R.getDimension("topBarFull")) / delimiter;
            clickedX = x * 2 - width / 2;
            clickedY = height / 2 - y * 2;
        } else {
            i = x / delimiter;
            j = (y - R.getDimension("topBarFull")) / delimiter;
            clickedX = x - width / 2;
            clickedY = height / 2 - y;
        }

        BoundingBox box;
        if ((box = hoverListener.getElementAt(clickedX, clickedY)) != null) {
            switch (box.id) {
                case CANCEL_BUY_WINDOW: //cancel button of buy new tower bar
                    drawBuyTowerBar = false;
                    cancelButtonHover = false;
                    break;
                case BUY_ARROW_TOWER: //arrow tower
                    buyArrowTower();
                    drawBuyTowerBar = false;
                    break;
                case BUY_LASER_TOWER: //laser tower
                    buyLaserTower();
                    drawBuyTowerBar = false;
                    break;
                case BUY_FREEZING_TOWER: //freezing tower
                    buyFreezingTower();
                    drawBuyTowerBar = false;
                    break;
                case CANCEL_UPGRADE_WINDOW: //cancel button of upgrade bar
                    cancelButtonHover = false;
                    drawUpgradeBar = false;
                    break;
                case UPGRADE_TOWER: //upgrade tower
                    upgradeTower();
                    drawUpgradeBar = false;
                    break;
                case SELL_TOWER: //sell tower
                    sellTower();
                    sellButtonHover = false;
                    drawUpgradeBar = false;
                    break;
                case NEXT_LEVEL_BUTTON:
                    return MainScene.START_NEXT_LEVEL; //start next level
                case TRY_AGAIN_BUTTON:
                    return MainScene.TRY_AGAIN; //start current level
                case VICTORY_MENU_BUTTON:
                    return MainScene.UNLOCK_LVL_AND_GO_TO_MENU; //won level but is going to main menu
                case DEFEAT_MENU_BUTTON:
                    return MainScene.GO_TO_MENU; //defeated and is going to main menu
                case PAUSE_BUTTON:
                    drawPauseMenu = true;
                    hoverListener.addPopUp2ButtonListeners(RESUME_BUTTON, DEFEAT_MENU_BUTTON);
                    break;
                case RESUME_BUTTON:
                    drawPauseMenu = false;
            }
            if (drawPauseMenu) {
                paused = true;
            } else {
                paused = false;
                hoverListener.resetListener();
                hoverListener.addPauseButtonListener();
            }
            return -1;
        } else if (!paused && map[i][j].getType() == TileType.BUILT_SITE)
            showBuyTowerBar(i, j);
        else if (!paused && map[i][j].getType() == TileType.TOWER)
            showUpgradeBar(i, j);
        return -1;
    }

    /**
     * changes texture when mouse is over some component that is in hover listener
     * @param e instance of MouseEvent
     * @param width frame width
     * @param height frame height
     */
    @Override
    public void mouseMoved(MouseEvent e, int width, int height) {
        if (paused) {
            int x = e.getX();
            int y = e.getY();
            int clickedX, clickedY;
            if (Main.isRetina) {
                clickedX = x * 2 - width / 2;
                clickedY = height / 2 - y * 2;
            } else {
                clickedX = x - width / 2;
                clickedY = height / 2 - y;
            }
            BoundingBox box;
            if ((box = hoverListener.getElementAt(clickedX, clickedY)) != null) {
                switch (box.id) {
                    //buy new tower hovers
                    case CANCEL_BUY_WINDOW:
                        cancelButtonHover = true;
                        firstBuyButtonHover = false;
                        secondBuyButtonHover = false;
                        thirdBuyButtonHover = false;
                        break;
                    case BUY_ARROW_TOWER:
                        firstBuyButtonHover = true;
                        secondBuyButtonHover = false;
                        thirdBuyButtonHover = false;
                        cancelButtonHover = false;
                        break;
                    case BUY_LASER_TOWER:
                        secondBuyButtonHover = true;
                        firstBuyButtonHover = false;
                        thirdBuyButtonHover = false;
                        cancelButtonHover = false;
                        break;
                    case BUY_FREEZING_TOWER:
                        thirdBuyButtonHover = true;
                        firstBuyButtonHover = false;
                        secondBuyButtonHover = false;
                        cancelButtonHover = false;
                        break;
                    //upgrade tower hovers
                    case CANCEL_UPGRADE_WINDOW:
                        cancelButtonHover = true;
                        upgradeButtonHover = false;
                        sellButtonHover = false;
                        break;
                    case UPGRADE_TOWER:
                        upgradeButtonHover = true;
                        sellButtonHover = false;
                        cancelButtonHover = false;
                        break;
                    case SELL_TOWER:
                        sellButtonHover = true;
                        upgradeButtonHover = false;
                        cancelButtonHover = false;
                        break;
                    //victory or defeat hovers
                    case NEXT_LEVEL_BUTTON:
                    case TRY_AGAIN_BUTTON:
                    case RESUME_BUTTON:
                        popUpLeftButtonHover = true;
                        mainMenuButtonHover = false;
                        break;
                    case VICTORY_MENU_BUTTON:
                    case DEFEAT_MENU_BUTTON:
                        mainMenuButtonHover = true;
                        popUpLeftButtonHover = false;
                        break;
                }
            } else {
                if (drawBuyTowerBar) {
                    cancelButtonHover = false;
                    firstBuyButtonHover = false;
                    secondBuyButtonHover = false;
                    thirdBuyButtonHover = false;
                } else if (drawUpgradeBar) {
                    upgradeButtonHover = false;
                    sellButtonHover = false;
                    cancelButtonHover = false;
                } else if (drawGameOver || drawGameWon || drawPauseMenu) {
                    popUpLeftButtonHover = false;
                    mainMenuButtonHover = false;
                }
            }
        }
    }
}