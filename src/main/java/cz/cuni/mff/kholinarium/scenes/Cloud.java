package cz.cuni.mff.kholinarium.scenes;

/**
 * Cloud is a wrapper class for clouds that are in the background of menu scene.<br>
 * <br>
 * Created by Adam Hornáček on 08/05/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
class Cloud {

    private float x;
    private float y;

    private int widthHalf;
    private int heightHalf;
    private float velocity;

    private int texture;

    public Cloud(float x, float y, int widthHalf, int heightHalf, float velocity, int texture) {
        this.x = x;
        this.y = y;
        this.widthHalf = widthHalf;
        this.heightHalf = heightHalf;
        this.velocity = velocity;
        this.texture = texture;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public int getWidthHalf() {
        return widthHalf;
    }

    public int getHeightHalf() {
        return heightHalf;
    }

    public int getTexture() {
        return texture;
    }

    public void moveByX(float change) {
        x += change * velocity;
    }

    public void flipDirection() {
        velocity *= -1;
    }
}
