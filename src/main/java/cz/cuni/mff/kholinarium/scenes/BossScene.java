package cz.cuni.mff.kholinarium.scenes;

import com.jogamp.opengl.GL2;
import cz.cuni.mff.kholinarium.level.Level;
import cz.cuni.mff.kholinarium.monsters.EnemyNest;
import cz.cuni.mff.kholinarium.monsters.Monster;

import java.util.Iterator;

/**
 * Boss scene is a descendant of game scene. Changes only a minority for correct rendering and updating boss fights.<br>
 * <br>
 * Created by Adam Hornáček on 16/05/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class BossScene extends GameScene {

    private Monster boss;
    private float maximumBossHealth;
    private float bossHealthChange;
    private float lastHealthCheck;

    BossScene(Level level, int width, int height) {
        super(level, width, height);
    }

    /**
     * spawns monster from nests
     * @param delta change of time in nanoseconds since last update – need for nest to determine the time since the
     *              last enemy was spawned
     */
    @Override
    void spawnMonsters(long delta) {
        for (EnemyNest nest : nests) {
            Monster m = nest.spawnMonster(delta, timePassed, R.getDimension("topBarFull"),
                    delimiter, delimiterHalf, width, height);
            if (m == null) //nothing to be spawned
                continue;
            switch (m.getType()) { //according to the type adds the monster dimensions to determine when projectile
                // hit them
                case Monster.FIRE_MONSTER:
                    m.setBoundingBox(R.getDimension("fireEnemyWidthHalf"), R.getDimension("fireEnemyHeightHalf"));
                    break;
                case Monster.ICE_MONSTER:
                    m.setBoundingBox(R.getDimension("iceEnemyWidthHalf"), R.getDimension("iceEnemyHeightHalf"));
                    break;
                case Monster.SLIME_MONSTER:
                    m.setBoundingBox(R.getDimension("slimeEnemyWidthHalf"), R.getDimension("slimeEnemyHeightHalf"));
                    break;
                case Monster.FIRE_MONSTER_BOSS:
                    boss = m;
                    maximumBossHealth = m.getHealth();
                    lastHealthCheck = m.getHealth();
                    m.setBoundingBox((int)(R.getDimension("fireEnemyWidthHalf") * 1.25),
                            (int) (R.getDimension("fireEnemyHeightHalf") * 1.25));
                    break;
                case Monster.SLIME_MONSTER_BOSS:
                    boss = m;
                    maximumBossHealth = m.getHealth();
                    lastHealthCheck = m.getHealth();
                    m.setBoundingBox((int)(R.getDimension("slimeEnemyWidthHalf") * 1.25),
                            (int) (R.getDimension("slimeEnemyHeightHalf") * 1.25));
                    break;
                case Monster.ICE_MONSTER_BOSS:
                    boss = m;
                    maximumBossHealth = m.getHealth();
                    lastHealthCheck = m.getHealth();
                    m.setBoundingBox((int)(R.getDimension("iceEnemyWidthHalf") * 1.25),
                            (int) (R.getDimension("iceEnemyHeightHalf") * 1.25));
                    break;
            }
            //adds monster to game scene and appropriate tile
            monsters.add(m);
            map[m.getLastPointX()][m.getLastPointY()].addMonster(m);
        }
    }

    /**
     * makes changes according to the time that passed since last update
     * @param delta time in nanoseconds since last update
     */
    @Override
    public void update(long delta) {
        super.update(delta);
        if (paused)
            return;
        if (boss != null) {
            bossHealthChange += lastHealthCheck - boss.getHealth();
            lastHealthCheck = boss.getHealth();
            if (bossHealthChange > 1) {
                currentMoney += (int) bossHealthChange;
                bossHealthChange -= (int) bossHealthChange;
                moneyChanged = true;
            }
            if (!boss.isAlive()) {
                paused = true;
                drawGameWon = true;
                showVictoryOrDefeatBar();
            }
        }
    }

    /**
     * renders top bar
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     */
    @Override
    void renderTopBar(GL2 gl, Drawer drawer) {
        drawer.drawTopBar(gl, widthHalf, heightHalf);
        drawer.drawHearts(gl, lives);
        drawer.drawLevelBar(gl);
        renderBossMiniature(gl, drawer);
        if (boss != null) {
            float percentage = boss.getHealth() / maximumBossHealth;
            drawer.drawLevelBarStatus(gl, percentage);
        } else {
            drawer.drawLevelBarStatus(gl, 1);
        }
        drawer.drawMoneyBar(gl);
        if (wavesChanged)
            wavesChanged = false;
        drawer.drawMoneyInfo(currentMoney, width, height, widthHalf, moneyChanged);
        if (moneyChanged)
            moneyChanged = false;
        if (paused)
            gl.glColor4f(0.5f, 0.5f, 0.5f, 1);
        else
            gl.glColor4f(1, 1, 1, 1);
        drawer.drawPauseButton(gl);
    }

    /**
     * renders boss's miniature instead of wave number
     * @param gl instance of GL2
     * @param drawer helpful class for rendering components
     */
    public void renderBossMiniature(GL2 gl, Drawer drawer) {
        switch (boss.getType()) {
            case Monster.FIRE_MONSTER_BOSS:
                drawer.drawRectangleWithCenterAt(gl, R.getDimension("waveTextPosition"), R.getDimension("topBarCenter"),
                        R.getDimension("fireEnemyWidthHalf"), R.getDimension("fireEnemyHeightHalf"),
                        R.getTexture("fireEnemy"));
                break;
            case Monster.SLIME_MONSTER_BOSS:
                drawer.drawRectangleWithCenterAt(gl, R.getDimension("waveTextPosition"), R.getDimension("topBarCenter"),
                        R.getDimension("slimeEnemyWidthHalf"), R.getDimension("slimeEnemyHeightHalf"),
                        R.getTexture("slimeEnemy"));
                break;
            case Monster.ICE_MONSTER_BOSS:
                drawer.drawRectangleWithCenterAt(gl, R.getDimension("waveTextPosition"), R.getDimension("topBarCenter"),
                        R.getDimension("iceEnemyWidthHalf"), R.getDimension("iceEnemyHeightHalf"),
                        R.getTexture("iceEnemy"));
                break;
        }
    }

    /**
     * moves monsters according to the change of time since last update
     * @param delta change of time in nanoseconds since last update
     */
    void moveMonsters(long delta) {
        for (Iterator<Monster> it = monsters.iterator(); it.hasNext(); ) {
            Monster m = it.next();
            int res = m.move(delta, R.getDimension("topBarFull"), delimiter, delimiterHalf, width, height);
            if (res == Monster.AT_THE_END) { //is at the end
                map[m.getLastPointX()][m.getLastPointY()].getMonsters().remove(m);
                it.remove();
                if (m == boss) {
                    paused = true;
                    drawGameOver = true;
                    showVictoryOrDefeatBar();
                } else
                    lives--; //decrement lives
            } else if (res == Monster.CHANGED_TILE) { //change tile
                map[m.getLastPointX()][m.getLastPointY()].getMonsters().remove(m);
                map[m.getNextPointX()][m.getNextPointY()].getMonsters().add(m);
            }
        }
    }

}
