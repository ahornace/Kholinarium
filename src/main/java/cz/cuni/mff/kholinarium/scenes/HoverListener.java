package cz.cuni.mff.kholinarium.scenes;

import java.util.LinkedList;

/**
 * Hover listener is a class that implements linked list of bounding boxes and returns appropriate id for box that the
 * parameters are in.<br>
 * <br>
 * Created by Adam Hornáček on 07/03/15.<br>
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
class HoverListener {

    private LinkedList<BoundingBox> listeningTo = new LinkedList<BoundingBox>();

    /**
     * adds listener to hover listener
     * @param startX start position in x axis of bounding box
     * @param endX end position in x axis of bounding box
     * @param startY start position in y axis of bounding box
     * @param endY end position in y axis of bounding box
     * @param id id of our bounding box we want to listen to – so we can determine the right one
     */
    private void addListener(float startX, float endX, float startY, float endY, int id) {
           listeningTo.add(new BoundingBox(startX, endX, startY, endY, id));
    }

    /**
     * clears hover listener
     */
    public void resetListener() {
        listeningTo.clear();
    }

    /**
     * @param x x axis value
     * @param y y axis value
     * @return first id of bounding box that x and y parameters are in
     */
    public BoundingBox getElementAt(float x, float y) {
        for(BoundingBox box : listeningTo) {
            if (x >= box.startX && x <= box.endX && y >= box.startY && y <= box.endY) {
                return box;
            }
        }
        return null;
    }

    /**
     * adds default menu hover listeners = main menu buttons and sliders
     */
    void addMenuDefaultHoverListeners() {
        addListener(R.getDimension("menuMusicSliderOffStartX"),
                -R.getDimension("menuMusicSliderOffStartX"),
                R.getDimension("menuStartButtonStartY") - R.getDimension("menuButtonHeight") +
                        R.getDimension("menuButtonsShadowHeight"),
                R.getDimension("menuStartButtonStartY"), MenuScene.START_LEVEL_BUTTON);
        addListener(R.getDimension("menuMusicSliderOffStartX"),
                -R.getDimension("menuMusicSliderOffStartX"),
                R.getDimension("menuAboutButtonStartY") - R.getDimension("menuButtonHeight") +
                        R.getDimension("menuButtonsShadowHeight"),
                R.getDimension("menuAboutButtonStartY"), MenuScene.ABOUT_BUTTON);
        addListener(R.getDimension("menuMusicSliderOffStartX"),
                -R.getDimension("menuMusicSliderOffStartX"),
                R.getDimension("menuQuitButtonStartY") - R.getDimension("menuButtonHeight") +
                        R.getDimension("menuButtonsShadowHeight"),
                R.getDimension("menuQuitButtonStartY"), MenuScene.QUIT_BUTTON);
        addListener(R.getDimension("menuButtonStartX"),
                R.getDimension("menuButtonStartX") + R.getDimension("menuSliderPlaceHolderWidth"),
                R.getDimension("menuSliderPlaceHolderStartY") -
                        R.getDimension("menuSliderPlaceHolderHeight") +
                        R.getDimension("menuButtonsShadowHeight"),
                R.getDimension("menuSliderPlaceHolderStartY") +
                        R.getDimension("menuButtonsShadowHeight"), MenuScene.MUSIC_SLIDER);
        addListener(R.getDimension("menuHiResSliderOffStartX"),
                R.getDimension("menuHiResSliderOffStartX") +
                        R.getDimension("menuSliderPlaceHolderWidth"),
                R.getDimension("menuSliderPlaceHolderStartY") -
                        R.getDimension("menuSliderPlaceHolderHeight"),
                R.getDimension("menuSliderPlaceHolderStartY"), MenuScene.HI_RES_SLIDER);
    }

    /**
     * adds level pick hover listeners according to the max level
     * @param maxUnlockedLevel max unlocked level so we know up to how many hover listeners to add
     */
    void addLevelPickHoverListeners(int maxUnlockedLevel) {
        for (int i = 0; i < maxUnlockedLevel; i++) {
            int row = i / 3;
            int column = i % 3;
            //level button listener
            addListener(R.getDimension("menuLevelPickButtonsStartXWithoutShadow") + column *
                            R.getDimension("menuLevelPickButtonsColumnWidth"),
                    R.getDimension("menuLevelPickButtonsStartXWithoutShadow") + column *
                            R.getDimension("menuLevelPickButtonsColumnWidth") +
                            R.getDimension("menuLevelPickButtonWidthWithoutShadow"),
                    R.getDimension("menuLevelPickButtonsStartY") - row *
                            R.getDimension("menuLevelPickButtonsRowHeight") -
                            R.getDimension("menuLevelPickButtonHeightWithoutShadow"),
                    R.getDimension("menuLevelPickButtonsStartY") - row *
                            R.getDimension("menuLevelPickButtonsRowHeight"), 10 + i
                    //10 because mod 10 will give the level number
            );
        }
        //main menu button listener
        addListener(-R.getDimension("menuLevelPickMenuButtonWidthHalf"),
                R.getDimension("menuLevelPickMenuButtonWidthHalf"), R.getDimension("menuLevelPickMenuButtonCenterY") -
                        R.getDimension("menuLevelPickMenuButtonHeightHalf"),
                R.getDimension("menuLevelPickMenuButtonCenterY") + R.getDimension("menuLevelPickMenuButtonHeightHalf"),
                MenuScene.MENU_BUTTON_AT_PICK_LEVEL);
    }

    /**
     * adds yes and no button listeners in quit game pop up window
     */
    void addYesOrNoHoverListeners() {
        //yes button listener
        addListener(R.getDimension("yesButtonStartXFromCenter"),
                R.getDimension("yesButtonStartXFromCenter") + R.getDimension("yesOrNoButtonWidth"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter") -
                        R.getDimension("yesOrNoButtonHeight"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter"), MenuScene.YES_BUTTON);
        //no button listener
        addListener(R.getDimension("noButtonStartXFromCenter"),
                R.getDimension("noButtonStartXFromCenter") + R.getDimension("yesOrNoButtonWidth"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter") -
                        R.getDimension("yesOrNoButtonHeight"),
                R.getDimension("yesOrNoWindowButtonStartYFromCenter"), MenuScene.NO_BUTTON);
    }

    /**
     * adds upgrade bar button listeners
     * @param addUpgradeButtonListener boolean value if we are supposed to add upgrade button listener –
     *                                 if the tower cannot be upgraded because of lack of money or has highest level
     *                                 then we are not adding active upgrade button
     * @param upgradeBarX start x axis position of upgrade bar
     * @param upgradeBarY start y axis position of upgrade bar
     */
    void addUpgradeBarListeners(boolean addUpgradeButtonListener, float upgradeBarX, float upgradeBarY) {
        //cancel button
        addListener(upgradeBarX + R.getDimension("upgradeBarWidth") +
                        R.getDimension("popUpCancelDecreaseX"),
                upgradeBarX + R.getDimension("upgradeBarWidth") +
                        R.getDimension("popUpCancelExtensionX"),
                upgradeBarY + R.getDimension("popUpCancelDecreaseY"),
                upgradeBarY + R.getDimension("popUpCancelExtensionY"), GameScene.CANCEL_UPGRADE_WINDOW);
        if (addUpgradeButtonListener) { //upgrade button
            addListener(upgradeBarX + R.getDimension("upgradeBarButtonStartX"),
                    upgradeBarX + R.getDimension("upgradeBarButtonStartX")
                            + R.getDimension("upgradeBarButtonWidth"),
                    upgradeBarY - R.getDimension("upgradeButtonStartY") -
                            R.getDimension("upgradeBarButtonHeight"),
                    upgradeBarY - R.getDimension("upgradeButtonStartY"), GameScene.UPGRADE_TOWER);
        }
        //sell button
        addListener(upgradeBarX + R.getDimension("upgradeBarButtonStartX"),
                upgradeBarX + R.getDimension("upgradeBarButtonStartX")
                        + R.getDimension("upgradeBarButtonWidth"),
                upgradeBarY - R.getDimension("sellButtonStartY") -
                        R.getDimension("upgradeBarButtonHeight"),
                upgradeBarY - R.getDimension("sellButtonStartY"), GameScene.SELL_TOWER);
    }

    /**
     * adds buy tower bar button listeners
     * @param buyTowerBarX start x axis position of buy tower bar
     * @param buyTowerBarY start y axis position of buy tower bar
     * @param canBuyArrowTower boolean value that determines if we have enough money to buy arrow tower
     * @param canBuyLaserTower boolean value that determines if we have enough money to buy laser tower
     * @param canBuyFreezingTower boolean value that determines if we have enough money to buy freezing tower
     */
    void addBuyTowerButtonListeners(float buyTowerBarX, float buyTowerBarY, boolean canBuyArrowTower,
                                    boolean canBuyLaserTower, boolean canBuyFreezingTower) {
        //cancel button
        addListener(buyTowerBarX + R.getDimension("buyTowerWidth") +
                        R.getDimension("popUpCancelDecreaseX"),
                buyTowerBarX + R.getDimension("buyTowerWidth") +
                        R.getDimension("popUpCancelExtensionX"),
                buyTowerBarY + R.getDimension("popUpCancelDecreaseY"),
                buyTowerBarY + R.getDimension("popUpCancelExtensionY"), GameScene.CANCEL_BUY_WINDOW);

        //TODO this is with shadows – do it without them
        if (canBuyArrowTower) { //buy arrow tower button – add only if the player has enough money
            addListener(buyTowerBarX + R.getDimension("buyButtonStartX"),
                    buyTowerBarX + R.getDimension("buyButtonStartX") + R.getDimension("buyButtonWidth"),
                    buyTowerBarY + R.getDimension("buyButtonFirstStartY") -
                            R.getDimension("buyButtonHeight"),
                    buyTowerBarY + R.getDimension("buyButtonFirstStartY"), GameScene.BUY_ARROW_TOWER);
        }
        if (canBuyLaserTower) { //buy laser tower button – add only if the player has enough money
            addListener(buyTowerBarX + R.getDimension("buyButtonStartX"),
                    buyTowerBarX + R.getDimension("buyButtonStartX") + R.getDimension("buyButtonWidth"),
                    buyTowerBarY + R.getDimension("buyButtonSecondStartY") -
                            R.getDimension("buyButtonHeight"),
                    buyTowerBarY + R.getDimension("buyButtonSecondStartY"), GameScene.BUY_LASER_TOWER);
        }
        if (canBuyFreezingTower) { //buy freezing tower button – add only if the player has enough money
            addListener(buyTowerBarX + R.getDimension("buyButtonStartX"),
                    buyTowerBarX + R.getDimension("buyButtonStartX") + R.getDimension("buyButtonWidth"),
                    buyTowerBarY + R.getDimension("buyButtonThirdStartY") -
                            R.getDimension("buyButtonHeight"),
                    buyTowerBarY + R.getDimension("buyButtonThirdStartY"), GameScene.BUY_FREEZING_TOWER);
        }
    }

    /**
     * adds listeners to popup window that is shown during the pause, victory or defeat
     * @param leftButtonId id of the left button we are supposed to add
     * @param mainMenuButtonId id of the right button we are supposed to add – it is always main menu button id but
     *                         with different meanings
     */
    void addPopUp2ButtonListeners(int leftButtonId, int mainMenuButtonId) {
        //left button
        addListener(R.getDimension("victoryOrDefeatLeftButtonStartX"),
                R.getDimension("victoryOrDefeatLeftButtonStartX") +
                        R.getDimension("victoryOrDefeatButtonWidth"),
                R.getDimension("victoryOrDefeatButtonStartY") -
                        R.getDimension("victoryOrDefeatButtonHeight"),
                R.getDimension("victoryOrDefeatButtonStartY"), leftButtonId);
        //right button
        addListener(R.getDimension("victoryOrDefeatMenuButtonStartX"),
                R.getDimension("victoryOrDefeatMenuButtonStartX") +
                        R.getDimension("victoryOrDefeatButtonWidth"),
                R.getDimension("victoryOrDefeatButtonStartY") -
                        R.getDimension("victoryOrDefeatButtonHeight"),
                R.getDimension("victoryOrDefeatButtonStartY"), mainMenuButtonId);
    }

    /**
     * adds pause button listener
     */
    void addPauseButtonListener() {
        addListener(R.getDimension("moneyBarEndX") - R.getDimension("pauseButtonWidthHalf") * 2,
                R.getDimension("moneyBarEndX"), R.getDimension("pauseButtonWithoutChainY") -
                        R.getDimension("pauseButtonWidthHalf"),
                R.getDimension("pauseButtonWithoutChainY") +
                        R.getDimension("pauseButtonWidthHalf"), GameScene.PAUSE_BUTTON);
    }

    /**
     * adds main menu button listener in about window
     */
    void addAboutWindowHoverListener() {
        addListener(-R.getDimension("menuLevelPickMenuButtonWidthHalf"),
                R.getDimension("menuLevelPickMenuButtonWidthHalf"), R.getDimension("menuAboutMenuButtonCenterY") -
                        R.getDimension("menuLevelPickMenuButtonHeightHalf"),
                R.getDimension("menuAboutMenuButtonCenterY") + R.getDimension("menuLevelPickMenuButtonHeightHalf"),
                MenuScene.MENU_BUTTON_AT_ABOUT);
    }

}
