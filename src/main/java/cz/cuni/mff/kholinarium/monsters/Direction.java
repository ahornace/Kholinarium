package cz.cuni.mff.kholinarium.monsters;

/**
 * Created by Adam Hornáček on 03/03/15.
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public enum Direction {
    UP, DOWN, RIGHT, LEFT
}
