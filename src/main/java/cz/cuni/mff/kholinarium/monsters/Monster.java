package cz.cuni.mff.kholinarium.monsters;

import cz.cuni.mff.kholinarium.level.Point;

import java.util.LinkedList;

/**
 * Created by Adam Hornáček on 14/02/15.
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class Monster {

    //constants
    public static final int AT_THE_END = 0;
    public static final int CHANGED_TILE = 1;

    public static final int FIRE_MONSTER = 0;
    public static final int ICE_MONSTER = 1;
    public static final int SLIME_MONSTER = 2;
    public static final int FIRE_MONSTER_BOSS = 3;
    public static final int SLIME_MONSTER_BOSS = 4;
    public static final int ICE_MONSTER_BOSS = 5;

    private double x;
    private double y;
    private int sourceX;
    private int sourceY;
    private double distanceInTile;
    private double targetX;
    private double targetY;
    private double boundingBoxX;
    private double boundingBoxY;


    private float health = 100;
    private float defaultVelocity = 1.5f;
    private float velocity = 1.5f;
    private double velocityConst;
    private double maxSpeed;

    private LinkedList<Point> path;
    private int type;
    private int value;
    private Direction direction;
    private boolean frozen = false;

    private int delimiter; //needed for determining the velocity

    public Monster(int delimiter, int type, float velocity, float health, int value, LinkedList<Point> path) {
        maxSpeed = 0.02 * velocity * delimiter / 2;
        this.delimiter = delimiter;
        velocityConst = velocity * delimiter / 1000000000L;
        defaultVelocity = velocity;
        this.type = type;
        this.velocity = velocity;
        this.health = health;
        this.value = value;
        this.path = path;
    }

    //access methods
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getBoundingBoxX() {
        return boundingBoxX;
    }

    public double getBoundingBoxY() {
        return boundingBoxY;
    }

    public Direction getDirection() {
        return direction;
    }

    public float getVelocity() {
        return velocity;
    }

    public int getValue() {
        return value;
    }

    public int getType() {
        return type;
    }

    public int getLastPointX() {
        return sourceX;
    }

    public int getLastPointY() {
        return sourceY;
    }

    public int getNextPointX() {
        return path.getFirst().getX();
    }

    public int getNextPointY(){
        return path.getFirst().getY();
    }

    public float getHealth() {
        return health;
    }

    public boolean isAlive() {
        return health > 0;
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void inflictDamage(float dmg) {
        health -= dmg;
    }

    public void slowDownBy(float percentage) {
        if (defaultVelocity * percentage < velocity)
            changeVelocityTo(defaultVelocity * percentage);
    }

    private void changeVelocityToNormal() {
        velocity = defaultVelocity;
        velocityConst = velocity * delimiter / 1000000000l;
        frozen = false;
    }

    private void changeVelocityTo(float velocity) {
        this.velocity = velocity;
        velocityConst = velocity * delimiter / 1000000000l;
        frozen = true;
    }

    public void setBoundingBox(int dxHalf, int dyHalf) {
        boundingBoxX = dxHalf;
        boundingBoxY = dyHalf;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setPreviousTile(int x, int y) {
        sourceX = x;
        sourceY = y;
    }

    public void setDistanceInTile(double distance) {
        this.distanceInTile = distance;
    }

    public void setDirection(Direction d) {
        this.direction = d;
    }

    public void setTargetPoint(double targetX, double targetY) {
        this.targetX = targetX;
        this.targetY = targetY;
    }

    /**
     * moves monster in target direction
     * @param delta change of time in nanoseconds since last update
     * @param topBarHeight height of the top bar
     * @param delimiter tile dimension
     * @param delimiterHalf half of the tile dimension
     * @param width frame width
     * @param height frame height
     * @return specific constant that tells game scene how to react – monster could have changed tile or could have come
     * to the end
     */
    public int move(long delta, float topBarHeight, int delimiter,
                     int delimiterHalf, int width, int height) {
        Point p = path.getFirst();
        if (Math.abs(targetX - x) <= maxSpeed && Math.abs(targetY - y) <= maxSpeed) { //is near the target position
            sourceX = p.getX();
            sourceY = p.getY();
            path.removeFirst();
            if (path.isEmpty())
                return AT_THE_END;

            p = path.getFirst();
            if (p.getX() - sourceX == -1) {
                direction = Direction.LEFT;
                targetX = -width / 2 + p.getX() * delimiter + delimiterHalf;
                y = targetY;
            }
            else if (p.getX() - sourceX == 1) {
                direction = Direction.RIGHT;
                targetX = -width / 2 + p.getX() * delimiter + delimiterHalf;
                y = targetY;
            }
            else if (p.getY() - sourceY == -1) {
                direction = Direction.UP;
                targetY = height / 2 - p.getY() * delimiter - delimiterHalf - topBarHeight;
                x = targetX;
            }
            else if (p.getY() - sourceY == 1) {
                direction = Direction.DOWN;
                targetY = height / 2 - p.getY() * delimiter - delimiterHalf - topBarHeight;
                x = targetX;
            }
        }
        double distance = velocityConst * delta;
        switch (direction) {
            case LEFT:
                x -= distance;
                break;
            case RIGHT:
                x += distance;
                break;
            case UP:
                y += distance;
                break;
            case DOWN:
                y -= distance;
        }
        distanceInTile += distance;
        if (distanceInTile > delimiter) {
            distanceInTile -= delimiter;
            return CHANGED_TILE; //is in another tile
        }
        if (frozen) { //tries to regain its normal speed
            changeVelocityToNormal();
        }
        return -1; //nothing happened
    }

    /**
     * repairs monster's position after resizing the window
     * @param delimiter new dimension of tile
     * @param delimiterHalf half of the tile dimension
     * @param topBarHeight height of the top bar
     * @param widthHalf half of the frame width
     * @param heightHalf half of the frame height
     */
    public void repairPosition(int delimiter, int delimiterHalf, float topBarHeight, int widthHalf, int heightHalf) {
        this.delimiter = delimiter;
        velocityConst = velocity * delimiter / 1000000000l;
        //repairs y
        y = heightHalf - sourceY * delimiter - delimiterHalf -  topBarHeight;
        if (direction == Direction.DOWN)
            y -= distanceInTile - delimiterHalf;
        else if (direction == Direction.UP)
            y += distanceInTile - delimiterHalf;

        Point p = path.getFirst();
        targetY = heightHalf - p.getY() * delimiter - delimiterHalf - topBarHeight; //repairs targetY

        //repairs x
        x = -widthHalf + sourceX * delimiter + delimiterHalf;
        if (direction == Direction.RIGHT)
            x += distanceInTile - delimiterHalf;
        else if (direction == Direction.LEFT)
            x -= distanceInTile - delimiterHalf;

        targetX = -widthHalf + p.getX() * delimiter + delimiterHalf; //repairs targetX
    }
}
