package cz.cuni.mff.kholinarium.monsters;

import cz.cuni.mff.kholinarium.level.Point;
import cz.cuni.mff.kholinarium.scenes.GameScene;

import java.util.LinkedList;

/**
 * Created by Adam Hornáček on 21/02/15.
 * School project for Java
 * Faculty of Mathematics and Physics
 * Charles University in Prague
 */
public class EnemyNest {

    private int x, y;
    private long startTime;
    private long endTime;
    private long rate;
    private long timePassedSinceLastDeployment = 0;
    private int monsterType;
    private float monsterVelocity;
    private float monsterHealth;
    private int monsterValue;
    private LinkedList<Point> path;
    private int count;

    public EnemyNest(float startTime, int x, int y, int monsterType, int count, double frequency,
                     LinkedList<Point> path, float monsterVelocity, float monsterHealth, int monsterValue) {
        this.startTime = (long) (startTime * 1000000000);
        //adds 5 seconds after the last spawn as end time
        this.endTime = (long) (startTime + (count - 1) * frequency * 1000000000 + 5000000000L);
        this.rate = (long) (frequency * 1000000000);
        timePassedSinceLastDeployment = this.rate + 1; //we are able to spawn monster right away
        this.x = x;
        this.y = y;
        this.monsterType = monsterType;
        this.path = path;
        this.count = count;
        this.monsterHealth = monsterHealth;
        this.monsterVelocity = monsterVelocity;
        this.monsterValue = monsterValue;
    }

    public long getEndTime() {
        return endTime;
    }

    /**
     * spawns monster to game scene if possible
     * @param delta change of time in nanoseconds since last update
     * @param timePassed time that passed from start of the wave
     * @param topBarHeight height of the top bar
     * @param delimiter tile dimension
     * @param delimiterHalf half of the tile dimension
     * @param width frame width
     * @param height frame height
     * @return returns monster that was spawned – if none then returns null
     */
    public Monster spawnMonster(long delta, long timePassed, int topBarHeight,
                                int delimiter, int delimiterHalf, int width, int height) {
        //allowing only one monster to be deployed in one frame
        if (count == 0 || timePassed < startTime) //we cannot spawn anything
            return null;
        timePassedSinceLastDeployment += delta;
        if (timePassedSinceLastDeployment > rate) {
            timePassedSinceLastDeployment = 0;
            Monster m = new Monster(delimiter, monsterType, monsterVelocity, monsterHealth, monsterValue,
                    new LinkedList<>(path));
            m.setX(-width / 2 + x * delimiter + delimiterHalf);
            m.setY(height / 2 - y * delimiter - delimiterHalf - topBarHeight);
            m.setPreviousTile(x, y);
            Point p = path.getFirst();
            m.setDistanceInTile(delimiterHalf);
            int startX = -width / 2 + p.getX() * delimiter;
            int startY = height / 2 - p.getY() * delimiter - topBarHeight;
            if (p.getX() - m.getLastPointX() == -1) {
                m.setDirection(Direction.LEFT);
                m.setTargetPoint(startX - delimiterHalf, startY - delimiterHalf);
            } else if (p.getX() - m.getLastPointX() == 1) {
                m.setDirection(Direction.RIGHT);
                m.setTargetPoint(startX + delimiterHalf, startY - delimiterHalf);
            } else if (p.getY() - m.getLastPointY() == -1) {
                m.setDirection(Direction.UP);
                m.setTargetPoint(startX + delimiterHalf, startY - delimiterHalf);
            } else if (p.getY() - m.getLastPointY() == 1) {
                m.setDirection(Direction.DOWN);
                m.setTargetPoint(startX + delimiterHalf, startY - delimiterHalf);
            }
            count--;
            return m;
        }
        return null;
    }
}